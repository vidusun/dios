# coding=utf-8

"""
    D.I.O.S. Server: Server for Design of Inputs-Outputs with Sikuli
    Copyright (C) 2017  Gabriel Siano
    Universidad Nacional del Litoral/CONICET
    <gabrielsiano_at_gmail_dot_com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import wx
import time
import sys
import statistics #https://pypi.python.org/pypi/statistics
# import ProtocoloProcesoPronsole
import atexit
import itertools
from wx import Menu, MenuBar
from wx import EVT_MENU, EVT_CLOSE, EVT_BUTTON
import random


#NOTE: next lines are needed mainly for stand-alone execution
#To undestand this more deeply, please see:
# https://github.com/kivy/kivy/issues/4182 and
# https://www.reddit.com/r/learnpython/comments/1fiw47/need_help_packaging_my_program_into_an_executable/?st=j3k8k632&sh=7c737905
if 'twisted.internet.reactor' in sys.modules:
    del sys.modules['twisted.internet.reactor']

from twisted.internet import wxreactor
wxreactor.install()
# import twisted reactor *only after* installing wxreactor
from twisted.internet import reactor, protocol
from twisted.protocols import basic
import twisted.internet.error

import glob
import serial
import socket
import os
#NOTA: Para usuarios de Linux conviene usar subprocess32 y no subprocess.
# Pero en windows 7 tuve problemas con 32, así que uso subprocess.
if os.name == "posix":
    import subprocess32
else:
    import subprocess

class ProtocoloProcesoPronsole(protocol.ProcessProtocol):
    def __init__(self, gui):#CON ACCESO A LA GUI
        self.data = ""
        self.conectado=False#conectado es para el proceso
        self.online=False#online es para saber si la impresora está online
        self.gui=gui
        self.analizar=True
        self.protoIN=gui.pronPan.TCprotoIN
        self.protoOUT=gui.pronPan.TCprotoOUT
        self.protoOUTprint=True#para evaluar si imprimir o no en la salida
        self.protoGotSettings=False
        self.protoGotMaxFeedRates=False
        self.protoG28_OK=False

    def setAnalizar(self, TrueOrFalse):
        self.analizar=TrueOrFalse

    def getAnalizar(self):
        return self.analizar

    def connectionMade(self):
        print "connectionMade!"
        self.conectado=True

    def outReceived(self, data):# ES EL EQUIVALENTE A dataReceived PERO PARA EL STDOUT
        # self.data = self.data + data #ESTO SIRVE PARA ACUMULAR
        self.data = data
        self.processSTDOUT(data)
    def errReceived(self, err):
        self.data = err
        self.processSTDOUT(err)
    def processSTDOUT(self, data):
        datos=data.split("\n")
        for linea in datos:
            # print "...." + linea #NOTA: esta línea es la que causa el error de broken pipe al inicio
            if self.analizar:
                if linea.startswith("\033[01moffline>"):#así está formateado offline>
                    linea="OFFLINE>"
                    self.online=False
                elif linea.startswith("\033[01mPC>"):#así está formateado PC>
                    linea="PC>"+linea.lstrip("\033[01mPC>")
                    if not self.protoGotSettings:
                        self.protoGotSettings=True #NOTA: ese valor cambiará a false desde serverGUI al desconectar
                        #dentro de la función pronOnConectar
                        #self.pronProtProc.protoGotSettings=False
                        self.online=True
                        #Solicitar que no se chequee el status de temperatura y que el debug esté en ON
                        #De esta forma es posible obtener la posición a través del comando M114, sino no.
                        self.gui.pronSend(EVT_BUTTON,"statuscheckOFF")
                        self.gui.pronSend(EVT_BUTTON,"debug_ON")

                elif linea.startswith("\033[01mT:"):#así está formateado online (en online, informa la T del extrusor,
                # por lo cual no pone online, sino T:valor
                    linea="ONLINE>"
                    self.online=True
                elif linea.startswith("[INFO] RECV: X"):#m114, where, posición
                    # tipo de frase: [INFO] RECV: X:100.00 Y:100.00 Z:10.00 E:0.00 Count X: 100.00 Y:100.00 Z:10.00
                    posi=linea.split(" ")
                    # ['[INFO]', 'RECV:', 'X:100.00', 'Y:100.00', 'Z:10.00', 'E:0.00', 'Count', 'X:', '100.00', 'Y:100.00', 'Z:10.00']
                    x=posi[2].split(":")[1]
                    y=posi[3].split(":")[1]
                    z=posi[4].split(":")[1]
                    self.gui.pronSetActPosi(x,y,z)
                elif linea.startswith("[INFO] RECV: ok T"):#Temperature
                    # tipo de frase: '[INFO] RECV: ok T:20.3 /0.0 B:19.0 /0.0 T0:20.3 /0.0 @:0 B@:0'
                    T=linea.split(" ")
                    # ['[INFO]', 'RECV:', 'ok', 'T:20.3', '/0.0', 'B:19.0', '/0.0', 'T0:20.3', '/0.0', '@:0', 'B@:0\r']
                    #Temperature Hot End = THE
                    THE=T[3].split(":")[1]
                    #Temperature Bed = TB
                    TB=T[5].split(":")[1]
                    self.gui.pronSetActTemperature(THE,TB)
                elif linea.startswith("[ERROR] Macro"):
                    if linea.endswith("is not defined"):#Macros no definidas
                        nombreMacroNec=linea.split("'")#Se hace split con ' . El mensaje es del tipo "[ERROR] Macro 'NOMBRE' "is not defined"
                        nombreMacroNec=nombreMacroNec[1]
                        # self.gui.pronDefineMacroNec(nombreMacroNec)#se indica a la gui que defina las macros necesarias
                elif linea.startswith("Macro '"):
                    if linea.endswith("defined as:"):#Macros definidas
                        nombreMacroNec=linea.split("'")#Se hace split con ' . El mensaje es del tipo "Macro 'miG28' defined as:"
                        nombreMacroNec=nombreMacroNec[1]
                        # self.gui.pronMacroNecLineByLine(nombreMacroNec)#se indica a la gui que CONTROLE LÍNEA A LÍNEA
                        #  las macros necesarias
                elif linea.startswith("Welcome to the printer console!"):  # mensaje inicial
                    pass
                elif not self.protoG28_OK:
                    if linea.startswith("[INFO] RECV: G28_OK"):
                        self.protoG28_OK = True  # NOTA: ese valor cambiará a false desde serverGUI al desconectar
                        # dentro de la función pronOnConectar
                        # self.pronProtProc.protoG28_OK=False
                        self.gui.pronSetG28_OK()
                elif linea.startswith("Print ended at"):  # fin de una impresion
                    self.gui.pronUpdateMoviendo(False)
                        # NOTA: El usuario estará a cargo de controlar los max feed rates
                # elif not self.protoGotMaxFeedRates:
                #     if linea.find("MaxFeedRates") and linea.find("ok"):  # Informe de MaxFeedRates con G7771
                #         linea2 = linea.split(" ")
                #         maxFRxyz = [-1, -1, -1]
                #         for lin in linea2:
                #             if lin.startswith("MaxFeedRates") or lin.startswith("ok"):
                #                 pass
                #             elif lin.startswith("X"):
                #                 maxFRxyz[0] = int(lin.lstrip("X"))
                #             elif lin.startswith("Y"):
                #                 maxFRxyz[1] = int(lin.lstrip("Y"))
                #             elif lin.startswith("Z"):
                #                 maxFRxyz[2] = int(lin.lstrip("Z"))
                #         if maxFRxyz[0] is not -1 and maxFRxyz[1] is not -1 and maxFRxyz[2] is not -1:
                #             self.protoGotMaxFeedRates = True  # NOTA: ese valor cambiará a false desde serverGUI al desconectar
                #             # dentro de la función pronOnConectar
                #             # self.pronProtProc.protoGotMaxFeedRates=False
                #             self.gui.pronAdaptMaxFeedRates(maxFRxyz)

         ## Insertar la linea en la gui
            # if gui:
            if self.protoOUTprint:
                try:
                    # val = gui.protoOUT.GetValue()
                    val = self.protoOUT.GetValue()
                    # gui.protoOUT.SetValue(val + linea + "\n" )
                    self.protoOUT.SetValue(val + linea + "\n" )
                    # gui.protoOUT.SetInsertionPointEnd()
                    self.protoOUT.SetInsertionPointEnd()
                except UnicodeDecodeError:
                    print "Típico error por poner acentos y esas cosas"
                    pass

    def inConnectionLost(self):
        print "INConnectionLost! stdIN is closed! (we probably did it)"
        self.conectado=False
        self.online=False
        self.protoG28_OK=False
        self.gui.pronSetG28_OK()
    def outConnectionLost(self):
        print "OUTConnectionLost! The child closed their stdOUT!"
        print self.data
        self.conectado=False
        self.online=False
        self.protoG28_OK=False
        self.gui.pronSetG28_OK()
    def errConnectionLost(self):
        print "ERRConnectionLost! The child closed their stdERR."
        self.conectado=False
        self.online=False
        self.protoG28_OK=False
        self.gui.pronSetG28_OK()
    def processExited(self, reason):
        self.conectado=False
        self.online=False
        self.protoG28_OK=False
        self.gui.pronSetG28_OK()
        print "processExited, status %d" % (reason.value.exitCode,)
    def processEnded(self, reason):
        self.conectado=False
        self.online=False
        self.protoG28_OK=False
        self.gui.pronSetG28_OK()
        print "processEnded, status %d" % (reason.value.exitCode,)
        print "quitting"
        # reactor.stop() #EN MI CASO EL STOP NO DEBE APLICARSE ACÁ PORQUE EL REACTOR TIENE OTROS USOS

class ChatFrame(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, parent=None, title=u"D.I.O.S.: Design of Inputs-Outputs with Sikuli",size=(1110, 610),pos=(0,0))
        self.protocol = ServerProtocol  # twisted Protocol
        self.servidor=None
        self.puerto=None
        self.configFile=None
        self.serverPort=None
        self.serverName = socket.gethostname()
        self.primeraConexion=True
        self.IsConectado=False
        self.serverIP=None
        self.OSname=os.name
        self.listaClientes=[]
        self.dictClientesIPs={}
        self.sikuServersAlreadyON = []
        self.dictSikulis={}
        self.dictTimeCoordServer={}
        self.dictTimeCoordClients={}
        self.dictTimeCoordMedian={}
        self.nTimesTimeCoord=7#cantidad de veces que se toman los tiempos al coordinar con un Cliente

        ##### PROPIEDADES GUI PARA PRONSOLE
        self.pronPortsAvailable= self.serial_ports()
        #Ya que en Windows el primer puerto es COM1 y suele estar reservado y no ser usado para dispositivos,
        #se da la orden de que se seleccione el siguiente (si es que hay más de uno)
        if len(self.pronPortsAvailable) is not 0:
            if self.pronPortsAvailable[0]=="COM1" and len(self.pronPortsAvailable)>1:
                self.pronPort = self.pronPortsAvailable[1]
            else:
                self.pronPort=self.pronPortsAvailable[0]
        else:
            self.pronPort="NO PORT"
        self.pronBaudrate=250000
        self.localFolder=os.getcwd()
        self.pronTranspProc=None
        self.pronPathPrintrun=self.localFolder+"/Printrun"
        self.pronPathGCODES=self.pronPathPrintrun+"/GCODES"
        if os.name == "nt":
            aux=self.pronPathPrintrun
            aux=aux.replace('/', "\\")
            self.pronPathPrintrun=aux+"\\"
            aux=self.pronPathGCODES
            aux=aux.replace("/", "\\")
            self.pronPathGCODES=aux+"\\"
            # print self.pronPathPrintrun
            # print self.pronPathGCODES
        self.pronGCODESlist=[]
        self.pronGCODESactual=" "
        self.pronCommHist=[]
        self.pronCommHistNItems=0
        self.pronCommHistMaxItems=10
        self.pronCommHistActualItem=-1#util para keyUp y keyDown
        self.pronPosition=[-1,-1,-1]#posiciones x,y,z
        self.pronTemperatures = [-1, -1]  # Temperatures: Hot End, Bed
        self.pronMaxFeedRates=[4200,4200,180]#mm/min
        self.pronManualFeedRates=[4200,4200,180]#mm/min
        self.pronG28_OK=False
        self.pronAddM400=False#En el inicio, q no agregue M400 a los gcodes aun cdo sea necesario
        self.pronMoviendo=False#inicialmente no se estará moviendo
        self.pronCheckTempValue=False
        ##### FIN PROPIEDADES GUI PARA PRONSOLE

        if os.name == "posix":
            self.serverIP=subprocess32.check_output(["hostname", "-I"],stderr=subprocess32.STDOUT)
        elif os.name == "nt":
            self.serverIP=socket.gethostbyname(socket.gethostname())

        #En caso de que haya más de un IP (más de una interface de red activa), entonces serverIP
        #contendrá más de un IP, separados entre ellos por un espacio: reformatear
        self.serverIP=self.serverIP.replace(' ','\n')#reemplaza espacios por Enters

        menu = Menu()
        menu.Append(ID_EXIT, "E&xit", "Terminate the program")
        menuBar = MenuBar()
        menuBar.Append(menu, "&File")
        self.SetMenuBar(menuBar)
        EVT_MENU(self, ID_EXIT,  self.DoExit)

        self.text = wx.TextCtrl(self, pos=(0,0), style=wx.TE_MULTILINE | wx.TE_READONLY, size=(200, 300))
        self.ctrl = wx.TextCtrl(self, pos=(0,301), style=wx.TE_PROCESS_ENTER, size=(200, 25))

        self.boxServidor =  wx.TextCtrl(self, pos=(200,0), style=wx.TE_PROCESS_ENTER, size=(150, 25))
        self.boxPuerto = wx.TextCtrl(self, pos=(350,0), style=wx.TE_PROCESS_ENTER, size=(50, 25))
        self.boxNTimesTimeCoord = wx.TextCtrl(self, pos=(200, 100), style=wx.TE_PROCESS_ENTER, size=(30, 25))
        self.boxNTimesTimeCoord.SetValue(str(self.nTimesTimeCoord))
        wx.StaticText(self, -1, 'Time Offset:', (200, 80))
        wx.StaticText(self, -1, 'nTimes', (200, 125))
        self.btnTimeOffset = wx.Button(self, -1, 'Offset',pos=(230,100), size=(90, 25))
        self.btnTimeOffset.Bind(wx.EVT_BUTTON, self.coordTimes)

        self.boxServidor.SetToolTipString(self.serverIP)

        wx.StaticText(self, -1, 'Server: '+ self.serverName, (200, 25))
        wx.StaticText(self, -1, 'Port', (350, 25))
        self.boxServidor.Bind(wx.EVT_TEXT_ENTER, self.cambiarConfig)
        self.boxPuerto.Bind(wx.EVT_TEXT_ENTER, self.cambiarConfig)
        self.boxNTimesTimeCoord.Bind(wx.EVT_TEXT_ENTER, self.changenTimesTimeCoord)

        self.notificaciones= wx.TextCtrl(self, pos=(1, 355), style=wx.TE_MULTILINE | wx.TE_READONLY | wx.TE_RICH, size=(390, 196))
        self.notificaciones.SetForegroundColour(wx.RED)
        self.notificaciones.Show()


        self.btnConectar = wx.Button(self, -1, 'Connect',pos=(200,50), size=(100, 25))
        self.btnConectar.Bind(wx.EVT_BUTTON, self.OnConectar)

        self.btnExeSikuli= wx.Button(self, -1, 'exeSikuli',pos=(200,300), size=(100, 25))
        self.btnExeSikuli.Bind(wx.EVT_BUTTON, self.OnExeSikuli)

        self.serverPortStateText=wx.StaticText(self, -1, 'disconected', (305, 55))

        self.ctrl.Bind(wx.EVT_TEXT_ENTER, self.send)

        self.comboListaClientes = wx.ComboBox(self, pos=(0,325), size=wx.DefaultSize,  style=wx.CB_SORT, value="Clients")
        self.comboListaClientes.SetEditable(False)
        self.comboListaClientes.Bind(wx.EVT_COMBOBOX, self.updateComboDictSikulis)

        self.comboDictSikulis = wx.ComboBox(self, pos=(200,325), size=wx.DefaultSize,  style=wx.CB_SORT, value="Sikulis")
        self.comboDictSikulis.SetEditable(False)

        EVT_CLOSE(self, lambda evt: reactor.stop())

        if os.path.isfile("configServ"):
            self.configFile = open("configServ", "r+")#abrir como lectura y escritura
            self.configFile.seek(0,0)
            self.servidor=self.configFile.readline().rstrip('\r\n')
            self.puerto=int(self.configFile.readline().rstrip('\r\n'))
            self.configFile.close()
        else:
            #Crearlo:
            self.configFile = open("configServ", "w+")#Crearlo, abrir como escritura y lectura
            self.configFile.seek(0,0)
            self.configFile.writelines(["127.0.0.1\n", "5001\n"])
            self.servidor="127.0.0.1"
            self.puerto=5001
            self.configFile.close()

        self.boxServidor.SetValue(self.servidor)
        self.boxPuerto.SetValue(str(self.puerto))


        ########################## Panel para Sequences ##########################
        self.panSec = wx.Panel(self, -1, style=wx.SIMPLE_BORDER, pos=(400,1), size=(400,550))
        #Sequence
        self.panSec.listSec = wx.ListCtrl(self.panSec, -1, style=wx.LC_REPORT|wx.BORDER_SUNKEN|wx.LC_SINGLE_SEL|wx.LC_HRULES|wx.LC_VRULES, size=(395,350),pos=(1,1))
        self.panSec.listSec.Show(False)#Arranca oculta

        self.treeSec = wx.TreeCtrl(self.panSec, -1, style=wx.TR_DEFAULT_STYLE |wx.TR_FULL_ROW_HIGHLIGHT |wx.TR_HAS_BUTTONS|wx.TR_SINGLE, size=(395,350),pos=(1,1))
        self.treeSec.SequenceRoot  = self.treeSec.AddRoot('Sequence')
        self.treeSec.SelectItem(self.treeSec.SequenceRoot, True)


        ## CONFIGURACIÓN Sequence
        self.secConfig={
            'currentItem':0,
            'orderSTR':"1",
            'orderLIST':[1],
            'nLoops':0,
            'infoLoops':{},
            'secDictLoops':{},
            'secListaClientes':[],
            'secDictSikulis':{},
            'NoSikuCommands':['TypeORD','TypeORDs','TypeText','TypeTime','TypeIDX','SendKEY','Time','TC', 'TCS','Delay','GCODEord','GCODE','Pause','DOrd', 'MsgBox','Notif'], #comandos que involucran a un cliente pero no son sikulis: 'TypeORD','TypeORDs','TypeText','TypeTime','SendKEY'
            'FinalSec':[],
            'secIs':'Stopped',#Stopped, Running, Paused
            'clientesNecFinalSec':[],
            'FinalSecLineAct':0,
            'FinalSecNLines':0,
            'secTIMES':[],
            'TCoordActual':0,
            'secDictTCoords':{},
            'secEndTCoordsVencidos':[],
            'delaySTR':"0",
            'secDELAYS':[],
            'secDELAYSfromTC': [],#DELAYS derivados de TC
            'secListaGCODES':[],
            'secGCODEordActual':'',
            'secFlagExistsGCODEord':False,
            'secNDords':0,#cantidad de DelayORDs en la secuecia
            'secDOrdActual': "",  # sentence: (ORD+20)*9
            'secDictFragmentos':{},#auxiliar para pasar Sequence en fragmentos
            'secONE': 1,
            }


        self.panSec.listSec.InsertColumn(0, "Sequence")
        self.panSec.listSec.SetColumnWidth(0, 395)


        #Subpanel botones
        self.panSec.subpanControl=wx.Panel(self.panSec, -1, pos=(0,351), size=(400,200))
        #Botones
        self.panSec.subpanControl.btnAdd=wx.Button(self.panSec.subpanControl, -1, 'Add', pos=(0,0), size=(40,25))
        self.panSec.subpanControl.btnAdd.Bind(wx.EVT_BUTTON, self.panSecTreeOnAdd)
        self.panSec.subpanControl.btnRemove=wx.Button(self.panSec.subpanControl, -1, 'Remove', pos=(40,0), size=(65,25))
        self.panSec.subpanControl.btnRemove.Bind(wx.EVT_BUTTON, self.panSecTreeOnRemove)
        self.panSec.subpanControl.btnClear=wx.Button(self.panSec.subpanControl, -1, 'Clear', pos=(105,0), size=(50,25))
        self.panSec.subpanControl.btnClear.Bind(wx.EVT_BUTTON, self.panSecTreeOnClear)
        self.panSec.subpanControl.btnCheck=wx.Button(self.panSec.subpanControl, -1, 'Check', pos=(155,0), size=(55,25))
        self.panSec.subpanControl.btnCheck.Bind(wx.EVT_BUTTON, self.panSecOnCheck)
        self.panSec.subpanControl.btnRun=wx.Button(self.panSec.subpanControl, -1, 'Run', pos=(210,0), size=(65,25))
        self.panSec.subpanControl.btnRun.Bind(wx.EVT_BUTTON, self.panSecOnRunPauseResume)


        self.panSec.subpanControl.btnAddGCODE=wx.Button(self.panSec.subpanControl, -1, '+GCODE', pos=(200,47), size=(70,23))
        self.panSec.subpanControl.btnAddGCODE.Bind(wx.EVT_BUTTON, self.panSecTreeOnAdd)

        self.panSec.subpanControl.btnAddGCODEord=wx.Button(self.panSec.subpanControl, -1, '+GCODEord', pos=(267,47), size=(90,23))
        self.panSec.subpanControl.btnAddGCODEord.Bind(wx.EVT_BUTTON, self.panSecTreeOnAdd)

        self.panSec.subpanControl.boxGCODEord =  wx.TextCtrl(self.panSec.subpanControl, pos=(198,69), style=wx.TE_PROCESS_ENTER, size=(198, 25))
        self.panSec.subpanControl.boxGCODEord.SetValue(" ")
        self.panSec.subpanControl.boxGCODEord.Bind(wx.EVT_TEXT_ENTER, self.panSecOnChangeGCODEord)

        self.panSec.subpanControl.boxTypeText =  wx.TextCtrl(self.panSec.subpanControl, pos=(236,95), style=wx.TE_PROCESS_ENTER, size=(160, 25))
        self.panSec.subpanControl.boxTypeText.SetValue("")
        self.panSec.subpanControl.boxTypeText.Bind(wx.EVT_TEXT_ENTER, self.panSecOnChangeTypeText)


        self.panSec.subpanControl.btnStop=wx.Button(self.panSec.subpanControl, -1, 'Stop', pos=(275,0), size=(50,25))
        self.panSec.subpanControl.btnStop.SetBackgroundColour( ( 255, 0, 0 ) )#rojo
        self.panSec.subpanControl.btnStop.Show(False)
        self.panSec.subpanControl.btnStop.Bind(wx.EVT_BUTTON, self.panSecOnStop)

        self.panSec.subpanControl.btnSecSetLineaActual=wx.Button(self.panSec.subpanControl, -1, 'SetLine', pos=(305,23), size=(65,25))
        self.panSec.subpanControl.btnSecSetLineaActual.Show(False)
        self.panSec.subpanControl.btnSecSetLineaActual.Bind(wx.EVT_BUTTON, self.panSecSetLineaActual)

        self.panSec.subpanControl.btnLoop=wx.Button(self.panSec.subpanControl, -1, 'Loop', pos=(0,23), size=(60,25))
        self.panSec.subpanControl.btnLoop.Bind(wx.EVT_BUTTON, self.panSecTreeOnAdd)

        self.panSec.subpanControl.btnSecOpen=wx.Button(self.panSec.subpanControl, -1, 'Open', pos=(200,23), size=(55,25))
        self.panSec.subpanControl.btnSecOpen.Bind(wx.EVT_BUTTON, self.panSecOnSecOpen)
        self.panSec.subpanControl.btnSecSave=wx.Button(self.panSec.subpanControl, -1, 'Save', pos=(250,23), size=(55,25))
        self.panSec.subpanControl.btnSecSave.Bind(wx.EVT_BUTTON, self.panSecOnSecSave)

        self.panSec.subpanControl.btnPause=wx.Button(self.panSec.subpanControl, -1, 'Pause', pos=(60,23), size=(60,25), name='Pause')
        self.panSec.subpanControl.btnPause.Bind(wx.EVT_BUTTON, self.panSecTreeOnAdd)

        #Fuente para botones Type
        staticTypeBtnsFont = wx.Font(7, wx.FONTFAMILY_ROMAN, wx.NORMAL, wx.NORMAL)

        self.panSec.subpanControl.staticType = wx.StaticText(self.panSec.subpanControl, -1, 'Type..', pos=(5,100))
        self.panSec.subpanControl.staticType.SetFont(wx.Font(9, wx.FONTFAMILY_ROMAN, wx.NORMAL, wx.NORMAL))

        self.panSec.subpanControl.btnSecTypeORD=wx.Button(self.panSec.subpanControl, -1, 'ORD', pos=(46,95), size=(38,25), name='TypeORD')
        self.panSec.subpanControl.btnSecTypeORD.SetFont(staticTypeBtnsFont)
        self.panSec.subpanControl.btnSecTypeORD.Bind(wx.EVT_BUTTON, self.panSecTreeOnAdd)

        self.panSec.subpanControl.btnSecTypeORDs=wx.Button(self.panSec.subpanControl, -1, 'ORDs', pos=(84,95), size=(38,25), name='TypeORDs')
        self.panSec.subpanControl.btnSecTypeORDs.SetFont(staticTypeBtnsFont)
        self.panSec.subpanControl.btnSecTypeORDs.Bind(wx.EVT_BUTTON, self.panSecTreeOnAdd)

        self.panSec.subpanControl.btnSecTypeIDX=wx.Button(self.panSec.subpanControl, -1, 'IDX', pos=(122,95), size=(38,25), name='TypeIDX')
        self.panSec.subpanControl.btnSecTypeIDX.SetFont(staticTypeBtnsFont)
        self.panSec.subpanControl.btnSecTypeIDX.Bind(wx.EVT_BUTTON, self.panSecTreeOnAdd)

        self.panSec.subpanControl.btnSecTypeTime=wx.Button(self.panSec.subpanControl, -1, 'Time', pos=(160,95), size=(38,25), name='TypeTime')
        self.panSec.subpanControl.btnSecTypeTime.SetFont(staticTypeBtnsFont)
        self.panSec.subpanControl.btnSecTypeTime.Bind(wx.EVT_BUTTON, self.panSecTreeOnAdd)

        self.panSec.subpanControl.btnSecTypeText=wx.Button(self.panSec.subpanControl, -1, 'Text:', pos=(198,95), size=(38,25), name='TypeText')
        self.panSec.subpanControl.btnSecTypeText.SetFont(staticTypeBtnsFont)
        self.panSec.subpanControl.btnSecTypeText.Bind(wx.EVT_BUTTON, self.panSecTreeOnAdd)


        self.panSec.subpanControl.btnSecSendKEY=wx.Button(self.panSec.subpanControl, -1, 'SendKEY', pos=(1,120), size=(60,26))
        self.panSec.subpanControl.btnSecSendKEY.SetFont(wx.Font(7, wx.FONTFAMILY_ROMAN, wx.NORMAL, wx.NORMAL))
        self.panSec.subpanControl.btnSecSendKEY.Bind(wx.EVT_BUTTON, self.panSecTreeOnAdd)

        TimeBtnsFont = wx.Font(7, wx.FONTFAMILY_ROMAN, wx.NORMAL, wx.NORMAL)
        self.panSec.subpanControl.btnSecTime=wx.Button(self.panSec.subpanControl, -1, 'Time', pos=(142,146), size=(56,25), name='Time')
        self.panSec.subpanControl.btnSecTime.SetFont(staticTypeBtnsFont)
        self.panSec.subpanControl.btnSecTime.Bind(wx.EVT_BUTTON, self.panSecTreeOnAdd)

        self.panSec.subpanControl.btnSecTCoord=wx.Button(self.panSec.subpanControl, -1, 'TC', pos=(236,146), size=(34,25))
        self.panSec.subpanControl.btnSecTCoord.SetFont(staticTypeBtnsFont)
        self.panSec.subpanControl.btnSecTCoord.Bind(wx.EVT_BUTTON, self.panSecTreeOnAdd)

        #Boton para tiempos de coordinación que al detectarse vencidos deben parar la Sequence (S=STOP)
        self.panSec.subpanControl.btnSecTCoordS=wx.Button(self.panSec.subpanControl, -1, 'TCS', pos=(198,146), size=(38,25))
        self.panSec.subpanControl.btnSecTCoordS.SetFont(staticTypeBtnsFont)
        self.panSec.subpanControl.btnSecTCoordS.Bind(wx.EVT_BUTTON, self.panSecTreeOnAdd)

        self.panSec.subpanControl.btnSecDelay=wx.Button(self.panSec.subpanControl, -1, 'Delay', pos=(1,146), size=(60,25))
        self.panSec.subpanControl.btnSecDelay.SetFont(staticTypeBtnsFont)
        self.panSec.subpanControl.btnSecDelay.Bind(wx.EVT_BUTTON, self.panSecTreeOnAdd)

        #Delay función de Ord
        self.panSec.subpanControl.btnSecDOrd=wx.Button(self.panSec.subpanControl, -1, 'DOrd', pos=(1,172), size=(60,25))
        self.panSec.subpanControl.btnSecDOrd.SetFont(staticTypeBtnsFont)
        self.panSec.subpanControl.btnSecDOrd.Bind(wx.EVT_BUTTON, self.panSecTreeOnAdd)

        self.panSec.subpanControl.btnMsgBox=wx.Button(self.panSec.subpanControl, -1, 'MsgBox', pos=(142,120), size=(56,25))
        self.panSec.subpanControl.btnMsgBox.SetFont(staticTypeBtnsFont)
        self.panSec.subpanControl.btnMsgBox.Bind(wx.EVT_BUTTON, self.panSecTreeOnAdd)

        self.panSec.subpanControl.btnNotif=wx.Button(self.panSec.subpanControl, -1, 'Notif', pos=(197,120), size=(39,25))
        self.panSec.subpanControl.btnNotif.SetFont(staticTypeBtnsFont)
        self.panSec.subpanControl.btnNotif.Bind(wx.EVT_BUTTON, self.panSecTreeOnAdd)

        #Boxes
        self.panSec.subpanControl.boxOrder =  wx.TextCtrl(self.panSec.subpanControl, pos=(0,69), style=wx.TE_PROCESS_ENTER, size=(198, 25))
        self.panSec.subpanControl.boxOrder.SetValue("1")
        self.panSec.subpanControl.boxOrder.Bind(wx.EVT_TEXT_ENTER, self.panSecOnChangeOrder)
        self.panSec.subpanControl.staticOrder = wx.StaticText(self.panSec.subpanControl, -1, 'Order', pos=(3,49))

        self.panSec.subpanControl.boxDelay =  wx.TextCtrl(self.panSec.subpanControl, pos=(61,147), style=wx.TE_PROCESS_ENTER, size=(54, 23))
        self.panSec.subpanControl.boxDelay.SetValue("0")
        self.panSec.subpanControl.boxDelay.Bind(wx.EVT_TEXT_ENTER, self.panSecOnChangeDelay)
        self.panSec.subpanControl.staticDelay = wx.StaticText(self.panSec.subpanControl, -1, '(ms)', pos=(115,152))

        self.panSec.subpanControl.boxDOrd = wx.TextCtrl(self.panSec.subpanControl, pos=(61, 172), style=wx.TE_PROCESS_ENTER, size=(180, 25))
        self.panSec.subpanControl.boxDOrd.SetValue(" ")
        self.panSec.subpanControl.boxDOrd.Bind(wx.EVT_TEXT_ENTER, self.panSecOnChangeDOrd)

        self.panSec.subpanControl.boxTCoord =  wx.TextCtrl(self.panSec.subpanControl, pos=(270,147), style=wx.TE_PROCESS_ENTER, size=(65, 23))
        self.panSec.subpanControl.boxTCoord.SetValue("0")
        self.panSec.subpanControl.boxTCoord.Bind(wx.EVT_TEXT_ENTER, self.panSecSetTCoordActual)
        self.panSec.subpanControl.staticTCoord = wx.StaticText(self.panSec.subpanControl, -1, '(ms)', pos=(335,152))
        self.panSec.subpanControl.staticTCoordRestante = wx.StaticText(self.panSec.subpanControl, -1, " ", pos=(335,1))
        self.panSec.subpanControl.staticTCoordRestante.SetForegroundColour(wx.RED)
        staticTCoordRestanteFont = wx.Font(14, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
        self.panSec.subpanControl.staticTCoordRestante.SetFont(staticTCoordRestanteFont)


        self.panSec.subpanControl.staticTCoordRestante.Show(False)


        #Combos
        self.panSec.subpanControl.comboKeys = wx.ComboBox(self.panSec.subpanControl, pos=(62,120), size=(80,25),  style=wx.CB_SORT, value="Key")
        self.panSec.subpanControl.comboKeys.SetEditable(False)
        self.panSec.subpanControl.comboKeys.AppendItems(['Enter','ESC'])

########## PANEL PRONSOLE
        self.pronPan = wx.Panel(self, -1, style=wx.SIMPLE_BORDER, pos=(801,1), size=(300,550))
        ## ENTRADA Y SALIDA GUI PRONSOLE
        self.pronPan.TCprotoOUT = wx.TextCtrl(self.pronPan, pos=(0,0), style=wx.TE_MULTILINE | wx.TE_READONLY, size=(297, 175), name="TCprotoOUT")
        self.pronPan.TCprotoIN = wx.TextCtrl(self.pronPan, pos=(0,176), style=wx.TE_PROCESS_ENTER, size=(297, 25), name="TCprotoIN")

        ## RECIBIR A LA GUI
        # En el init, recibe a la gui (self) y a 2 TextCtrl (protoIN y protoOUT)
        # self.pronProtProc= ProtocoloProcesoPronsole.ProtocoloProcesoPronsole(self, self.pronPan.TCprotoIN, self.pronPan.TCprotoOUT )
        self.pronProtProc = []

        ## MACROS:
        #Las siguiente macros son necesarias y su existencia y contenido DEBERÍAN SER chequeados.
        #Pueden agregarse directo desde pronsole o pronterface.
        #Tener en cuenta el indent en términos de Python
        self.pronDictMacrosNec = {
                        'debug_ON':["    !self.p.loud = 1"],
                        'debug_OFF':["    !self.p.loud = 0"],
                        'statuscheckOFF':["    !self.statuscheck=False","    !self.status_thread.join(0.01)", "    !while self.status_thread.isAlive():", "        !pass"],
                        'statuscheckON':["    !if not self.status_thread.isAlive():","        !self.statuscheck = True","        !self.status_thread = threading.Thread(target = self.statuschecker)","        !self.status_thread.start()"],
        }

########################################################################################################################
        #Subpanel controles
        self.pronPan.subpanControl=wx.Panel(self.pronPan, -1, pos=(0,200), size=(300,350))

        self.pronPan.subpanControl.BTconectar = wx.Button(self.pronPan.subpanControl, -1, 'Go!',pos=(0,0), size=(40, 25))
        self.pronPan.subpanControl.BTpos = wx.Button(self.pronPan.subpanControl, -1, 'Pos',pos=(0,25), size=(40, 25))
        # self.pronPan.subpanControl.BTmaxFeedRates = wx.Button(self.pronPan.subpanControl, -1, 'MFR',pos=(190,25), size=(45, 25))
        self.pronPan.subpanControl.TGCheckTemp = wx.ToggleButton(self.pronPan.subpanControl, -1, 'chkTemp',pos=(41,25), size=(80, 25))

        ## COMBO DE PUERTOS
        self.pronPan.subpanControl.comboPorts = wx.ComboBox(self.pronPan.subpanControl, pos=(41,0), size=(140,25),  style=wx.CB_SORT, value=self.pronPort)
        self.pronPan.subpanControl.comboPorts.SetEditable(False)
        self.pronPan.subpanControl.comboPorts.AppendItems(self.pronPortsAvailable)
        self.pronPan.subpanControl.comboPorts.Bind(wx.EVT_COMBOBOX, self.updateComboPort)

        self.pronPan.subpanControl.STextAtBaudrate = wx.StaticText(self.pronPan.subpanControl, -1,"@", pos=(185, 4), size=(20, 20))

        ## COMBO DE BAUDRATES
        self.pronPan.subpanControl.comboBaudrate = wx.ComboBox(self.pronPan.subpanControl, pos=(205,0), size=(85,25),  style=wx.CB_SORT, value=str(self.pronBaudrate))
        self.pronPan.subpanControl.comboBaudrate.SetEditable(False)
        self.pronPan.subpanControl.comboBaudrate.AppendItems(["9600","57600","115200","250000"])
        self.pronPan.subpanControl.comboBaudrate.Bind(wx.EVT_COMBOBOX, self.updateComboBaudrate)

        #SubSubpanel GCODES
        self.pronPan.subpanControl.spGCODES=wx.Panel(self.pronPan.subpanControl, -1, pos=(1,53), size=(296,80),style=wx.SIMPLE_BORDER)

        self.pronPan.subpanControl.spGCODES.boxPathGCODES =  wx.TextCtrl(self.pronPan.subpanControl.spGCODES, pos=(0,0), style=wx.TE_READONLY, size=(290, 24),name="boxPathGCODES")
        self.pronPan.subpanControl.spGCODES.boxPathGCODES.SetValue(self.pronPathGCODES)

        self.pronPan.subpanControl.spGCODES.comboGCODES = wx.ComboBox(self.pronPan.subpanControl.spGCODES, pos=(0,25), size=(200,25),  style=wx.CB_SORT, value="GCODES")
        self.pronPan.subpanControl.spGCODES.comboGCODES.SetEditable(False)
        self.pronPan.subpanControl.spGCODES.comboGCODES.Bind(wx.EVT_COMBOBOX, self.pronUpdateGCODESactual)

        self.pronPan.subpanControl.spGCODES.BTchangePronPathGCODES = wx.Button(self.pronPan.subpanControl.spGCODES, -1, 'Path',pos=(201,25), size=(45, 25))
        self.pronPan.subpanControl.spGCODES.BTchangePronPathGCODES.Bind(wx.EVT_BUTTON, self.pronOnSelectGCODESFolder)
        #Llenar inicialmente la lista con los GCODES presentes en la carpeta por defecto (self.pronPathGCODES)
        self.pronUpdateGCODESlistYCombo()

        self.pronPan.subpanControl.spGCODES.BTrunGCODEActual= wx.Button(self.pronPan.subpanControl.spGCODES, -1, 'Run',pos=(0,50), size=(45, 25))
        self.pronPan.subpanControl.spGCODES.BTrunGCODEActual.Bind(wx.EVT_BUTTON, self.pronRunGCODEActual)

        self.pronPan.subpanControl.spGCODES.TGPronAddM400 = wx.ToggleButton(self.pronPan.subpanControl.spGCODES, -1, 'M400off',pos=(201,50), size=(80, 25))
        self.pronPan.subpanControl.spGCODES.TGPronAddM400.Bind(wx.EVT_TOGGLEBUTTON, self.pronSetM400)

        self.pronPan.subpanControl.spGCODES.STextMoviendo=wx.StaticText(self.pronPan.subpanControl.spGCODES, -1, "STOPPED", pos=(101,55), size=(80, 25))

        #BOTÓN DETENER POR EMERGENCIA (KILL)
        #NOTA: Dado que se usa M400 para confirmar culminación de movimientos, M112 (KILL) no será ejecutado hasta
        # terminar con los movimientos actuales. Conviene descomponer grandes movimientos en pequeños.
        # self.pronPan.subpanControl.BTpronKILL = wx.Button(self.pronPan.subpanControl, -1, 'KILL!',pos=(120,110),size=(45, 45),name="BTpronKILL" )
        # self.pronPan.subpanControl.BTpronKILL.SetBackgroundColour( ( 255,0,0 ) )
        # self.pronPan.subpanControl.BTpronKILL.SetForegroundColour( ( 0, 255, 0) )
        # self.pronPan.subpanControl.BTpronKILL.Bind(wx.EVT_BUTTON, self.pronKILL)


        #SubSubpaneles de FeedRates de XY y de Z (mm/s)
        self.pronPan.subpanControl.spFRxyz=wx.Panel(self.pronPan.subpanControl, -1, pos=(85,240), size=(165,50))

        self.pronPan.subpanControl.spFRxyz.boxFRxy =  wx.TextCtrl(self.pronPan.subpanControl.spFRxyz, pos=(0,0), style=wx.TE_PROCESS_ENTER, size=(50, 24),name="boxFRxy")
        self.pronPan.subpanControl.spFRxyz.boxFRz  =  wx.TextCtrl(self.pronPan.subpanControl.spFRxyz, pos=(0,24), style=wx.TE_PROCESS_ENTER, size=(50, 24),name="boxFRz")

        self.pronPan.subpanControl.spFRxyz.boxFRxy.SetValue(str(self.pronManualFeedRates[0]))#X (suponer igual a Y)
        self.pronPan.subpanControl.spFRxyz.boxFRz.SetValue(str(self.pronManualFeedRates[2]))#Z

        self.pronPan.subpanControl.spFRxyz.STextFRXY=wx.StaticText(self.pronPan.subpanControl.spFRxyz, -1, str(round(float(self.pronManualFeedRates[0])/60.0,2))+' mm/s (XY)', (50, 4))
        self.pronPan.subpanControl.spFRxyz.STextFRZ=wx.StaticText(self.pronPan.subpanControl.spFRxyz, -1, str(round(float(self.pronManualFeedRates[2])/60.0,2))+' mm/s (Z)', (50, 28))


        #SubSubpaneles de HOMEs
        self.pronPan.subpanControl.spHOME=wx.Panel(self.pronPan.subpanControl, -1, pos=(98,210), size=(105,27),style=wx.SIMPLE_BORDER)

        self.pronPan.subpanControl.spHOME.BThomeY = wx.Button(self.pronPan.subpanControl.spHOME, -1, 'Y',pos=(0,0),size=(35, 25),name="BThomeY" )
        self.pronPan.subpanControl.spHOME.BThomeY.SetBackgroundColour( ( 110, 110, 255 ) )
        self.pronPan.subpanControl.spHOME.BThomeY.SetForegroundColour( ( 0, 0, 255 ) )

        self.pronPan.subpanControl.spHOME.BThomeX = wx.Button(self.pronPan.subpanControl.spHOME, -1, 'X',pos=(34,0),size=(35, 25),name="BThomeX" )
        self.pronPan.subpanControl.spHOME.BThomeX.SetBackgroundColour( ( 255,255,51 ) )
        self.pronPan.subpanControl.spHOME.BThomeX.SetForegroundColour( ( 100, 100, 0) )

        self.pronPan.subpanControl.spHOME.BThomeZ = wx.Button(self.pronPan.subpanControl.spHOME, -1, 'Z',pos=(68,0),size=(35, 25),name="BThomeZ" )
        self.pronPan.subpanControl.spHOME.BThomeZ.SetBackgroundColour( ( 255,139,34 ) )
        self.pronPan.subpanControl.spHOME.BThomeZ.SetForegroundColour( ( 0, 100, 0) )

        #SubSubpaneles de botones de movimiento
        #eje X
        self.pronPan.subpanControl.spBTX=wx.Panel(self.pronPan.subpanControl, -1, pos=(42,320), size=(214,27),style=wx.SIMPLE_BORDER)

        self.pronPan.subpanControl.spBTX.STextX=wx.StaticText(self.pronPan.subpanControl.spBTX, -1, 'X', (100, 0))
        self.pronPan.subpanControl.spBTX.STextX.SetFont(wx.Font(16, wx.FONTFAMILY_ROMAN, wx.NORMAL, wx.NORMAL))
        self.pronPan.subpanControl.spBTX.STextX.SetForegroundColour((153,153,0))

        self.pronPan.subpanControl.spBTX.BTMas01 = wx.Button(self.pronPan.subpanControl.spBTX, -1, '.1',pos=(117,0),size=(25, 25),name="BTXM01" )
        self.pronPan.subpanControl.spBTX.BTMas01.SetBackgroundColour( ( 255,255,204 ) )
        self.pronPan.subpanControl.spBTX.BTMas01.SetForegroundColour( ( 100, 100, 0 ) )

        self.pronPan.subpanControl.spBTX.BTMas1 = wx.Button(self.pronPan.subpanControl.spBTX, -1, '1',pos=(140,0),size=(25, 25), name="BTXM1" )
        self.pronPan.subpanControl.spBTX.BTMas1.SetBackgroundColour( ( 255,255,153 ) )
        self.pronPan.subpanControl.spBTX.BTMas1.SetForegroundColour( ( 100, 100, 0 ) )

        self.pronPan.subpanControl.spBTX.BTMas10 = wx.Button(self.pronPan.subpanControl.spBTX, -1, '10',pos=(163,0),size=(25, 25), name="BTXM10" )
        self.pronPan.subpanControl.spBTX.BTMas10.SetFont(wx.Font(8, wx.FONTFAMILY_ROMAN, wx.NORMAL, wx.NORMAL))
        self.pronPan.subpanControl.spBTX.BTMas10.SetBackgroundColour( ( 255,255,102 ) )
        self.pronPan.subpanControl.spBTX.BTMas10.SetForegroundColour( ( 100, 100, 0 ) )

        self.pronPan.subpanControl.spBTX.BTMas100 = wx.Button(self.pronPan.subpanControl.spBTX, -1, '100',pos=(186,0),size=(25, 25), name="BTXM100" )
        self.pronPan.subpanControl.spBTX.BTMas100.SetFont(wx.Font(5, wx.FONTFAMILY_ROMAN, wx.NORMAL, wx.NORMAL))
        self.pronPan.subpanControl.spBTX.BTMas100.SetBackgroundColour( ( 255,255,51 ) )
        self.pronPan.subpanControl.spBTX.BTMas100.SetForegroundColour( ( 100, 100, 0 ) )


        self.pronPan.subpanControl.spBTX.BTMen01 = wx.Button(self.pronPan.subpanControl.spBTX, -1, '.1',pos=(70,0),size=(25, 25), name="BTXm01" )
        self.pronPan.subpanControl.spBTX.BTMen01.SetBackgroundColour( ( 255,255,204 ) )
        self.pronPan.subpanControl.spBTX.BTMen01.SetForegroundColour( ( 100, 100, 0 ) )

        self.pronPan.subpanControl.spBTX.BTMen1 = wx.Button(self.pronPan.subpanControl.spBTX, -1, '1',pos=(47,0),size=(25, 25), name="BTXm1" )
        self.pronPan.subpanControl.spBTX.BTMen1.SetBackgroundColour( ( 255,255,153 ) )
        self.pronPan.subpanControl.spBTX.BTMen1.SetForegroundColour( ( 100, 100, 0 ) )

        self.pronPan.subpanControl.spBTX.BTMen10 = wx.Button(self.pronPan.subpanControl.spBTX, -1, '10',pos=(24,0),size=(25, 25), name="BTXm10" )
        self.pronPan.subpanControl.spBTX.BTMen10.SetFont(wx.Font(8, wx.FONTFAMILY_ROMAN, wx.NORMAL, wx.NORMAL))
        self.pronPan.subpanControl.spBTX.BTMen10.SetBackgroundColour( ( 255,255,102 ) )
        self.pronPan.subpanControl.spBTX.BTMen10.SetForegroundColour( ( 100, 100, 0 ) )

        self.pronPan.subpanControl.spBTX.BTMen100 = wx.Button(self.pronPan.subpanControl.spBTX, -1, '100',pos=(1,0),size=(25, 25), name="BTXm100" )
        self.pronPan.subpanControl.spBTX.BTMen100.SetFont(wx.Font(5, wx.FONTFAMILY_ROMAN, wx.NORMAL, wx.NORMAL))
        self.pronPan.subpanControl.spBTX.BTMen100.SetBackgroundColour( ( 255,255,51 ) )
        self.pronPan.subpanControl.spBTX.BTMen100.SetForegroundColour( ( 100, 100, 0 ) )

        #eje Y
        self.pronPan.subpanControl.spBTY=wx.Panel(self.pronPan.subpanControl, -1, pos=(1,137), size=(28,210),style=wx.SIMPLE_BORDER)

        self.pronPan.subpanControl.spBTY.STextY=wx.StaticText(self.pronPan.subpanControl.spBTY, -1, 'Y', (8, 94))
        self.pronPan.subpanControl.spBTY.STextY.SetFont(wx.Font(14, wx.FONTFAMILY_ROMAN, wx.NORMAL, wx.NORMAL))
        self.pronPan.subpanControl.spBTY.STextY.SetForegroundColour((0,0,255))

        self.pronPan.subpanControl.spBTY.BTMas01 = wx.Button(self.pronPan.subpanControl.spBTY, -1, '.1',pos=(1,70),size=(25, 25), name="BTYM01" )
        self.pronPan.subpanControl.spBTY.BTMas01.SetBackgroundColour( ( 200, 200, 255 ) )
        self.pronPan.subpanControl.spBTY.BTMas01.SetForegroundColour( ( 0, 0, 255 ) )

        self.pronPan.subpanControl.spBTY.BTMas1 = wx.Button(self.pronPan.subpanControl.spBTY, -1, '1',pos=(1,47),size=(25, 25), name="BTYM1" )
        self.pronPan.subpanControl.spBTY.BTMas1.SetBackgroundColour( ( 170, 170, 255 ) )
        self.pronPan.subpanControl.spBTY.BTMas1.SetForegroundColour( ( 0, 0, 255 ) )

        self.pronPan.subpanControl.spBTY.BTMas10 = wx.Button(self.pronPan.subpanControl.spBTY, -1, '10',pos=(1,24),size=(25, 25), name="BTYM10" )
        self.pronPan.subpanControl.spBTY.BTMas10.SetFont(wx.Font(8, wx.FONTFAMILY_ROMAN, wx.NORMAL, wx.NORMAL))
        self.pronPan.subpanControl.spBTY.BTMas10.SetBackgroundColour( ( 140, 140, 255 ) )
        self.pronPan.subpanControl.spBTY.BTMas10.SetForegroundColour( ( 0, 0, 255 ) )

        self.pronPan.subpanControl.spBTY.BTMas100 = wx.Button(self.pronPan.subpanControl.spBTY, -1, '100',pos=(1,0),size=(25, 25), name="BTYM100" )
        self.pronPan.subpanControl.spBTY.BTMas100.SetFont(wx.Font(5, wx.FONTFAMILY_ROMAN, wx.NORMAL, wx.NORMAL))
        self.pronPan.subpanControl.spBTY.BTMas100.SetBackgroundColour( ( 110, 110, 255 ) )
        self.pronPan.subpanControl.spBTY.BTMas100.SetForegroundColour( ( 0, 0, 255 ) )

        self.pronPan.subpanControl.spBTY.BTMen01 = wx.Button(self.pronPan.subpanControl.spBTY, -1, '.1',pos=(1,114),size=(25, 25), name="BTYm01" )
        self.pronPan.subpanControl.spBTY.BTMen01.SetBackgroundColour( ( 200, 200, 255 ) )
        self.pronPan.subpanControl.spBTY.BTMen01.SetForegroundColour( ( 0, 0, 255 ) )

        self.pronPan.subpanControl.spBTY.BTMen1 = wx.Button(self.pronPan.subpanControl.spBTY, -1, '1',pos=(1,137),size=(25, 25), name="BTYm1" )
        self.pronPan.subpanControl.spBTY.BTMen1.SetBackgroundColour( ( 170, 170, 255 ) )
        self.pronPan.subpanControl.spBTY.BTMen1.SetForegroundColour( ( 0, 0, 255 ) )

        self.pronPan.subpanControl.spBTY.BTMen10 = wx.Button(self.pronPan.subpanControl.spBTY, -1, '10',pos=(1,160),size=(25, 25), name="BTYm10" )
        self.pronPan.subpanControl.spBTY.BTMen10.SetFont(wx.Font(8, wx.FONTFAMILY_ROMAN, wx.NORMAL, wx.NORMAL))
        self.pronPan.subpanControl.spBTY.BTMen10.SetBackgroundColour( ( 140, 140, 255 ) )
        self.pronPan.subpanControl.spBTY.BTMen10.SetForegroundColour( ( 0, 0, 255 ) )

        self.pronPan.subpanControl.spBTY.BTMen100 = wx.Button(self.pronPan.subpanControl.spBTY, -1, '100',pos=(1,183),size=(25, 25), name="BTYm100" )
        self.pronPan.subpanControl.spBTY.BTMen100.SetFont(wx.Font(5, wx.FONTFAMILY_ROMAN, wx.NORMAL, wx.NORMAL))
        self.pronPan.subpanControl.spBTY.BTMen100.SetBackgroundColour( ( 110, 110, 255 ) )
        self.pronPan.subpanControl.spBTY.BTMen100.SetForegroundColour( ( 0, 0, 255 ) )

        #eje Z
        self.pronPan.subpanControl.spBTZ=wx.Panel(self.pronPan.subpanControl, -1, pos=(269,172), size=(28,175),style=wx.SIMPLE_BORDER)

        self.pronPan.subpanControl.spBTZ.STextZ=wx.StaticText(self.pronPan.subpanControl.spBTZ, -1, 'Z', (8, 75))
        self.pronPan.subpanControl.spBTZ.STextZ.SetFont(wx.Font(16, wx.FONTFAMILY_ROMAN, wx.NORMAL, wx.NORMAL))
        self.pronPan.subpanControl.spBTZ.STextZ.SetForegroundColour((0,100,0))

        self.pronPan.subpanControl.spBTZ.BTMas01 = wx.Button(self.pronPan.subpanControl.spBTZ, -1, '.1',pos=(1,50),size=(25, 25), name="BTZM01" )
        self.pronPan.subpanControl.spBTZ.BTMas01.SetBackgroundColour( ( 173,255,47 ) )
        self.pronPan.subpanControl.spBTZ.BTMas01.SetForegroundColour( ( 0, 100, 0 ) )

        self.pronPan.subpanControl.spBTZ.BTMas1 = wx.Button(self.pronPan.subpanControl.spBTZ, -1, '1',pos=(1,27),size=(25, 25), name="BTZM1" )
        self.pronPan.subpanControl.spBTZ.BTMas1.SetBackgroundColour( ( 50,205,50 ) )
        self.pronPan.subpanControl.spBTZ.BTMas1.SetForegroundColour( ( 0, 100, 0 ) )

        self.pronPan.subpanControl.spBTZ.BTMas10 = wx.Button(self.pronPan.subpanControl.spBTZ, -1, '10',pos=(1,4),size=(25, 25), name="BTZM10" )
        self.pronPan.subpanControl.spBTZ.BTMas10.SetFont(wx.Font(8, wx.FONTFAMILY_ROMAN, wx.NORMAL, wx.NORMAL))
        self.pronPan.subpanControl.spBTZ.BTMas10.SetBackgroundColour( ( 34,139,34 ) )
        self.pronPan.subpanControl.spBTZ.BTMas10.SetForegroundColour( ( 0, 100, 0 ) )

        self.pronPan.subpanControl.spBTZ.BTMen01 = wx.Button(self.pronPan.subpanControl.spBTZ, -1, '.1',pos=(1,99),size=(25, 25), name="BTZm01" )
        self.pronPan.subpanControl.spBTZ.BTMen01.SetBackgroundColour( ( 173,255,47  ) )
        self.pronPan.subpanControl.spBTZ.BTMen01.SetForegroundColour( ( 0, 100, 0 ) )

        self.pronPan.subpanControl.spBTZ.BTMen1 = wx.Button(self.pronPan.subpanControl.spBTZ, -1, '1',pos=(1,122),size=(25, 25), name="BTZm1" )
        self.pronPan.subpanControl.spBTZ.BTMen1.SetBackgroundColour( ( 50,205,50  ) )
        self.pronPan.subpanControl.spBTZ.BTMen1.SetForegroundColour( ( 0, 100, 0 ) )

        self.pronPan.subpanControl.spBTZ.BTMen10 = wx.Button(self.pronPan.subpanControl.spBTZ, -1, '10',pos=(1,145),size=(25, 25), name="BTZm10" )
        self.pronPan.subpanControl.spBTZ.BTMen10.SetFont(wx.Font(8, wx.FONTFAMILY_ROMAN, wx.NORMAL, wx.NORMAL))
        self.pronPan.subpanControl.spBTZ.BTMen10.SetBackgroundColour( ( 34,139,34 ) )
        self.pronPan.subpanControl.spBTZ.BTMen10.SetForegroundColour( ( 0, 100, 0 ) )


##BINDS#################################################################################################################
        self.pronPan.TCprotoIN.Bind(wx.EVT_TEXT_ENTER, self.pronSend)
        # self.pronPanTCprotoIN.Bind(wx.EVT_KEY_DOWN, self.pronOnKeyDown)
        self.pronPan.TCprotoIN.Bind(wx.EVT_KEY_UP, self.pronOnKeyUp)
        self.pronPan.subpanControl.BTconectar.Bind(wx.EVT_BUTTON, self.pronOnConectar)
        # self.pronPan.subpanControl.BTmaxFeedRates.Bind(wx.EVT_BUTTON, self.pronInfoMaxFeedRates)
        self.pronPan.subpanControl.spFRxyz.boxFRxy.Bind(wx.EVT_TEXT_ENTER, self.pronOnManualFeedRates)
        self.pronPan.subpanControl.spFRxyz.boxFRz.Bind(wx.EVT_TEXT_ENTER, self.pronOnManualFeedRates)
        self.pronPan.subpanControl.BTpos.Bind(wx.EVT_BUTTON, self.pronInfoPos)
        self.pronPan.subpanControl.TGCheckTemp.Bind(wx.EVT_TOGGLEBUTTON, self.pronCheckTemp)


        controlHomes=self.pronPan.subpanControl.spHOME.GetChildren()
        for auxi in controlHomes:
            if auxi.GetName().startswith("BThome"):
                auxi.Bind(wx.EVT_BUTTON, self.pronPanHomes)


        controlsX=self.pronPan.subpanControl.spBTX.GetChildren()
        for auxi in controlsX:
            if auxi.GetName().startswith("BTXM") or auxi.GetName().startswith("BTXm"):
                auxi.Bind(wx.EVT_BUTTON, self.pronPanMove)

        controlsY=self.pronPan.subpanControl.spBTY.GetChildren()
        for auxi in controlsY:
            if auxi.GetName().startswith("BTYM") or auxi.GetName().startswith("BTYm"):
                auxi.Bind(wx.EVT_BUTTON, self.pronPanMove)

        controlsZ=self.pronPan.subpanControl.spBTZ.GetChildren()
        for auxi in controlsZ:
            if auxi.GetName().startswith("BTZM") or auxi.GetName().startswith("BTZm"):
                auxi.Bind(wx.EVT_BUTTON, self.pronPanMove)


########## FIN PANEL PRONSOLE

########## FIN INIT

########################################################################################################################
    def updateComboPort(self,event):
        if self.pronPan.subpanControl.comboPorts.GetCurrentSelection() is not wx.NOT_FOUND:
            self.pronPort=self.pronPan.subpanControl.comboPorts.GetStringSelection()

    def updateComboBaudrate(self,event):
        if self.pronPan.subpanControl.comboBaudrate.GetCurrentSelection() is not wx.NOT_FOUND:
            self.pronBaudrate=self.pronPan.subpanControl.comboBaudrate.GetStringSelection()

    def serial_ports(self):
        # CODE: http://stackoverflow.com/questions/12090503/listing-available-com-ports-with-python
        """ Lists serial port names

            :raises EnvironmentError:
                On unsupported or unknown platforms
            :returns:
                A list of the serial ports available on the system
        """
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')

        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        return result

    def pronKILL(self,event):
        #todo NOTA IMPORTANTE: M112 NO SE EJECUTARÁ HASTA TERMINAR LOS MOVIMIENTOS QUE SEAN PRECEDIDOS POR M400.
        #todo COMO EL M400 es necesario para obtener la señal de la culminación de un movimiento, convendrá partir a los
        #todo movimientos en pequeñas partes. De esa forma el M112 podrá ser tenido en cuenta
        self.pronSend(wx.EVT_BUTTON,"M112")#KILL

    def pronSetM400(self,event):
        objValue=self.pronPan.subpanControl.spGCODES.TGPronAddM400.GetValue()
        if objValue:
            self.pronPan.subpanControl.spGCODES.TGPronAddM400.SetLabel("M400on")
        else:
            self.pronPan.subpanControl.spGCODES.TGPronAddM400.SetLabel("M400ff")
        self.pronAddM400=objValue

    def pronCheckTemp(self,event):
        objValue=self.pronPan.subpanControl.TGCheckTemp.GetValue()
        if objValue:
            self.pronPan.subpanControl.TGCheckTemp.SetLabel("UnChkTemp")
            self.pronSend(wx.EVT_BUTTON, "statuscheckON")
        else:
            self.pronPan.subpanControl.TGCheckTemp.SetLabel("ChkTemp")
            self.pronSend(wx.EVT_BUTTON, "statuscheckOFF")
        self.pronCheckTempValue=objValue



    def pronSetG28_OK(self):
        if self.pronProtProc.protoG28_OK:
            self.pronG28_OK=True
            self.pronPan.subpanControl.spHOME.BThomeZ.SetBackgroundColour( ( 34,139,34 ) )
        else:
            self.pronG28_OK=False
            self.pronPan.subpanControl.spHOME.BThomeZ.SetBackgroundColour( ( 255,139,34 ) )


    def pronCheckEndM400(self,pathAndFile,AddM400=False):#recibe el path completo, incluyendo el nombre del archivo,
    # y revisa si al final existe el comando M400. En caso de no estar presente, si AddM400 es True, entonces se
    # agregará M400 al código, y si es False, sólo habrá alertas por mensaje
        EndsInM400=False
        #1- Abrir el archivo
        with open(pathAndFile, "r+") as inputfile:
            aux=inputfile.readlines()
        inputfile.close()
        #2- Leer líneas desde la última hacia la primera
        for linea in reversed(aux):#recorrer desde la última hacia la primera
            if linea=='\n' or linea=='':
                pass
            elif linea.startswith("M400"):
                EndsInM400=True
                break
            else:
                break

        if not EndsInM400:
            if AddM400:
                with open(pathAndFile, "a") as outputfile:#abrir en modo append, eso lleva el puntero al final
                    outputfile.write( "\n");#enviar un enter por las dudas. Lo peor q puede pasar es q se genere una
                    # línea en blanco, pero no será tenida en cuenta al hacer load
                    outputfile.write( "M400");#agregar M400
                outputfile.close()

            else:
                msg = "Los GCODEs deben terminar en M400. Revise el archivo : "+ '\n' +pathAndFile
                dlg = wx.MessageDialog(self, msg, 'GCODEs sin M400', wx.OK|wx.ICON_INFORMATION)
                dlg.ShowModal()
                dlg.Destroy()

    def pronRunGCODEActual(self,event):
        if self.pronGCODESactual != " " and self.pronProtProc.conectado and self.pronProtProc.online:
            if not self.pronProtProc.protoG28_OK:
                try:
                    val = self.pronPan.TCprotoOUT.GetValue()
                    self.pronPan.TCprotoOUT.SetValue(val + "REALICE HOME A Z (G28) ANTES DE EJECUTAR UN GCODE" + "\n" )
                    self.pronPan.TCprotoOUT.SetInsertionPointEnd()
                except:
                    pass
                else:
                    return

            pathAndFile=self.pronPathGCODES+"/"+self.pronGCODESactual
            self.pronCheckEndM400(pathAndFile,self.pronAddM400)

            gcode="load "
            # path=self.pronPathGCODES
            # file=self.pronGCODESactual
            gcode=gcode + pathAndFile
            self.pronSend(wx.EVT_BUTTON,gcode)

    def pronUpdateGCODESlistYCombo(self):
        try:
            self.pronGCODESlist = [f for f in os.listdir(self.pronPathGCODES) if f.endswith('.gcode')]
        except:
            self.pronGCODESlist=['homeX.gcode']
        self.pronGCODESlist.sort()
        self.pronPan.subpanControl.spGCODES.comboGCODES.Clear()
        if self.pronGCODESlist != []:
            self.pronPan.subpanControl.spGCODES.comboGCODES.AppendItems(self.pronGCODESlist)
        self.pronPan.subpanControl.spGCODES.comboGCODES.SetValue("GCODES")#ESTO LO PONGO DE TODAS FORMAS SIEMPRE
        self.pronGCODESactual=" "#COMO NO HAY UN SELECTO, EL ACTUAL DEBE ESTAR VACÍO

    def pronOnSelectGCODESFolder(self, event):
        pathAux = wx.DirSelector(message='Directorio donde se encuentran los archivos gcode?',
                              defaultPath=self.pronPathGCODES)
        if pathAux:#Se chequea si se retornó un empty return  (el usuario presionó Cancel)
            self.pronPathGCODES=pathAux
            self.pronPan.subpanControl.spGCODES.boxPathGCODES.SetValue(self.pronPathGCODES)
            self.pronUpdateGCODESlistYCombo()

    def pronUpdateGCODESactual(self,event):
        if self.pronPan.subpanControl.spGCODES.comboGCODES.GetCurrentSelection() is not wx.NOT_FOUND:
            self.pronGCODESactual=self.pronPan.subpanControl.spGCODES.comboGCODES.GetStringSelection()

    def pronPanHomes(self,event):
        objName=event.GetEventObject().GetName()
        gcode="home "
        eje=""
        if objName=="BThomeX":
            eje="X"
        elif objName=="BThomeY":
            eje="Y"
        elif objName=="BThomeZ":
            gcode="G28"
            self.pronSend(wx.EVT_BUTTON,gcode)
            #NOTA: originalmente, para que el sistema estuviera listo, debía recibir G28_OK desde Marlin. Esto implica
            # modificar el firmware para que envíe G28_OK al hacer homing de Z. Decido sacarlo, y se supone que el
            # usuario estará al tanto de ese homing en Z.
            self.pronProtProc.protoG28_OK = True
            self.pronSetG28_OK()
            return
        gcode=gcode + eje
        self.pronSend(wx.EVT_BUTTON,gcode)

    def pronPanMove(self,event):
        objName=event.GetEventObject().GetName()
        gcode="move "
        eje=""
        valor=""
        FR=self.pronManualFeedRates[0]#este feedRate se cambia solo si se mueve Z

        if objName.startswith("BTXM"):
            eje="X "
            cuanto=objName[4:]
            if cuanto=="01":
                valor="0.1"
            else:
                valor=cuanto
        elif objName.startswith("BTXm"):
            if not self.pronProtProc.protoG28_OK:
                try:
                    val = self.pronPan.TCprotoOUT.GetValue()
                    self.pronPan.TCprotoOUT.SetValue(val + "Do Z Home(G28)" + "\n" )
                    self.pronPan.TCprotoOUT.SetInsertionPointEnd()
                except:
                    pass
                else:
                    return

            eje="X "
            cuanto=objName[4:]
            if cuanto=="01":
                valor="-0.1"
            else:
                valor="-"+cuanto

        elif objName.startswith("BTYM"):
            eje="Y "
            cuanto=objName[4:]
            if cuanto=="01":
                valor="0.1"
            else:
                valor=cuanto
        elif objName.startswith("BTYm"):
            if not self.pronProtProc.protoG28_OK:
                try:
                    val = self.pronPan.TCprotoOUT.GetValue()
                    self.pronPan.TCprotoOUT.SetValue(val + "Do Z Home(G28)" + "\n" )
                    self.pronPan.TCprotoOUT.SetInsertionPointEnd()
                except:
                    pass
                else:
                    return


            eje="Y "
            cuanto=objName[4:]
            if cuanto=="01":
                valor="-0.1"
            else:
                valor="-"+cuanto

        elif objName.startswith("BTZM"):
            FR=self.pronManualFeedRates[2]
            eje="Z "
            cuanto=objName[4:]
            if cuanto=="01":
                valor="0.1"
            else:
                valor=cuanto
        elif objName.startswith("BTZm"):
            if not self.pronProtProc.protoG28_OK:
                try:
                    val = self.pronPan.TCprotoOUT.GetValue()
                    self.pronPan.TCprotoOUT.SetValue(val + "Do Z Home(G28)" + "\n" )
                    self.pronPan.TCprotoOUT.SetInsertionPointEnd()
                except:
                    pass
                else:
                    return

            FR=self.pronManualFeedRates[2]
            eje="Z "
            cuanto=objName[4:]
            if cuanto=="01":
                valor="-0.1"
            else:
                valor="-"+cuanto

        gcode=gcode + eje +valor +" " + str(FR)
        self.pronSend(wx.EVT_BUTTON,gcode)


    def pronUpdateCommHist(self,entry):
        #En el momento de un agregado, no se están usando las flechas Up y Down para ver comandos previos. Por eso se
        # reinicia self.pronCommHistActualItem a -1
        self.pronCommHistActualItem=-1
        #Ubicar la nueva entry en el primer lugar del pronCommHist
        auxList=[entry]
        self.pronCommHist=auxList+self.pronCommHist
        #Actualizar pronCommHistNItems
        self.pronCommHistNItems=self.pronCommHistNItems+1
        # Corroborar que no haya más items que pronCommHistMaxItems
        if self.pronCommHistNItems>self.pronCommHistMaxItems:
            del self.pronCommHist[self.pronCommHistMaxItems]#borrar el último
            self.pronCommHistNItems=self.pronCommHistNItems-1#disminuir la cuenta

    def pronOnKeyUp(self,event):
        keycode = event.GetKeyCode()
        if keycode == wx.WXK_UP:
            if self.pronCommHistActualItem<self.pronCommHistNItems-1:
                self.pronCommHistActualItem=self.pronCommHistActualItem+1
            else:
                self.pronCommHistActualItem=0#volver al primero

            if self.pronCommHist!=[]:
                self.pronPan.TCprotoIN.SetValue(self.pronCommHist[self.pronCommHistActualItem])
        elif keycode == wx.WXK_DOWN:
            if self.pronCommHistActualItem>0:
                self.pronCommHistActualItem=self.pronCommHistActualItem-1
            else:
                self.pronCommHistActualItem=self.pronCommHistNItems-1 #ir al último

            if self.pronCommHistActualItem>-1 and self.pronCommHist!=[]:
                self.pronPan.TCprotoIN.SetValue(self.pronCommHist[self.pronCommHistActualItem])
        else:
            event.Skip()
            return
        event.Skip()

    def pronSend(self,event,gcode=""):
        if self.pronTranspProc is not None:
            if gcode=="":
                aux=(self.pronPan.TCprotoIN.GetValue()).encode('ascii')
                #Agregar el comando aux en la CommandHistory
                self.pronUpdateCommHist(aux)
                #Borrar el comando
                self.pronPan.TCprotoIN.SetValue("")
                # self.pronTranspProc.writeToChild(0,aux +"\n")#ESTA TAMBIÉN FUNCIONARÍA
                # Fuente:
                # https://twistedmatrix.com/documents/current/api/twisted.internet.interfaces.IReactorProcess.html
                # The parent process can write to that file descriptor using IProcessTransport.writeToChild.
                # This is useful for the child's stdin.
                self.pronTranspProc.write(aux+"\n")
            elif gcode.startswith("M112"):
                self.pronTranspProc.write(gcode.encode('ascii')+"\n")
            elif gcode.startswith("move"):
                self.pronTranspProc.write(gcode.encode('ascii')+"\n")
            elif gcode.startswith("home"):
                self.pronTranspProc.write(gcode.encode('ascii')+"\n")
            elif gcode.startswith("load"):
                self.pronTranspProc.write(gcode.encode('ascii')+"\n")
                self.pronTranspProc.write("print"+"\n")
                self.pronUpdateMoviendo(True)
            elif gcode.startswith("G28"):
                self.pronTranspProc.write(gcode.encode('ascii')+"\n")
            elif gcode.startswith("statuscheckON"):
                self.pronTranspProc.write(gcode.encode('ascii')+"\n")
            elif gcode.startswith("statuscheckOFF"):
                self.pronTranspProc.write(gcode.encode('ascii')+"\n")
            elif gcode.startswith("debug_ON"):
                self.pronTranspProc.write(gcode.encode('ascii')+"\n")
            elif gcode.startswith("debug_OFF"):
                self.pronTranspProc.write(gcode.encode('ascii')+"\n")
            elif gcode.startswith("M114"):#posición
                self.pronTranspProc.write(gcode.encode('ascii')+"\n")


    def pronUpdateMoviendo(self,boolMoviendo):#True si está en movimiento
        self.pronMoviendo=boolMoviendo
        if self.pronMoviendo:
            self.pronPan.subpanControl.spGCODES.STextMoviendo.SetLabel("MOVING")
        else:
            if self.secConfig['secIs'] != 'Stopped':#Está en Sequence, Running o Paused
                wx.CallAfter(self.FinalSecProcess,['SEC',str(self.secConfig['FinalSecLineAct']),'OK'])
            self.pronPan.subpanControl.spGCODES.STextMoviendo.SetLabel("STOPPED")



    def pronOnConectar(self,event):
        objLabel=event.GetEventObject().GetLabel()
        if objLabel=='Go!':
            if self.pronPort=="NO PORT":
                return
            event.GetEventObject().SetLabel("Bye!")
            pythonPath=sys.executable
            pythonENV=os.environ
            #En pythonPath estará el path hacia python.

            #NOTA: Si se quiere hacer un bundle ejecutable, hay problemas con la impresora:
            # Sin embargo, hay que distinguir si Python está interpretando el
            # código, o si la aplicación es un bundle:
            # NO BUNDLE: sys.executable es en sí Python
            # BUNDLE: sys.executable no es en sí Python, sino el ejecutable en el cual está embebido Python (DIOS)
            # Como prosole debe ser ejecutado por Python, y no encontré cómo hacer una referencia al Python embebido en
            # lugar del nombre del ejecutable, entonces spawnProcess abre una nueva instancia de la GUI de DIOS al
            # querer interpretar a Pronsole.


            # SOURCE: https://pyinstaller.readthedocs.io/en/stable/runtime-information.html#run-time-information
            if getattr(sys, 'frozen', False):
                # we are running in a bundle
                if os.name == "posix":
                    pythonPath = str(pythonENV['PYTHONHOME'])+"/python"
                else:
                    pythonPath = str(pythonENV['PYTHONHOME']) + "\python.exe"
            else:
                # we are running in a normal Python environment
                pass


            # self.pronProtProc = ProtocoloProcesoPronsole.ProtocoloProcesoPronsole(self)
            self.pronProtProc = ProtocoloProcesoPronsole(self)

            self.pronTranspProc=reactor.spawnProcess(self.pronProtProc, pythonPath, [pythonPath, "pronsole.py"], pythonENV, self.pronPathPrintrun)
            # self.pronTranspProc=reactor.spawnProcess(self.pronProtProc, pythonPath, [pythonPath, "pronsole.py"], {'DISPLAY': os.environ['DISPLAY']}, self.pronPathPrintrun)

            while self.pronTranspProc is None:
                pass
            connect_port_baudrate="connect " + str(self.pronPort) + " " + str(self.pronBaudrate)
            self.pronTranspProc.write(connect_port_baudrate+"\n")


        elif objLabel=='Bye!':
            event.GetEventObject().SetLabel("Go!")
            if self.pronTranspProc is not None:
                self.pronTranspProc.write("disconnect"+"\n")
                self.pronTranspProc.write("exit"+"\n")
                self.pronTranspProc=None
                self.pronProtProc.protoGotSettings=False
                # self.pronProtProc.protoGotMaxFeedRates=False
                self.pronProtProc.protoG28_OK=False
                self.pronSetG28_OK()

    def pronAdaptMaxFeedRates(self,listMaxFRxyz):
    # NOTA: Esta función no era ejecutada desde esta gui, sino desde ProtocoloProcesoPronsole, una vez que ya se habían
    # solicitado los MaxFeedRates y se había obtenido respuesta. Por haber tenido algunos problemas, no la uso. Queda
    # A CARGO DEL USUARIO NO SUPERAR LOS MAX FEED RATES DE SU IMPRESORA

    #Esta función no se supone que sea para cambiar los max feed rates de
    # la impresora, sino para que los valores por defecto en la gui, en caso de ser mayores a los reales permitidos por
    # la impresora, sean corregidos
    #NOTA: es posible que los maxFeedRates sean muy altos. Aun cdo los movimientos a esas velocidades estén técnicamente
    # permitidos, no los actualizaré si son mayores a los impuestos en el init para self.pronMaxFeedRates
    #Los valores están en mm/s, son pasados a mm/min
        cuenta=0
        for maxFR in listMaxFRxyz:
            mmPorMin=maxFR*60
            if self.pronMaxFeedRates[cuenta]>mmPorMin:
                self.pronMaxFeedRates=mmPorMin
            cuenta=cuenta+1
        #A su vez, es posible que los valores manuales no sean correctos una vez actualizados los máximos.
        FRxyManual=self.pronPan.subpanControl.spFRxyz.boxFRxy.GetValue()
        if int(FRxyManual)>self.pronMaxFeedRates[0]:
            self.pronPan.subpanControl.spFRxyz.boxFRxy.SetValue(str(self.pronMaxFeedRates[0]))
            self.pronManualFeedRates[0]=self.pronMaxFeedRates[0]
            self.pronManualFeedRates[1]=self.pronMaxFeedRates[1]
        FRzManual=self.pronPan.subpanControl.spFRxyz.boxFRz.GetValue()
        if int(FRzManual)>self.pronMaxFeedRates[2]:
            self.pronPan.subpanControl.spFRxyz.boxFRz.SetValue(str(self.pronMaxFeedRates[2]))
            self.pronManualFeedRates[2]=self.pronMaxFeedRates[2]


    # def pronInfoMaxFeedRates(self,event):
    #     linea=str(self.pronMaxFeedRates[0])+" "+str(self.pronMaxFeedRates[1])+" "+str(self.pronMaxFeedRates[2])
    #     try:
    #         val = self.pronPan.TCprotoOUT.GetValue()
    #         self.pronPan.TCprotoOUT.SetValue(val + linea + "\n" )
    #         self.pronPan.TCprotoOUT.SetInsertionPointEnd()
    #     except:
    #         pass

    def pronInfoPos(self,event):
        gcode="M114"
        self.pronSend(event,gcode)

    def pronOnManualFeedRates(self,event):
        #Identificar si es XY o Z
        objName=event.GetEventObject().GetName()
        if objName.startswith("boxFRxy"):
            feedRate= self.pronPan.subpanControl.spFRxyz.boxFRxy.GetValue()
            feedRate=feedRate.strip(' ')
            #Genero una lista, aunque sólo debería haber 1 valor. Si hay más, no serán analizados y serán descartados.
            feedRatelist = feedRate.split ()
            if feedRatelist[0].isdigit() and int(feedRatelist[0])!=0 and int(feedRatelist[0])<=self.pronMaxFeedRates[0]:#que sea dígito, que no
            # sea 0, y que no sea mayor al máximo permitido
                self.pronManualFeedRates[0]=int(feedRatelist[0])#X
                self.pronManualFeedRates[1]=int(feedRatelist[0])#Y
                self.pronPan.subpanControl.spFRxyz.boxFRxy.SetValue(str(int(feedRatelist[0])))
                #Actualizar valor en mm/s
                self.pronPan.subpanControl.spFRxyz.STextFRXY.SetLabel(str(round(float(self.pronManualFeedRates[0])/60.0,2))+' mm/s (XY)')
            else:
                self.pronPan.subpanControl.spFRxyz.boxFRxy.SetValue(str(self.pronManualFeedRates[0]))
        elif objName.startswith("boxFRz"):
            feedRate= self.pronPan.subpanControl.spFRxyz.boxFRz.GetValue()
            feedRate=feedRate.strip(' ')
            #Genero una lista, aunque sólo debería haber 1 valor. Si hay más, no serán analizados y serán descartados.
            feedRatelist = feedRate.split ()
            if feedRatelist[0].isdigit() and int(feedRatelist[0])!=0 and int(feedRatelist[0])<=self.pronMaxFeedRates[2]:#que sea dígito, que no
            # sea 0, y que no sea mayor al máximo permitido
                self.pronManualFeedRates[2]=int(feedRatelist[0])#Z
                self.pronPan.subpanControl.spFRxyz.boxFRz.SetValue(str(int(feedRatelist[0])))
                #Actualizar valor en mm/s
                self.pronPan.subpanControl.spFRxyz.STextFRZ.SetLabel(str(round(float(self.pronManualFeedRates[2])/60.0,2))+' mm/s (Z)')
            else:
                self.pronPan.subpanControl.spFRxyz.boxFRz.SetValue(str(self.pronManualFeedRates[2]))



    def pronSetActPosi(self,x,y,z):
        self.pronPosition=[float(x), float(y), float(z)]

    def pronSetActTemperature(self,THE,TB):
        self.pronTemperatures=[float(THE), float(TB)]

#########################END PRONSOLE COMMANDS########################################################################
    def notifClear(self):  # Borra el contenido de self.notificaciones
        self.notificaciones.SetValue("")

    def notifAddNotif(self, newNotif):  # Agrega notificación en self.notificaciones
        # newNotif es un string con una nueva notificación
        self.notificaciones.SetValue(self.notificaciones.GetValue() + '\n' + str(newNotif))


    def panSecOnDordOpenFile(self, event):
        openFileDialog = wx.FileDialog(self, "Open DORD file", "", "",
                               "DORD files (*.dord)|*.dord", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        openFileDialog.SetDirectory(os.getcwd())

        if openFileDialog.ShowModal() == wx.ID_CANCEL:
            return

        # leer archivo
        archivo= open(openFileDialog.GetPath(), "r")
        lista=archivo.readlines()
        listaDelays=[]
        for linea in lista:
            if linea=="" or linea==" " or linea=="\n" :
                break
            listaDelays.append(int(linea.rstrip('\r\n')))

        #almacenar propiedades del archivo en secDOrdActual
        self.secConfig['secDOrdActual']=[openFileDialog.GetFilename().encode('ascii'),openFileDialog.GetDirectory().encode('ascii'),listaDelays]
        #actualizar box Dord
        self.panSec.subpanControl.boxDOrd.SetValue(openFileDialog.GetFilename().encode('ascii'))


    def panSecOnSecOpen(self, event):
        openFileDialog = wx.FileDialog(self, "Open SEC file", "", "",
                               "SEC files (*.sec)|*.sec", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        openFileDialog.SetDirectory(os.getcwd())

        if openFileDialog.ShowModal() == wx.ID_CANCEL:
            return
        # borrar Sequence actual (y dentro, parámetros de config en secConfig)
        self.panSecTreeOnClear(wx.EVT_BUTTON)

        # leer archivo
        archivo= open(openFileDialog.GetPath(), "r")
        lista=archivo.readlines()

        if len(lista)==0:
            return

        #Convertir lista a diccionario, y también revisar texto para configurar parámetros de Sequence
        log={}
        nSentencia=0
        for linea in lista:
            nSentencia+=1
            aux=linea.split("#") #linea: '2#LOOP3:4 5\n' , aux[0]=2 (nivel) aux[1]=LOOP3:4 5\n
            nivel = int(aux[0])
            texto = (aux[1]).rstrip('\r\n')

            log[nSentencia] = [nivel, texto]

            #Analizar texto
            #Hay LOOP?
            if texto.startswith("LOOP"):
                #Obtener Número de LOOP y order
                posi=texto.find(":")+1#posicion desde donde empieza order
                nLoop=int(texto[4:(posi-1)])
                orderAux=str(texto[posi:])
                #Actualizar parámetros y GUI
                self.secConfig['orderSTR']=orderAux
                self.panSec.subpanControl.boxOrder.SetValue(orderAux)
                #Registrar Loop
                self.registerNewKeyLoops(nLoop,orderAux)


            #Hay TCoords?
            #texto es algo como !DIOS:TCoordNN:MM
            elif texto.startswith("!DIOS:TC"):
                auxi=texto.split(":")
                # Encontrar cuál se quiere agregar (nTCoord) en función de si es TC, TCS:
                #NOTA: evaluar primero TCS (o cualquier nuevo tipo de TC que tenga más letras que TC)
                if auxi[1][0:3]=="TCS":
                    nTcoord = int(auxi[1][3:])
                elif auxi[1][0:2]=="TC":
                    nTcoord=int(auxi[1][2:])

                #Encontrar el tiempo de coordinación
                endT=int(auxi[2])
                #Registrar
                self.registerNewKeyTCoord(nTcoord,endT)
                # self.secConfig['secDictTCoords'][nTcoord]=[0,0, endT]
                # print self.secConfig['secDictTCoords']

        #A partir del diccionario, obtener tree
        tree=self.processDict2Tree(log,self.treeSec)
        self.treeSec=tree
        self.treeSec.ExpandAll()
        self.treeSec.SelectItem(self.treeSec.SequenceRoot, True)

    def panSecOnSecSave(self, event):
        #Obtener diccionario del tree
        log=self.processTree2Dict(self.treeSec)
        # print log
        totalSentencias=len(log.keys())
        if totalSentencias==0:#Si no hay elementos para salvar, salir
            return
        #Preparar texto a guardar
        lista=[]
        for nSentencia in range(1,totalSentencias+1):
            linea = str(log[nSentencia][0]) + "#" + str(log[nSentencia][1]) + '\n'
            lista.append(linea)
        saveFileDialog = wx.FileDialog(self, "Save SEC file", "", "",
                                   "SEC files (*.sec)|*.sec", wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)
        saveFileDialog.SetDirectory(os.getcwd())
        if saveFileDialog.ShowModal() == wx.ID_CANCEL:
                return
        # Controlar si se agregó .sec al final
        nombre=saveFileDialog.GetPath()
        if not nombre.endswith(".sec"):
            nombre=nombre+".sec"
        aux = open(nombre, "w+")#abrir como escritura y lectura
        aux.seek(0,0)
        aux.writelines(lista)
        aux.close()

    def panSecShowPopup(self):
        wx.MessageBox('Sequence ready?', 'Sequence ready?',
            wx.OK | wx.ICON_INFORMATION)

    # https: // wiki.wxpython.org / MessageBoxes
    def YesNo(parent, question, caption='Si o no?'):
        dlg = wx.MessageDialog(parent, question, caption, wx.YES_NO | wx.ICON_QUESTION)
        result = dlg.ShowModal() == wx.ID_YES
        dlg.Destroy()
        return result

    def panSecOnItemSelected(self, event):
        self.secConfig['currentItem'] = event.m_itemIndex

    def panSecGetNivelAct(self,selecto):
        for num in range(0,self.secConfig['nivelMax']+1):
            item = self.panSec.listSec.GetItem(selecto, num) #GetItem(row, col)
            aux=item.GetText()
            if aux!='':
                break
        return num


    def panSecOnChangeGCODEord(self, event):
        texto = (str(self.panSec.subpanControl.boxGCODEord.GetValue())).strip(' ')
        self.secConfig['secGCODEordActual']=""
        if not ("ORD" in texto or "IDX" in texto):
            dlg = wx.MessageDialog(self, 'GCODEords: Neither ORD nor IDX found in expression', 'GCODEord without ORD/IDX',
                                   wx.OK | wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()
            return

        self.secConfig['secGCODEordActual'] = texto

    def panSecOnChangeTypeText(self, event):
        pass

    def panSecSetTCoordActual(self,event):
        self.secConfig['TCoordActual']=int(self.panSec.subpanControl.boxTCoord.GetValue())
        # print self.secConfig['TCoordActual']

    def panSecOnChangeDOrd(self,event):
        texto = (str(self.panSec.subpanControl.boxDOrd.GetValue())).strip(' ')
        self.secConfig['secDOrdActual'] = ""
        if "ORD" in texto or "IDX" in texto:
            textoAux = texto.replace("ORD", str(1))#se reemplaza ORD con el entero 1 solo para evaluar si el resultado
            # es numérico
            textoAux = textoAux.replace("IDX", str(1))
            try:
                valor=eval(textoAux)
                self.secConfig['secDOrdActual'] = texto
            except:
                dlg = wx.MessageDialog(self, 'DOrds: Expression must result in a number', 'Wrong written DOrd', wx.OK | wx.ICON_INFORMATION)
                dlg.ShowModal()
                dlg.Destroy()
                return
        else:
            dlg = wx.MessageDialog(self, 'DOrds: Neither ORD nor IDX found in expression', 'DOrd without ORD/IDX',
                                   wx.OK | wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()
            return

    def panSecOnChangeOrder(self, event):
        order = self.panSec.subpanControl.boxOrder.GetValue()
        order=order.strip(' ')
        orderlist = order.split ()
        finalList=[]
        for num in orderlist:
            if num.isdigit() and int(num)!=0:#que sea dígito y no sea 0
                finalList.append(num)
        if len(finalList)==0:#no existen valores válidos
            self.secConfig['orderSTR']="1"
            self.panSec.subpanControl.boxOrder.SetValue("1")
        else:
            self.secConfig['orderSTR']=' '.join(finalList)
            self.panSec.subpanControl.boxOrder.SetValue(self.secConfig['orderSTR'])

    def panSecOnChangeDelay(self, event):
        delay = self.panSec.subpanControl.boxDelay.GetValue()
        delay=delay.strip(' ')
        #Genero una lista, aunque sólo debería haber 1 valor. Si hay más, no serán analizados y serán descartados.
        delaylist = delay.split ()
        if delaylist[0].isdigit() and int(delaylist[0])!=0:#que sea dígito y no sea 0
            self.secConfig['delaySTR']=str(delaylist[0])
            self.panSec.subpanControl.boxDelay.SetValue(self.secConfig['delaySTR'])
        else:
            self.secConfig['delaySTR']="0"
            self.panSec.subpanControl.boxDelay.SetValue("0")


    def processDict2Tree(self,dictTree, copyTree=[]):
        # Recibe un Dict del tipo: log[nSentencia]=[nivel, textoItem, item, cookie/None]
        # y en base a este elabora un tree
        # copyTree: si se envía uno, tree copiará sus parámetros (no su contenido). Sino, tree tomará parámetros por default
        if copyTree!=[]:
            tree=copyTree
            tree.DeleteAllItems()
        else:
            tree = wx.TreeCtrl(self, -1, wx.DefaultPosition, (-1,-1), wx.TR_HAS_BUTTONS)

        root = tree.AddRoot('Sequence')
        totalSentencias=len(dictTree.keys())

        if totalSentencias==0:#Si el diccionario se pasó vacío
            return tree#Devolver un tree solo con root=Sequence

        nivel=1#Sea cual sea la sentencia, la primera será de nivel 1, y su parent será root (Sequence)
        parent=root

        parentsDeNivel={}
        for nSentencia in range(1,totalSentencias+1):
            niv = dictTree[nSentencia][0]
            texto = dictTree[nSentencia][1]

            if niv == nivel:
                # parent=parent #NO cambiar al último parent si el nivel es el mismo
                #Realizar inserción
                inserto=tree.AppendItem(parent, texto)

            elif niv > nivel:#si hay subida de nivel, entonces puede subir 1 solo nivel
                #Backup del parent de nivel
                parentsDeNivel[nivel]=parent
                #Al cambiar el nivel, cambia el parent
                parent=inserto
                inserto=tree.AppendItem(parent, texto)
                nivel=niv
            else:#niv < nivel
                #Recuperar el parent de nivel niv
                parent=parentsDeNivel[niv]
                inserto=tree.AppendItem(parent, texto)
                nivel=niv
        return tree



    def processTree2Dict(self,tree,searchTypeORDs=False):
        # Recibe un tree y lo descompone, sentencia a sentencia, en un diccionario del tipo:
        # log[nSentencia]=[nivel, textoItem, item, cookie/None]
        # También se evalúa la existencia de TypeORDs y se obtiene información sobre estos, la cual será últil
        # posteriormente para obtener combinaciones de órdenes de Loops

        if searchTypeORDs:
            resu=[]
            logTypeORDs={}

        rootItem=tree.GetRootItem()
        # print "hijos de root No recursivo: " + str(tree.GetChildrenCount(rootItem,False))
        # print "hijos de root Recursivo (no tiene en cuent al root, sólo hijos): " + str(tree.GetChildrenCount(rootItem,True))

        nSentencias=tree.GetChildrenCount(rootItem,False)#True por recursivo, nDescendientes: hijos, nietos, etc.
        log={}
        if nSentencias==0:
            return log

        ancestros=["SEC"]
        nivel=1#nivel 0 es la Sequence

        #Procesar Línea 1
        sentenciaAct=1
        #Selección de item
        item, cookie=tree.GetFirstChild(rootItem)
        texto=tree.GetItemText(item)
        log[sentenciaAct] = [nivel, texto, item, cookie]

        if searchTypeORDs:
            textoAux=texto.split(':')
            if textoAux[1]=='TypeORDs':
                loopSKey = (textoAux[2]).encode('ascii')
                if not logTypeORDs.has_key(loopSKey):
                    # Obtener, para loopsKey, los nro de loop, y con estos acceder a secDictLoops para obtener los órdenes
                    splitLoopSkey=loopSKey.split('Loop')#extraer Loop para que sólo queden los nros
                    splitLoopSkey = splitLoopSkey[1:]
                    ORDERS=[]
                    for loopAux in splitLoopSkey:
                        ORDERS.append(self.secConfig['secDictLoops'][int(loopAux)])
                    logTypeORDs[loopSKey] = [1, 1, 0, 0, ORDERS]#REPET ACTUAL, MAX REPET, cuentaActual, cuentaTotal, ORDERS OF LOOPS
                else:
                    logTypeORDs[loopSKey][1] = logTypeORDs[loopSKey][1]+1#MAX REPET +1

            # print texto

        flagHijos=True
        seguir=True
        #Procesar Resto de Líneas
        while seguir:
            if flagHijos and tree.ItemHasChildren(item):#item tiene Hijos?
                #Backup de datos al entrar en un Children
                ancestros.append([item, cookie])
                nivel+=1#aumento de nivel al entrar en un children
                #Siguiente Sentencia
                sentenciaAct+=1
                #Reemplazo de item
                item, cookie=tree.GetFirstChild(item)
                texto=tree.GetItemText(item)
                log[sentenciaAct] = [nivel, texto, item, cookie]

                if searchTypeORDs:
                    textoAux = texto.split(':')
                    if textoAux[1] == 'TypeORDs':
                        loopSKey = (textoAux[2]).encode('ascii')
                        if not logTypeORDs.has_key(loopSKey):
                            # Obtener, para loopsKey, los nro de loop, y con estos acceder a secDictLoops para obtener los órdenes
                            splitLoopSkey = loopSKey.split('Loop')  # extraer Loop para que sólo queden los nros
                            splitLoopSkey = splitLoopSkey[1:]
                            ORDERS = []
                            for loopAux in splitLoopSkey:
                                ORDERS.append(self.secConfig['secDictLoops'][int(loopAux)])
                                logTypeORDs[loopSKey] = [1, 1, 0, 0, ORDERS]  # REPET ACTUAL, MAX REPET, cuentaActual, cuentaTotal, ORDERS OF LOOPS
                        else:
                            logTypeORDs[loopSKey][1] = logTypeORDs[loopSKey][1] + 1  # MAX REPET +1
            elif wx.TreeItemId.IsOk(tree.GetNextSibling(item)):#item tiene Hermanos?
                #Siguiente Sentencia
                sentenciaAct+=1
                #Reemplazo de item
                item=tree.GetNextSibling(item)
                texto=tree.GetItemText(item)
                log[sentenciaAct]=[nivel, texto, item, None]
                flagHijos=True#Puede pasar a False en el siguiente elif

                if searchTypeORDs:
                    textoAux = texto.split(':')
                    if textoAux[1] == 'TypeORDs':
                        loopSKey = (textoAux[2]).encode('ascii')
                        if not logTypeORDs.has_key(loopSKey):
                            # Obtener, para loopsKey, los nro de loop, y con estos acceder a secDictLoops para obtener los órdenes
                            splitLoopSkey = loopSKey.split('Loop')  # extraer Loop para que sólo queden los nros
                            splitLoopSkey=splitLoopSkey[1:]
                            ORDERS = []
                            for loopAux in splitLoopSkey:
                                ORDERS.append(self.secConfig['secDictLoops'][int(loopAux)])
                            logTypeORDs[loopSKey] = [1, 1, 0, 0, ORDERS]  #REPET ACTUAL, MAX REPET, cuentaActual, cuentaTotal, ORDERS OF LOOPS                        else:
                        else:
                            logTypeORDs[loopSKey][1] = logTypeORDs[loopSKey][1] + 1  # MAX REPET +1

            elif len(ancestros) > 1: #Hay ancentros además de "SEC"?
                #Recuperar el último ancestro en entrar a ancestros
                ancestro=ancestros.pop()#pop saca al ultimo de ancestros (lo actualiza) y lo devuelve
                item=ancestro[0]
                cookie=ancestro[1]
                flagHijos=False#Indica que en la siguiente iteración no debe preguntarse si item tiene hijos porque
                #item ya ha sido registrado y si tenía hijos ya han sido evaluados. Importa saber si item tiene hermanos.
                nivel-=1#disminución de nivel al salir de un children
            else:
                seguir=False


        if searchTypeORDs:
            resu.append(log)
            resu.append(logTypeORDs)
            return resu
        else:
            return log


    def panSecSetLineaActual(self,event):#Forzar a que la línea actual sea una determinada
        selectIDX = self.panSec.listSec.GetFirstSelected()
        self.secConfig['FinalSecLineAct']=selectIDX

    def updateTCoordRestante(self,event):
        tRestante=self.panSec.subpanControl.staticTCoordRestante.GetLabel()
        t=int(tRestante)
        if t<=0:
            self.timerTCRestante.Stop()
            self.panSec.subpanControl.staticTCoordRestante.SetLabel(" ")
            self.panSec.subpanControl.staticTCoordRestante.Show(False)
        else:
            tRestante = str(t-1)
            self.panSec.subpanControl.staticTCoordRestante.SetLabel(tRestante)
            self.panSec.subpanControl.staticTCoordRestante.Show(True)

        return

    def panSecTreeOnAdd(self, event):
        #Generador de evento:
        objLabel=event.GetEventObject().GetLabel()
        objName = event.GetEventObject().GetName()

       #Sobre el selecto y su parent:
        itemSel = self.treeSec.GetSelection()
        if not wx.TreeItemId.IsOk(itemSel):  # Si no hay selecto
            dlg = wx.MessageDialog(self, "It must be a selected item before inserting another one", 'No selected item', wx.OK | wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()
            return
        itemSelText= self.treeSec.GetItemText(itemSel)
        itemSelParent= self.treeSec.GetItemParent(itemSel)
        if not wx.TreeItemId.IsOk(itemSelParent):#Si no tiene Parent, es Sequence (root)
            itemSelParentText = ""
        else:
            itemSelParentText= self.treeSec.GetItemText(itemSelParent)

        # print "Current selected: " + self.treeSec.GetItemText(itemSel)
        # print "Parent of current selected: " + itemSelParentText

#####DÓNDE INSERTAR???#####DÓNDE INSERTAR???#####DÓNDE INSERTAR???#####DÓNDE INSERTAR???
        flagParentIsLoop=False #Las sentencias que dependen de ORD (GCODEord, TypeORD y DOrd) no deben poder ser
        # insertas fuera de un loop, ya que requieren de ORD
        if itemSelText.startswith("LOOP"):
            #Se quiere una inserción dentro del loop, en su inicio
            parent=itemSel
            beforeItem=0
            flagParentIsLoop=True #Con esto se habilita la inserción de sentencias dependientes de ORD
        elif itemSelText.startswith("eLOOP"):
            #Se quiere una inserción a continuación del loop (fuera de este) con el mismo parent
            parent=itemSelParent
            beforeItem=itemSel

            #Chequear si el parent es un LOOP
            if itemSelParentText.startswith("LOOP"):
                flagParentIsLoop=True

        elif itemSelText=="Sequence":
            #Se quiere una inserción al inicio de la Sequence
            parent=itemSel
            beforeItem=0

            #Si el parent es Sequence, no es un LOOP y por ende no pueden usarse sentencias f(ORD)
        elif itemSelText.startswith("!DIOS"):
        # elif itemSelText.startswith("!DIOS:Time") or itemSelText.startswith("!DIOS:TCoord") or itemSelText.startswith("!DIOS:Delay") or itemSelText.startswith("!DIOS:GCODEord") or itemSelText.startswith("!DIOS:GCODE"):
            #Comandos de DIOS e inserción directa
            #Se quiere una inserción debajo del selecto, con el mismo parent
            parent=itemSelParent
            beforeItem=itemSel

            # Chequear si el parent es un LOOP
            if itemSelParentText.startswith("LOOP"):
                flagParentIsLoop = True
        else:#Revisadas todas las sentencias posibles (inician en Loop, eLoop, Sequence, o !DIOS) quedan las sentencias
            #que comienzan con el nombre de un cliente, tipo !HPLC:TypeOrd o !HPLC:sikuliTal
            #Sentencias: !HPLC:NombreSikuli, !HPLC:TypeORD, !HPLC:TypeText, !HPLC:TypeTime, !HPLC:SendKEY
            #Inserción directa

            parent=itemSelParent
            beforeItem=itemSel

            # Chequear si el parent es un LOOP
            if itemSelParentText.startswith("LOOP"):
                flagParentIsLoop=True

            aux=itemSelText.split(":")
            nombreCliente=aux[0].lstrip('!')
            # print nombreCliente


#####HASTA AQUÍ: SE TIENE INFORMACIÓN SOBRE DÓNDE INSERTAR (parent y beforeItem)

#####QUÉ INSERTAR???#####QUÉ INSERTAR???#####QUÉ INSERTAR???#####QUÉ INSERTAR???#####QUÉ INSERTAR???

        if objLabel=="Loop":
            #Inserción de LOOPs/eLOOPs
            newKey=self.getNewKeyLoops()
            #Registrar newKey para Loops
            self.registerNewKeyLoops(newKey,self.secConfig['orderSTR'])
            #Agregar LOOP
            texto="LOOP"+str(newKey)+":"+self.secConfig['orderSTR']
            loop0=self.insertSentence(self.treeSec,texto,parent,beforeItem)
            #Agregar eLOOP
            texto="eLOOP"+str(newKey)+":"+self.secConfig['orderSTR']
            loop1=self.insertSentence(self.treeSec,texto,parent,loop0)
            #Ante la inserción de un Loop-eLoop, dejar selecto el Loop
            self.treeSec.SelectItem(loop0,True)

        ####SENTENCIAS RELACIONADAS A CLIENTES
        elif objLabel=="Add":
            if self.comboListaClientes.GetCurrentSelection() is -1 or self.comboDictSikulis.GetCurrentSelection() is -1:
                return
            texto=("!"+self.comboListaClientes.GetValue()+":"+self.comboDictSikulis.GetValue()).split(".")[0]
            inserto=self.insertSentence(self.treeSec,texto,parent,beforeItem)
        elif objName=="TypeORD":
            if self.comboListaClientes.GetCurrentSelection() is -1:
                return

            if not flagParentIsLoop:
                dlg = wx.MessageDialog(self, 'TypeORD must be inside a LOOP',
                                       'TypeORD outside a LOOP',
                                       wx.OK | wx.ICON_INFORMATION)
                dlg.ShowModal()
                dlg.Destroy()
                return

            texto="!"+self.comboListaClientes.GetValue()+":"+"TypeORD"
            inserto=self.insertSentence(self.treeSec,texto,parent,beforeItem)
        elif objName=="TypeORDs":
            if self.comboListaClientes.GetCurrentSelection() is -1:
                return

            if not flagParentIsLoop:
                dlg = wx.MessageDialog(self, 'TypeORDs must be inside a LOOP',
                                       'TypeORDs outside a LOOP',
                                       wx.OK | wx.ICON_INFORMATION)
                dlg.ShowModal()
                dlg.Destroy()
                return

            texto="!"+self.comboListaClientes.GetValue()+":"+"TypeORDs"+":"

            # En el caso de TypeORDs, a texto se le agregan los nombres de los Loops ancestros
            flagSeguir=True
            ancestro=parent
            ParentText = (self.treeSec.GetItemText(ancestro)).encode('ascii')
            LOOPxx = self.panSecFindLoopN_endLoopN_order(ParentText, False)
            orderLoopS="Loop"+str(LOOPxx[0])

            while flagSeguir:
                ancestro=self.treeSec.GetItemParent(ancestro)
                if wx.TreeItemId.IsOk(ancestro):  # Si tiene Parent, puede ser un Loop, tomar datos:
                    if ancestro == self.treeSec.GetRootItem():
                        flagSeguir = False
                    else:
                        ParentText = (self.treeSec.GetItemText(ancestro)).encode('ascii')
                        LOOPxx = self.panSecFindLoopN_endLoopN_order(ParentText, False)
                        orderLoopS ="Loop" + str(LOOPxx[0])+orderLoopS
                elif not wx.TreeItemId.IsOk(itemSelParent):  # Si no tiene Parent, es Sequence (root)
                    flagSeguir=False
            texto = texto + orderLoopS
            inserto=self.insertSentence(self.treeSec,texto,parent,beforeItem)
        elif objName=="TypeIDX":
            if self.comboListaClientes.GetCurrentSelection() is -1:
                return

            if not flagParentIsLoop:
                dlg = wx.MessageDialog(self, 'TypeIDX must be inside a LOOP',
                                       'TypeIDX outside a LOOP',
                                       wx.OK | wx.ICON_INFORMATION)
                dlg.ShowModal()
                dlg.Destroy()
                return

            texto="!"+self.comboListaClientes.GetValue()+":"+"TypeIDX"
            inserto=self.insertSentence(self.treeSec,texto,parent,beforeItem)
        elif objName=="TypeTime":
            if self.comboListaClientes.GetCurrentSelection() is -1:
                return
            texto="!"+self.comboListaClientes.GetValue()+":"+"TypeTime"
            inserto=self.insertSentence(self.treeSec,texto,parent,beforeItem)
        elif objName=="TypeText":
            if self.comboListaClientes.GetCurrentSelection() is -1:
                return
            textoAux=self.panSec.subpanControl.boxTypeText.GetValue()
            if textoAux.isspace() or textoAux == "":#verificar que haya texto
                return
            texto="!"+self.comboListaClientes.GetValue()+":"+"TypeText"+":"+self.panSec.subpanControl.boxTypeText.GetValue()
            inserto=self.insertSentence(self.treeSec,texto,parent,beforeItem)
        elif objLabel=="SendKEY":
            if self.comboListaClientes.GetCurrentSelection() is -1 or self.panSec.subpanControl.comboKeys.GetCurrentSelection() is -1:
                return
            texto="!"+self.comboListaClientes.GetValue()+":"+"SendKEY"+":"+self.panSec.subpanControl.comboKeys.GetStringSelection()
            inserto=self.insertSentence(self.treeSec,texto,parent,beforeItem)

        ####SENTENCIAS RELACIONADAS A DIOS
        elif objName=="Time":
            texto="!"+"DIOS"+":"+"Time"
            inserto=self.insertSentence(self.treeSec,texto,parent,beforeItem)
        elif objLabel=="Delay":
            # Primero ejecutar panSecOnChangeDelay para confirmar el valor del delay
            self.panSecOnChangeDelay(wx.EVT_TEXT_ENTER)
            if self.secConfig['delaySTR']!="0":
                texto="!"+"DIOS"+":"+"Delay"+":"+self.secConfig['delaySTR']
                inserto=self.insertSentence(self.treeSec,texto,parent,beforeItem)
            else:
                return
        elif objName=="Pause":
            texto="!"+"DIOS"+":"+"Pause"
            inserto=self.insertSentence(self.treeSec,texto,parent,beforeItem)
        elif objLabel=="DOrd":
            if self.panSec.subpanControl.boxDOrd.GetValue() == " ":
                return
            if self.secConfig['secDOrdActual']=="":
                dlg = wx.MessageDialog(self, 'Current expression has not been validated. Did you press Enter?', 'DOrds: Not validated expression',
                                       wx.OK | wx.ICON_INFORMATION)
                dlg.ShowModal()
                dlg.Destroy()
                return
            if not flagParentIsLoop:
                dlg = wx.MessageDialog(self, 'DOrd must be inside a LOOP',
                                       'DOrd outside a LOOP',
                                       wx.OK | wx.ICON_INFORMATION)
                dlg.ShowModal()
                dlg.Destroy()
                return

            #Debe controlarse si la expresión va a dar milisegundos enteros o flotantes. En el último caso, alertar.
            #Para llegar acá, flagParentIsLoop debe ser True. Puede ser True de 2 formas:
            #1- El elemento selecto antes de la inserción es un LOOP
            #2- No se cumple 1, pero el selecto tiene un parent que es un LOOP
            #Lo que varía en ambos casos es el ORDER que debe procesarse
            if itemSelText.startswith("LOOP"):
                orderLoop=self.getOrderLoop(itemSelText)
            elif itemSelParentText.startswith("LOOP"):
                orderLoop = self.getOrderLoop(itemSelParentText)

            flagAlertDOrdFloat=False
            idxCount=0
            for ord in orderLoop:
                idxCount+=1
                textoAux= self.secConfig['secDOrdActual']
                textoAux = textoAux.replace("ORD",str(ord))
                textoAux = textoAux.replace("IDX", str(idxCount))
                try:
                    valor = eval(textoAux)
                    if int(valor) != valor:
                        flagAlertDOrdFloat=True
                        break
                except:
                    dlg = wx.MessageDialog(self, 'DOrds: Expression can not be evaluated.', 'Wrong written DOrd?',
                                           wx.OK | wx.ICON_INFORMATION)
                    dlg.ShowModal()
                    dlg.Destroy()
                    return

            if flagAlertDOrdFloat:
                dlg = wx.MessageDialog(self, 'DOrds: There exist numeric DOrds, but not integers.'+'\n'+'Calculated delays will be changed to integers',
                                       'Not integer DOrds', wx.OK | wx.ICON_INFORMATION)

                dlg.ShowModal()
                dlg.Destroy()

            texto="!"+"DIOS"+":"+"DOrd"+":"+self.secConfig['secDOrdActual']
            inserto=self.insertSentence(self.treeSec,texto,parent,beforeItem)
        elif objLabel=="+GCODE":
            if self.pronGCODESactual != " ":
                texto="!"+"DIOS"+":"+"GCODE"+":"+self.pronGCODESactual+":"+self.pronPathGCODES
                inserto=self.insertSentence(self.treeSec,texto,parent,beforeItem)
            else:
                return
        elif objLabel=="+GCODEord":
            if self.panSec.subpanControl.boxGCODEord.GetValue() == "":
                return
            if self.secConfig['secGCODEordActual']=="":
                dlg = wx.MessageDialog(self, 'Current expression has not been validated. Did you press Enter?', 'GCODEord: Not validated expression',
                                       wx.OK | wx.ICON_INFORMATION)
                dlg.ShowModal()
                dlg.Destroy()
                return

            if not flagParentIsLoop:
                dlg = wx.MessageDialog(self, 'GCODEord must be inside a LOOP',
                                       'GCODEord outside a LOOP',
                                       wx.OK | wx.ICON_INFORMATION)
                dlg.ShowModal()
                dlg.Destroy()
                return

            #Para llegar acá, flagParentIsLoop debe ser True. Puede ser True de 2 formas:
            #1- El elemento selecto antes de la inserción es un LOOP
            #2- No se cumple 1, pero el selecto tiene un parent que es un LOOP
            #Lo que varía en ambos casos es el ORDER que debe procesarse
            if itemSelText.startswith("LOOP"):
                orderLoop=self.getOrderLoop(itemSelText)
            elif itemSelParentText.startswith("LOOP"):
                orderLoop = self.getOrderLoop(itemSelParentText)

            #Debe controlarse si las expresiones entre $ (y que contengan a ORD o IDX) dan numérico o no.
            gcodeAct = self.secConfig['secGCODEordActual']
            # Dividir con $
            gcodeActList = gcodeAct.split("$")
            for code in gcodeActList:#Ej G0 F300 X$ORD+1$ Y$IDX+2$
                #Evaluar solo si existe ORD y/o IDX
                if "ORD" in code or "IDX" in code:
                    idxCount=0
                    for ord in orderLoop:
                        idxCount+=1
                        textoAux= code
                        textoAux = textoAux.replace("ORD",str(ord))
                        textoAux = textoAux.replace("IDX", str(idxCount))
                        try:
                            valor=eval(textoAux)
                            valor = float(valor)
                            pass
                        except:
                            dlg = wx.MessageDialog(self, 'GCODEords: Some expressions between "$" does not result numeric', 'Wrong written GCODEord?',
                                                   wx.OK | wx.ICON_INFORMATION)
                            dlg.ShowModal()
                            dlg.Destroy()
                            return


            texto="!"+"DIOS"+":"+"GCODEord"+":"+self.secConfig['secGCODEordActual']
            inserto=self.insertSentence(self.treeSec,texto,parent,beforeItem)
            return
        elif objLabel=="TC" or objLabel=="TCS":
            if self.secConfig['TCoordActual']==0:
                return
            TcoordN=self.getNewKeyTCoord()
            self.registerNewKeyTCoord(TcoordN,self.secConfig['TCoordActual'])

            #Insertar en el diccionario: LO HACE registerNewKeyTCoord
            # self.secConfig['secDictTCoords'][TcoordN]=[0,0, self.secConfig['TCoordActual']]
            # print "Diccionario de TCoords:"
            # print self.secConfig['secDictTCoords']
            #Insertar texto correspondiente
            if objLabel=="TC":
                texto="!"+"DIOS"+":"+"TC"+str(TcoordN)+":"+str(self.secConfig['TCoordActual'])
                TCoord0=self.insertSentence(self.treeSec,texto,parent,beforeItem)
                texto="!"+"DIOS"+":"+"eTC"+str(TcoordN)+":"+str(self.secConfig['TCoordActual'])
                TCoord1=self.insertSentence(self.treeSec,texto,parent,TCoord0)
            elif objLabel=="TCS":
                texto="!"+"DIOS"+":"+"TCS"+str(TcoordN)+":"+str(self.secConfig['TCoordActual'])
                TCoord0=self.insertSentence(self.treeSec,texto,parent,beforeItem)
                texto="!"+"DIOS"+":"+"eTCS"+str(TcoordN)+":"+str(self.secConfig['TCoordActual'])
                TCoord1=self.insertSentence(self.treeSec,texto,parent,TCoord0)
            return
        elif objLabel == "MsgBox":
            textoAux=self.panSec.subpanControl.boxTypeText.GetValue()
            if textoAux.isspace() or textoAux=="":#verificar que haya texto
                return
            texto = "!"+"DIOS"+ ":" + "MsgBox" + ":" + self.panSec.subpanControl.boxTypeText.GetValue()
            inserto = self.insertSentence(self.treeSec, texto, parent, beforeItem)
            return
        elif objLabel == "Notif":
            textoAux = self.panSec.subpanControl.boxTypeText.GetValue()
            if textoAux.isspace() or textoAux == "":  # verificar que haya texto
                return
            texto = "!" + "DIOS" + ":" + "Notif" + ":" + self.panSec.subpanControl.boxTypeText.GetValue()
            inserto = self.insertSentence(self.treeSec, texto, parent, beforeItem)
            return


    def getOrderLoop(self,text):
        #text debe ser del tipo "LOOP20: 4 7 1 12"
        #La función extrae los números de order y los convierte a enteros. Entrega una list
        orderAux = text.split(":")
        orderLoop = orderAux[1].split(' ')
        orderAux = []
        for ord in orderLoop:
            orderAux.append(int(ord))
        return orderAux


    def getNewKeyLoops(self):
         #Checkear cuáles keys hay en el secDictLoops
        LoopsKeys=self.secConfig['secDictLoops'].keys()
        if len(LoopsKeys)==0:
            newKey=1
        else:
            #Determinar cual es la siguiente key
            newKey=max(LoopsKeys)+1
        return newKey

    def registerNewKeyLoops(self,newKey,order):
        #order: en inserciones por GUI, proviene de self.secConfig['orderSTR']. También puede provenir de la
        # lectura de un archivo de Sequence
        self.secConfig['secDictLoops'][newKey]=[(order).encode('ascii')]

    def unRegisterKeyLoops(self,oldKey):
        del self.secConfig['secDictLoops'][oldKey]
        # print self.secConfig['secDictLoops']

    def getNewKeyTCoord(self):
             #Checkear cuáles keys hay en el secDictTCoords
            self.secConfig['secDictTCoords']
            TCKeys=self.secConfig['secDictTCoords'].keys()
            if len(TCKeys)==0:
                newKey=1
            else:
                #Determinar cual es la siguiente key
                newKey=max(TCKeys)+1
            return newKey

    def registerNewKeyTCoord(self,newKey,TC):
            self.secConfig['secDictTCoords'][newKey]=[0,0,TC]

    def unRegisterKeyTCoord(self,oldKey):
        del self.secConfig['secDictTCoords'][oldKey]
        # print self.secConfig['secDictTCoords']


    def insertSentence(self, tree,texto,parent,beforeItem):
        #NOTA: la función original era:
        # inserto = tree.InsertItem(parent, beforeItem, texto)
        # tree.SelectItem(inserto, True)
        # return inserto
        # Sin embargo, desde wxPython 3 (Phoenix), en Linux se encontraba un error (wx.PyAssertionError) por medio del
        # cual no era posible insertar comandos si éstos no iban a ser siblings de beforeItem. Antes, la inserciones
        # podían realizarse aun cuando parent y beforeItem eran el mismo elemento. Por ende se modificó la función
        # panSecTreeOnAdd para que beforeItem tome el valor 0 como señal para usar PrependItem y no InsertItem
        if beforeItem is not 0:
            inserto=tree.InsertItem(parent,beforeItem,texto)
        else:
            inserto = tree.PrependItem(parent, texto)
        tree.SelectItem(inserto,True)
        return inserto

    def processGCODEord(self,gcode,numORD,idxCount):
        #Procesa una sentencia CGODEord. Genera archivos .gcode y resulta en la ruta hacia esos archivos
        #gcode es la sentencia del tipo G0 F300 X$ORD+20$ Y$IDX*3.1$
        #numORD es el entero (de un vector ORDER) que representa a ORD en una iteracion
        #idxCount es el índice (posición) de ORD en el ORDER

        #Dividir con $
        lista=gcode.split("$")
        stringfinal=""
        gcodeList=[]
        for aux in lista:
            if "ORD" in aux or "IDX" in aux:
                aux2=aux.replace("ORD",str(numORD))
                aux2 = aux2.replace("IDX", str(idxCount))
                valor=eval(aux2)
                stringfinal=stringfinal+str(valor)
            else:
                stringfinal=stringfinal+str(aux)
        gcodeList.append(stringfinal+"\n")
        gcodeList.append("M400"+"\n")

        # guardar con un nombre del tipo TEMP_3_time_RANDOM.gcode
        auxrand=random.random()
        while auxrand < 0.1:
            auxrand=random.random()
        auxrand=int(10000*auxrand)
        fileName="TEMP_"+str(numORD)+"_"+str(time.time())+"_"+str(auxrand)+".gcode"
        fileDir=os.getcwd()
        myfile = open(fileName, "w+")#abrir como escritura y lectura,sobreescribir
        myfile.seek(0,0)
        myfile.writelines(gcodeList)
        myfile.close()

        lista=[];
        lista.append(fileName)
        lista.append(fileDir)
        lista.append(stringfinal)

        return lista

    def panSecUpdateLoopsExistentes(self):
        result={}
        fila=-1#señal de que FindItem busque desde el inicio
        for loop in range(1,self.secConfig['nLoops']+1):#para cada loop:
            strLOOP="LOOP"+str(loop)
            posiLOOP=self.panSec.listSec.FindItem(fila,strLOOP,True)
            if posiLOOP==-1:
                pass
            else:
                item = self.panSec.listSec.GetItem(posiLOOP, 0)
                texto=item.GetText()
                aux=self.panSecFindLoopN_endLoopN_order(texto, True)
                posiEndLOOP=aux[1]
                orderLoop=aux[2]
                result[strLOOP] = [posiLOOP, posiEndLOOP, orderLoop]
                fila=posiEndLOOP#preparar para la siguiente búsqueda

        self.secConfig['infoLoops']=result #se actualiza infoLoops



    def panSecAllowNewLoop(self,fila):
        # fila es la fila donde se quiere realizar la inserción
        allow=False
        # Hay que recorrer hacia abajo. Si se encuentra eLOOP antes que LOOP, entonces no se puede
        posiEndLOOP=self.panSec.listSec.FindItem(fila,"eLOOP",True)
        posiLOOP=self.panSec.listSec.FindItem(fila,"LOOP",True)
        if posiEndLOOP==-1 and posiLOOP==-1:#No existen loops ni eLoops inferiores: PERMITIR
            allow=True
        elif posiEndLOOP!=-1 and posiLOOP==-1:# no hay loops inferiores, pero si un eLOOP, entonces se está
            #dentro de un loop: NO PERMITIR
            allow=False
        # LA SIGUIENTE COMBINACIÓN NO: no puede encontrarse un LOOP sin su eLOOP debajo
        # elif posiEndLOOP==-1 and posiLOOP!=-1:# hay loops inferiores, pero no eLOOP, entonces se está
        #     #fuera de un loop: PERMITIR
        #     allow=True
        elif posiEndLOOP!=-1 and posiLOOP!=-1:# hay loops inferiores, hay un eLOOP, deben compararse:
            if posiEndLOOP<posiLOOP:
                allow=False
            else:
                allow=True
        return allow

    def panSecFindLoopN_endLoopN_order(self,textoLoopN, findEndLoopN=True):
        # Esta función recibe un texto donde se haya encontrado LOOPxx
        # El resultado será el valor de xx (nLOOP), la fila donde termina ese loop (eLOOPnLOOP) y el order del LOOP
        # Si findendLoopN es false, entonces sólo se obtendrá nLOOP y el order del LOOP
        aux=textoLoopN.split(':')
        orderLoop=aux[1]
        aux=aux[0]
        nLOOP=int(aux[4:])#la palabra LOOP tiene 4 letras, luego de las primeras 4 letras (0,1,2 y 3) viene la 5ta, que es
        # 4:..luego no se pone nada porque se instruye "hasta el final"
        endLOOPnLOOP=-1
        if findEndLoopN:
            #Encontrar el eLOOPnLOOP
            # FindItem sólo encuentra Items en la columna 0
            buscar="eLOOP"+str(nLOOP)+":"#agrego los ":" porque FindItem trabaja con True y entonces
            # puede encontrar un resultado parcial. De esa forma, con el :, no será posible
            endLOOPnLOOP=self.panSec.listSec.FindItem(-1,buscar,True)
        result=[nLOOP,endLOOPnLOOP,orderLoop]
        return result

    def compareDictsUnregisterChildrens(self,dictTree0,dictTree1):
        #Tras algunas eliminaciones (LOOPs por ej) se eliminan childrens. Esta función revisa a los childrens
        #eliminados y, en caso de haber estado registrados en algún dict, elimina dichos registros
        #dictTree0 es el diccionario proveniente de un tree antes de ser modificado con eliminaciones
        #dictTree1 es el diccionario proveniente luego de las eliminaciones

        if len(dictTree1.keys()) > 0:#En determinadas eliminaciones dictTree1 entra a la función sin ningún elemento
            #por lo cual, en ese caso, deben eliminarse todos los registros sin evaluar comparaciones
            k1 = 1
            for k0 in dictTree0.keys():
                niv0 = str(dictTree0[k0][0])
                texto0 = str(dictTree0[k0][1])
                niv1 = str(dictTree1[k1][0])
                texto1 = str(dictTree1[k1][1])
                if niv0 == niv1 and texto0 == texto1:
                    k1 += 1
                else:
                    self.unregisterKeyAll(texto0)
        else:
            self.unregisterKeyAll("NoImporta", True) #El flag True indica eliminación total de todos los registros


    def panSecTreeOnRemove(self, event):
        # Determinar la sentencia a BORRAR en base al actual selecto
        itemSel = self.treeSec.GetSelection()
        if not wx.TreeItemId.IsOk(itemSel):#Si no hay selecto
            return

        #Habiendo un selecto, no se puede borrar sobre cualquier sentencia
        #Impedir que se borren sentencias si se seleciona eLOOP, eTC o Sequence
        #Tomar el texto y analizar
        texto = self.treeSec.GetItemText(itemSel)
        if texto.startswith("eLOOP") or texto.startswith("!DIOS:eTC") or texto.startswith("Sequence"):
            return

        # Hay que obtener el dictTree antes de borrar, y luego de borrar. Comparar, y sacar de los registros a aquellos
        # que ya no estén
        dictTree0=self.processTree2Dict(self.treeSec)

        # Determinar si lo que se va a borrar es un LOOP, y en ese caso también borrar su eLOOP.
        if texto.startswith("LOOP"):
            #Obtener el número del LOOP: LOOP12: 3 5 6
            aux=texto.split(':')
            aux=aux[0]#LOOP12
            nLOOP=aux[4:]#12
            #Borrar el Loop, sus "descendientes" (todos) y el item siguiente al Loop (que es su eLOOP)
            eLOOPnLOOP=self.treeSec.GetNextSibling(itemSel)
            #Borrar todos los descendientes:
            self.treeSec.DeleteChildren(itemSel)
            #Borrar eLOOP
            self.treeSec.Delete(eLOOPnLOOP)
            #Borrar LOOP
            self.treeSec.Delete(itemSel)

            #NOTA: ESTE PROBLEMA SE DARÁ CON LOOPS, PERO TAMBIÉN CON CUALQUIER SENTENCIA QUE:
            #1- CONTENGA UN INICIO Y UN FIN (LOOP-eLOOP POR EJ)
            #2- AL INSERTARSE, GENERE UN NUEVO NIVEL
            #EN EL FUTURO, SENTENCIAS COMO WHILE-eWHILE, ETC, DEBERÁN SER TENIDAS EN CUENTA

            # SI SE ELIMINAN TODOS LOS DESCENDIENTES, PERO DENTRO DE ESTOS HAY LOOPS O TC
            #HABRA QUE REVISAR A LOS ELIMINADOS Y SACARLOS DE LOS REGISTROS (DICTS)
            dictTree1 = self.processTree2Dict(self.treeSec)
            self.compareDictsUnregisterChildrens(dictTree0,dictTree1)

            #Actualizar nLoops
            self.secConfig['nLoops']=self.secConfig['nLoops']-1
            return
        elif texto.startswith("!DIOS:TCS"):#Se quiere borrar TCoord STOP
            # Obtener el número del TCS: TCS3: 60
            aux = texto.split(':')
            aux = aux[1]  # TCS3
            nTcoord = int(aux[3:])
            #NOTA: el eTC de un TC es sibling del TC, buscarlo así
            #Buscar eTC respectivo
            itemAux=itemSel
            flagTCnotFound=True
            while wx.TreeItemId.IsOk(self.treeSec.GetNextSibling(itemAux)):#Mientras haya un sibling del TC
                itemAux = self.treeSec.GetNextSibling(itemAux)
                textItemAux=self.treeSec.GetItemText(itemAux)
                if textItemAux.startswith("!DIOS:eTCS" + str(nTcoord) +":"):#NOTA: agrego los ":" porque puede suceder
                    #que por error algo empiece así pero no sea lo correcto. Por ej, buscar TCS1 podría confundirse al
                    #encontrar TCS12 o TCS11111

                    #Borrar eTCS
                    self.treeSec.Delete(itemAux)
                    # Borrar TCS
                    self.treeSec.Delete(itemSel)
                    # Eliminar TCSn del registro de TCoords
                    self.unRegisterKeyTCoord(nTcoord)
                    flagTCnotFound=False
                    break
            if flagTCnotFound:
                auxmsg="eTCS not found" + str(nTcoord)
                dlg = wx.MessageDialog(self, auxmsg,'endTCS not found', wx.OK | wx.ICON_INFORMATION)
                dlg.ShowModal()
                dlg.Destroy()
            return
        elif texto.startswith("!DIOS:TC"):#Se quiere borrar TCoord Simple
            # Obtener el número del TC: TC3: 60
            aux = texto.split(':')
            aux = aux[1]  # TC3
            nTcoord = int(aux[2:])
            # NOTA: el eTC de un TC es sibling del TC, buscarlo así
            # Buscar eTC respectivo
            itemAux = itemSel
            flagTCnotFound = True
            while wx.TreeItemId.IsOk(self.treeSec.GetNextSibling(itemAux)):  # Mientras haya un sibling del TC
                itemAux = self.treeSec.GetNextSibling(itemAux)
                textItemAux = self.treeSec.GetItemText(itemAux)
                if textItemAux.startswith("!DIOS:eTC" + str(nTcoord) + ":"):
                    # Borrar eTCS
                    self.treeSec.Delete(itemAux)
                    # Borrar TCS
                    self.treeSec.Delete(itemSel)
                    # Eliminar TCSn del registro de TCoords
                    self.unRegisterKeyTCoord(nTcoord)
                    flagTCnotFound = False
                    break
            if flagTCnotFound:
                auxmsg = "eTC not found" + str(nTcoord)
                dlg = wx.MessageDialog(self, auxmsg, 'endTC not found', wx.OK | wx.ICON_INFORMATION)
                dlg.ShowModal()
                dlg.Destroy()
            return
        else:#Eliminación de cualquier otra sentencia (NO LOOP, TC, TCS)
            # Borrar Sentencia
            self.treeSec.Delete(itemSel)
            return

    def unregisterKeyAll(self, texto, flagAll=False):
        #Si flagAll es True, entonces se limpian TODOS LOS REGISTROS
        if flagAll:
            self.secConfig['secDictLoops']={}
            self.secConfig['secDictTCoords'] = {}
            return
        else:
            if texto.startswith("LOOP"):  # Unregister LOOPs
                # Obtener el número del LOOP: LOOP12: 3 5 6
                aux = texto.split(':')
                aux = aux[0]  # LOOP12
                nLOOP = int(aux[4:])  # 12
                self.unRegisterKeyLoops(nLOOP)
            elif texto.startswith("!DIOS:TCS"):  # Unregister TCoords STOP
                # Obtener el número del TCS: TCS3: 60
                aux = texto.split(':')
                aux = aux[1]  # TCS3
                nTcoord = int(aux[3:])
                self.unRegisterKeyTCoord(nTcoord)
            elif texto.startswith("!DIOS:TC"):  # Unregister TCoords Simples
                # Obtener el número del TC: TC20: 100
                aux = texto.split(':')
                aux = aux[1]  # TC20
                nTcoord = int(aux[2:])
                self.unRegisterKeyTCoord(nTcoord)

    def corregirNLoops(self,nLoopRemoved):
        #Evaluar línea a línea
        nitems=self.panSec.listSec.GetItemCount()# contar elementos de la Sequence
        for fila in range(0,nitems):#para cada fila:
            nivSelecto=self.panSecGetNivelAct(fila)
            item = self.panSec.listSec.GetItem(fila, nivSelecto)
            texto=item.GetText()
            if texto.startswith("LOOP"):
                result=self.panSecFindLoopN_endLoopN_order(texto, True)
                if result[0]>nLoopRemoved:#si el loop encontrado es mayor que el removido:
                    aux=texto.split(':')
                    orderAux=aux[1]#order que llevaba el loop
                    straux="LOOP"+str(result[0]-1)+":"+str(orderAux)#notar q disminuye en 1 el nro de loop
                    self.panSec.listSec.SetStringItem(fila, nivSelecto, straux)#se cambia el texto
                    straux="eLOOP"+str(result[0]-1)+":"+str(orderAux)#notar q disminuye en 1 el nro de eLoop
                    self.panSec.listSec.SetStringItem(result[1], nivSelecto, straux)#se cambia el texto



    def panSecTreeOnClear(self, event):
        self.notifClear()#Borrar notificaciones si existen
        self.notificaciones.Show()

        #Eliminar todos los hijos de root
        self.treeSec.DeleteChildren(self.treeSec.GetRootItem())
        self.treeSec.SelectItem(self.treeSec.SequenceRoot, True)
        #Vaciar diccionario de Loops
        self.secConfig['secDictLoops']={}
        self.secConfig['nLoops']=0
        self.secConfig['orderSTR']="1"
        self.panSec.subpanControl.boxOrder.SetValue("1")
        self.panSec.subpanControl.boxDelay.SetValue("0")
        self.panSec.subpanControl.boxTypeText.SetValue("")
        self.secConfig['secDictTCoords']={}
        self.secConfig['TCoordActual']=0
        self.panSec.subpanControl.boxTCoord.SetValue("0")
        self.secConfig['secDOrdActual'] = ""
        self.panSec.subpanControl.boxDOrd.SetValue("")
        self.secConfig['secGCODEordActual'] = ""
        self.panSec.subpanControl.boxGCODEord.SetValue("")
        self.secConfig['secListaGCODES']=[]#vaciar lista de GCODEs necesarios (NO GCODEord)
        self.secConfig['secFlagExistsGCODEord']=False#Indicar que no hay GCODEords
        #Eliminar archivos gcode temporales
        self.deleteAllTempGCODE()

        # NOTA: desde wxPython 3 (Phoenix), en ocasiones no queda seleccionado "Sequence" aun con
        # self.treeSec.SelectItem(self.treeSec.SequenceRoot, True) . Por lo tanto se usa ensureSelectRoot con
        # wx.CallAfter . Si se da que no queda seleccionado, cuando el usuario quiere insertar un primer comando
        # obtendrá un error que cierra el programa.
        wx.CallAfter(self.ensureSelectRoot)


    def ensureSelectRoot(self):
        self.treeSec.SelectItem(self.treeSec.SequenceRoot, True)
        return

    def panSecOnStop(self,event):
        self.panSecCoordSecIsBtns(['Stopped','Run',False])

    def panSecCoordSecIsBtns(self,config):#config es una lista del tipo [secIs, LabelBtnRUN, ShowStop]
        # secIs: Running, Paused, Stopped
        # LabelBtnRUN: Run, Pause, Resume
        # ShowStop: True or False
        self.secConfig['secIs']=config[0]
        # En base al secIs del server, enviar info a los clientes para actualizar sus secIs
        if config[0]=='Running':
            if len(self.secConfig['clientesNecFinalSec']) is not 0:# CASO SOLO DIOS
                self.protocol.sendMsg("SEC_RUNNING")
            self.panSec.subpanControl.btnSecSetLineaActual.Show(False)
        elif config[0]=='Paused':
            # Enviar mensaje de PAUSE a todos los clientes
            if len(self.secConfig['clientesNecFinalSec']) is not 0:  # CASO SOLO DIOS
                self.protocol.sendMsg("SEC_PAUSED")
            self.panSec.subpanControl.btnSecSetLineaActual.Show(True)
            # Ejecutar FinalSecProcess para ir al estado paused
            wx.CallAfter(self.FinalSecProcess,[])
        elif config[0]=='Stopped':
            # Enviar mensaje de STOP a todos los clientes
            if len(self.secConfig['clientesNecFinalSec']) is not 0:  # CASO SOLO DIOS
                self.protocol.sendMsg("SEC_STOPPED")
            self.panSec.subpanControl.btnSecSetLineaActual.Show(False)
            # Ejecutar FinalSecProcess para ir al estado stopped
            wx.CallAfter(self.FinalSecProcess,[])


        # Actualizar Label de btnRun y estado de btnStop
        self.panSec.subpanControl.btnRun.SetLabel(config[1])
        self.panSec.subpanControl.btnStop.Show(config[2])

    def dict2DirectSentences(self,dict):
        #toma un diccionario (proveniente de un tree) y lo analiza con el objetivo de producir sentencias directas. Es decir,
        #hacer desaparecer sentencias LOOP, y convertir donde haya un ORD en TypeORD, GCODEord y DOrd a Type, GCODE y Delay
        #Devuelve una list (listRESULT)

        totalSentencias=len(dict.keys())
        if totalSentencias==0:#Si el diccionario se pasó vacío
            return []

        dictCopy=dict#copia del dict original
        nivMax=0#máximo nivel en las sentencias analizadas
        nivMaxFirstSent=1 #Primera Sentencia encontrada con nivel máximo
        nSentencEnProc = []#Número de sentencias en proceso
        dictBLOCKS={}#dict para ir almacenando BLOCKS
        dictCopy2={}#auxiliar para trabajar con dictCopy

        while totalSentencias > 0:#mientras haya sentencias para procesar:
            #Obtener keys de sentencias aun sin procesar
            keysBeforeProc=dictCopy.keys()

            #Evaluar todas las sentencias de dictCopy, encontrar el máximo nivel y la primera sentencia en presentar
            # ese nivel
            for k in keysBeforeProc:#obtener el nivel máximo en las sentencias
                if dictCopy[k][0] > nivMax:
                    nivMax = dictCopy[k][0]
                    nivMaxFirstSent = k

            # En nSentencEnProc se irán agregando los índices de las sentencias que estén siendo procesadas
            if nivMaxFirstSent != 1:
                if totalSentencias > 1:
                    nSentencEnProc = [nivMaxFirstSent - 1,nivMaxFirstSent]  # nivMaxFirstSent -1 es el parent de
                    # nivMaxFirstSent, que tiene que ser un LOOP y debe por lo tanto desaparecer de la lista de sentencias simples
                else:#Es posible que nivMaxFirstSent != 1, pero si queda solo una sentencia, tampoco tiene parent
                    nSentencEnProc = [nivMaxFirstSent]
            else:
                nSentencEnProc = [nivMaxFirstSent]#la sentencia 1 no tiene una anterior (parent) para agregar

            # Buscar sentencias de igual nivel que nivMax y que sean contínuas a nivMaxFirstSent
            for nSentencia in keysBeforeProc:
                if nSentencia in nSentencEnProc:
                    pass#no hacer nada
                elif dictCopy[nSentencia][0] == nivMax and nSentencia > nivMaxFirstSent:
                    nSentencEnProc.append(nSentencia)  # Agregar sentencia si es del mismo nivel y contnínua a
                    # nivMaxFirstSent
                elif dictCopy[nSentencia][0] == nivMax - 1 and nSentencia > nivMaxFirstSent:  # Si disminuye en uno el
                    # nivel, es el eLOOP
                    nSentencEnProc.append(nSentencia)  # agregar sentencia
                    break  # terminar allí


            if len(nSentencEnProc)>0:
                sentencToProc=[]
                for s in nSentencEnProc:
                    sentencToProc.append(dictCopy[s][1])#copiar las sentencias
                result=self.processSentence(sentencToProc)#procesar las sentencias seleccionadas
                newKeyBlock=len(dictBLOCKS.keys())+1#generar nueva key para BLOCKs
                dictBLOCKS[newKeyBlock]=result#agregar el resultado de las sentencias procesadas como un BLOCK

            #Cuando se tiene un nuevo BLOCK, es necesario eliminar de dictCopy a las sentencias representadas en
            # dicho BLOCK, y en su lugar deben ser reemplazadas por la palabra BLOCKn (n es el nro de BLOCK, su key)
            counter=1
            auxflag=True
            for k in dictCopy.keys():#para cada key presente en dictCopy
                if k not in nSentencEnProc:#Si la sentencia no fue procesada en un BLOCK
                    dictCopy2[counter]=dictCopy[k]#copiar la sentencia en dictCopy2
                    counter+=1
                elif auxflag:#Acá entra una única vez, cuando k por primera vez toma uno de los valores presente
                    # en nSentencEnProc
                    auxflag=False#para no volver a entrar más acá
                    auxstr="BLOCK"+str(newKeyBlock) + ":0"#generar la palabra BLOCKn:0 (el :0 es para compatibilizar el
                    # análisis de estos strings con otros del tipo DIOS:Delay:20)
                    auxniv=dictCopy[nSentencEnProc[0]][0]#el BLOCK debe insertarse con el mismo nivel que el nivel
                    # correspondiente al primer elemento en nSentencEnProc (un LOOP)
                    dictCopy2[counter] = [auxniv,auxstr]#agregar a dictCopy2 [nivel, BLOCKn:0]
                    counter += 1

            dictCopy=dictCopy2#actualizar dictCopy

            dictCopy2 = {}#reiniciar dictCopy2
            #Reiniciar nivMax
            nivMax=0
            #Reiniciar nSentencEnProc
            nSentencEnProc=[]

            #Actualizar totalSentencias
            totalSentencias = len(dictCopy.keys())
            #Esta lógica irá generando BLOCKS. Finalmente reducirá todas las sentencias a un último BLOCK y esa es la
            # señal para culminar
            if totalSentencias==1 and (dictCopy[1][1]).startswith("BLOCK"):#todas las sentencias han sido reducidas a
                # un único BLOCK
                break#salir del while totalSentencias > 0

        #Hasta aquí, el tree (su dict equivalente) ha sido analizado desde los niveles máximos hacia los mínimos
        #Ahora hay que realizar el camino inverso, reemplazando BLOCKn por lo que corresponda

        #El último newKeyBlock generado será del nivel más básico, y es lo primero en entrar al resultado final (resuFinal)
        resuFinal=dictBLOCKS[newKeyBlock]
        #Recorrer los blocks desde los últimos (niveles menores) hacia los primeros (niveles mayores)
        for i in range(newKeyBlock-1, 0, -1):
            auxi=dictBLOCKS[i]#tomar un BLOCK
            seguir=True
            while seguir:
                #la función index arroja una excepción si no encuentra lo que busca. Como debe buscar recurrentemente
                # justamente hasta no encontrar, se pone en un try. La búsqueda debe ser recurrente porque un mismo
                # BLOCK puede aparecer más de una vez
                try:
                    idx = resuFinal.index("BLOCK" + str(i) + ":0")#encontrar el idx del BLOCK pertinente
                    resuFinal = resuFinal[0:idx] + auxi + resuFinal[(idx + 1):]#concatenar eliminando a BLOCK y
                    # suplantándolo por sus sentencias respectivas
                except:
                    seguir=False
        return resuFinal

    def processSentence(self, toProc):
        #toProc es una list con las sentencias a procesar
        result=[]
        #Chequear si la primera corresponde a un LOOP
        if toProc[0].startswith("LOOP"):
            #Tomar las sentencias excluyendo LOOP (toProc[0]) y eLOOP (len(toProc)-1)
            noLoopSentenc=toProc[1:(len(toProc)-1)]
            #Tomar el order:
            order=toProc[0].split(":")
            order=(order[1]).split(' ')
            orderAux=[]
            for ord in order:
                orderAux.append(int(ord))
            order=orderAux
            idxCount=0
            for ord in order:
                idxCount+=1
                for s in noLoopSentenc:
                    aux=s.split(":")#Ej: !DIOS:GCODEord:G0 F3000 X$68.6+18.6+(ORD-1)*9.0$
                    if "GCODEord" == aux[1]:
                        gcodeproc = self.processGCODEord(aux[2], ord, idxCount)
                        # Se convierte a una orden normal de GCODE para DIOS
                        texto = "DIOS" + ":" + "GCODE" + ":" + gcodeproc[0] + ":" + gcodeproc[1]
                        result.append(texto)
                    elif "DOrd" == aux[1]:
                        # Se convierte a una orden normal de delay para DIOS
                        texto = "DIOS" + ":" + "Delay" + ":" + self.processDOrd(aux[2],ord,idxCount)
                        result.append(texto)
                    elif "TypeORD" == aux[1]:
                        #Obtener cliente que debe hacer Type
                        cliente = aux[0][1:]# eliminar "!"
                        # Se convierte a una orden normal de Type para un cliente
                        texto = cliente + ":" + "TypeORD" + ":" + str(ord)
                        result.append(texto)
                    elif "TypeORDs" == aux[1]:
                        #Obtener cliente que debe hacer Type
                        cliente = aux[0][1:]# eliminar "!"
                        # Se convierte a una orden normal de Type para un cliente
                        texto = cliente + ":" + "TypeORDs" + ":" + aux[2]
                        result.append(texto)
                    elif "TypeIDX" == aux[1]:
                        #Obtener cliente que debe hacer Type
                        cliente = aux[0][1:]# eliminar "!"
                        # Se convierte a una orden normal de Type para un cliente
                        texto = cliente + ":" + "TypeIDX" + ":" + str(idxCount)
                        result.append(texto)
                    elif aux[0].startswith("BLOCK"):
                        #Si aparece una palabra que comience en BLOCK, no procesar, solo append
                        result.append(s)
                    elif aux[0].startswith("LOOP") or aux[0].startswith("eLOOP"):#CASO ESPECIAL: LOOPS VACÍOS
                        #Cuando el usuario dejó LOOPs vacíos, al no tener sentencias dentro, son las palabras LOOP y
                        # eLOOP las que pueden dar un nivMax. En esta lógica las sentencias son procesadas por haber
                        # estado dentro de un LOOP externo y por eso se revisan las sentencias en buscar de f(ORD). Si
                        # no son encontradas en los if/elif anteriores, se asume que la sentencia debe ser inserta de
                        # forma directa con la eliminación del primer caracter ("!"). Pero en este caso el primer
                        # caracter es la L de LOOP o la e de eLOOP. Por ende, no hay que hacer nada.
                        pass#NO HACER NADA, NO AGREGAR A RESULT
                    else:#Agregar directamente
                        result.append(s[1:])# eliminar "!"

        else:#sentencias directas
            for s in toProc:
                if s.startswith("!"):
                    result.append(s[1:])#agregar directamente
                else:
                    result.append(s)  # agregar directamente
        return result

    def processDOrd(self,DOrdSentenc,numORD,idxCount):
        #Procesa una sentencia DOrd
        #DOrdSentenc es la sentencia del tipo (ORD-1)*9+20
        #numORD es el entero (de un vector ORDER) que representa a ORD en una iteracion
        #idxCount es el índice (posición) de ORD en el ORDER
        aux = DOrdSentenc.replace("ORD", str(numORD))
        aux = aux.replace("IDX", str(idxCount))
        valor = str(int(eval(aux)))#Se usa int porque los delays son en milisegundos y deben ser enteros
        return valor

    def panSecOnRunPauseResume(self, event):
        objLabel=event.GetEventObject().GetLabel()

        if objLabel=="Pause":
            self.panSecCoordSecIsBtns(['Paused','Resume',True])
            wx.CallAfter(self.FinalSecProcess,[])
        elif objLabel=="Resume":
            self.panSecCoordSecIsBtns(['Running','Pause',True])
            wx.CallAfter(self.FinalSecProcess,[])
        elif objLabel=="Run":
            #Vaciar la list de eTCoords vencidos
            self.secConfig['secEndTCoordsVencidos']=[]
            if not self.panSecOnCheck(wx.EVT_BUTTON):#check
                return
            self.secConfig['FinalSec']=[]

            searchTypeORDs=True
            resu=self.processTree2Dict(self.treeSec, searchTypeORDs)
            treeDict=resu[0]
            infoTypeORDs=resu[1]
            #Convertir tree en FinalSec
            FinalSec = self.dict2DirectSentences(treeDict)

            #Analizar infoTypeORDs
            for k in infoTypeORDs.keys():
                info=infoTypeORDs[k]
                # print info
                #info tiene esta estructura (ejemplo):
                # [1, 2, 0, 0, [['2 4 6'], ['1 3 5']]]
                #hay que obtener todas las combinaciones de texto entre elementos de order
                orders=info[4]
                nLoops=len(orders)
                for nLoop in range(0,nLoops):
                    order=orders[nLoop]
                    order=(order[0]).split(' ')
                    aux=[]
                    for ord in order:
                        aux.append(ord)
                    orders[nLoop]=aux
                    # print orders[nLoop]
                #Acá se obtienen todas las combinaciones
                if nLoops>1:
                    combinOrders=list(itertools.product(*orders))
                    infoTypeORDs[k][3] = len(combinOrders)
                else:#si nLoops es 1 y uso itertools.product(*orders), agregar una ',' innecesaria
                    combinOrders=orders[0]
                infoTypeORDs[k][4]=combinOrders
                infoTypeORDs[k][3] = len(combinOrders)

            #Ahora falta evaluar cada línea de FinalSec, en búsqueda de TypeORDs. A medida que van apareciendo,
            # se consulta su key. Con esta se entra a infoTypeORDs y se deduce cómo reemplazar a la línea en cuestión
            if len(infoTypeORDs.keys())>0:
                for linNum in range (0,len(FinalSec)):
                    lin=FinalSec[linNum]
                    linea=lin.split(':')
                    if linea[1]=='TypeORDs':
                        loopKey=linea[2]
                        # ejemplo: logTypeORDs[loopSKey] = [1, 1, 1, 9, ORDERS]  # REPET ACTUAL, MAX REPET, cuentaActual, cuentaTotal, ORDERS OF LOOPS
                        repet=infoTypeORDs[loopKey][0]
                        repetMax=infoTypeORDs[loopKey][1]
                        contAct=infoTypeORDs[loopKey][2]
                        contTotal = infoTypeORDs[loopKey][3]

                        auxstr=infoTypeORDs[loopKey][4][contAct]
                        auxi = '_'.join(auxstr)
                        linea[2]=auxi
                        if repet==repetMax:
                            infoTypeORDs[loopKey][0]=1
                            contAct=contAct+1
                            infoTypeORDs[loopKey][2]=contAct
                        elif repet<repetMax:
                            repet=repet+1
                            infoTypeORDs[loopKey][0]=repet

                        # print ':'.join(linea)
                        FinalSec[linNum]=':'.join(linea)

            # Hasta acá se procesaron todas las líneas, obteniendo una lista de líneas simples, sin loops, según
            # cómo deben ser ejecutadas y según quién deba ejecutarla
            self.secConfig['FinalSec']=FinalSec
            self.secConfig['FinalSecLineAct']=0
            self.secConfig['FinalSecNLines']=len(FinalSec)

            #Cambiar visual: ocultar Tree y mostrar List
            self.changeTreeSecFinalSec(False)

            autorizacion=self.YesNo("Sequence ready?","Sequence ready?")
            if not autorizacion:
                self.changeTreeSecFinalSec(True)
                self.panSecCoordSecIsBtns(['Stopped', 'Run', False])
                # Invocar función FinalSecProcess en estado Stopped (detener timers, recuperar listSec, etc)
                wx.CallAfter(self.FinalSecProcess, [])
                return

            # Pasar Sequence a los clientes necesarios
            self.secConfig['clientesNecFinalSec']=self.secConfig['secDictSikulis'].keys()#nombres de clientes necesarios
            auxi='#'.join(FinalSec)#genera string a partir de la lista FinalSec, uniendo las líneas de la Sequence con
            # el separador #
            #NOTA:hemos tenido problemas al pasar Sequences de muchos caracteres. Por ende, se contarán los caracteres y
            # serán enviados de a grupos
            nChars=len(auxi)
            nFragmentos=0
            self.secConfig['secDictFragmentos']={}
            for chars in range(0,nChars,1000):
                nFragmentos=nFragmentos+1
                self.secConfig['secDictFragmentos'][nFragmentos]=auxi[chars:(chars+1000)]
            msg="SEC_PASANDO_NFRAGS*"+str(nFragmentos)
            # print self.secConfig['clientesNecFinalSec']

            #Pueden existir Sequences sin la participación de clientes, solo DIOS. En ese caso
            # self.secConfig['clientesNecFinalSec'] estará vacío.
            #Ya que la Sequence inicia una vez que todos los clientes han confirmado que recibieron la Sequence,
            # no existirá esa confirmación. Por ende, debe tenerse en cuenta:
            if len(self.secConfig['clientesNecFinalSec']) == 0:#SOLO DIOS
                # Reestablecer clientesNecFinalSec
                self.secConfig['clientesNecFinalSec'] = self.secConfig['secDictSikulis'].keys()  # nombres de clientes necesarios
                # Configurar estado de la Sequence (secIs), Label de botones y presencia de Stop
                self.panSecCoordSecIsBtns(['Running', 'Pause', True])  # secIs, LabelBtnRUN, ShowStop
                # Reiniciar lista de TIMEs con el tiempo de inicio como elemento 0
                self.secConfig['secTIMES'] = [time.time()]
                # Reiniciar lista de DELAYs
                self.secConfig['secDELAYS'] = []
                self.secConfig['secDELAYSfromTC'] = []
                # Programar enviar orden para comenzar la Sequence
                wx.CallAfter(self.FinalSecProcess, [])
            else:
                for cliente in self.secConfig['clientesNecFinalSec']:
                    self.protocol.sendMsgCliente(msg.encode('ascii'), cliente.encode('ascii'))


    def changeTreeSecFinalSec(self, TreeSobreFinal = True):
        # TreeSobreFinal: True pone al Tree al frente, sino a la FinalSec
        if TreeSobreFinal:
            self.panSec.listSec.Show(False)
            self.treeSec.Show(True)
        else:
            self.panSec.listSec.Show(True)
            self.panSec.listSec.DeleteAllItems()
            self.treeSec.Show(False)
            fila=0
            for line in self.secConfig['FinalSec']:
                self.panSec.listSec.InsertStringItem(fila, line)
                fila+=1


    def FinalSecProcess(self,msgList):
        secIs=self.secConfig['secIs']
        nLinea=self.secConfig['FinalSecLineAct']
        linea=self.secConfig['FinalSec'][nLinea]

        if msgList!=[]:#si no está vacío habrá información
            if msgList[1]==str(nLinea):#es información sobre la línea actual
                if msgList[2]=="OK":
                    if nLinea+1>=self.secConfig['FinalSecNLines']:#FIN DE LA Sequence
                        if self.secConfig['secEndTCoordsVencidos']!=[]:
                            print "Time Coordinations (TC):"
                            print self.secConfig['secEndTCoordsVencidos']
                            self.secConfig['secEndTCoordsVencidos']=[]
                        print "Times in secTIMEs: "
                        print self.secConfig['secTIMES']
                        dlg = wx.MessageDialog(self, 'End of Sequence', 'Sequence finished', wx.OK|wx.ICON_INFORMATION)
                        dlg.ShowModal()
                        dlg.Destroy()
                        self.panSecCoordSecIsBtns(['Stopped','Run',False])
                        # Invocar función FinalSecProcess en estado Stopped (detener timers, recuperar listSec, etc)
                        wx.CallAfter(self.FinalSecProcess,[])
                        return
                    # Pasar a la línea siguiente
                    nLinea=nLinea+1
                    self.panSec.listSec.Select(nLinea,1)
                    self.panSec.listSec.EnsureVisible(nLinea)  # esto asegura que lo seleccionado sea visible
                    self.secConfig['FinalSecLineAct']=nLinea
                    linea=self.secConfig['FinalSec'][nLinea]
                elif msgList[2]=="FAILTCoord":#TCoords simples, si se vencen, se pasan
                    auxmsg="TC is over - line: " + str(nLinea+1) + ":" + str(linea)
                    if not self.notificaciones.IsShown():
                        self.notificaciones.Show()
                    self.notifAddNotif(auxmsg)

                    self.panSecCoordSecIsBtns(['Running','Pause',True])
                    secIs=self.secConfig['secIs']
                    # Pasar a la línea siguiente
                    nLinea=nLinea+1
                    self.panSec.listSec.Select(nLinea,1)
                    self.secConfig['FinalSecLineAct']=nLinea
                    linea=self.secConfig['FinalSec'][nLinea]

                elif msgList[2]=="FAILTCoordS":#TCoords STOP, detienen la Sequence
                    dlg = wx.MessageDialog(self, 'Error TCS - line '+str(nLinea)+':\n'+linea+ '\nSequence Paused' , 'Error in TCS', wx.OK|wx.ICON_INFORMATION)
                    dlg.ShowModal()
                    dlg.Destroy()

                    auxmsg="TCS is over - line: " + str(nLinea+1) + ":" + str(linea)
                    if not self.notificaciones.IsShown():
                        self.notificaciones.Show()
                    self.notifAddNotif(auxmsg)

                    #Pause
                    self.panSecCoordSecIsBtns(['Paused','Resume',True])
                    secIs=self.secConfig['secIs']


                else:
                    #ALERTAR
                    dlg = wx.MessageDialog(self, 'Error in line '+str(nLinea)+':\n'+linea+ '\nSequence Paused', 'Error in Sequence', wx.OK|wx.ICON_INFORMATION)
                    dlg.ShowModal()
                    dlg.Destroy()
                    self.panSecCoordSecIsBtns(['Paused','Resume',True])
                    secIs=self.secConfig['secIs']

        #Seleccionar la linea actual
        self.panSec.listSec.Select(nLinea,1)
        self.panSec.listSec.EnsureVisible(nLinea)#esto asegura que lo seleccionado sea visible

        if secIs=="Running":
            texto=linea.split(":")#convertir a list
            cliente=texto[0]
            comando=texto[1]
            if len(texto)>2:
                extra=texto[2]#podría ser un ord, pero otras instrucciones pueden tener otros extra

            msg="SEC*"+str(nLinea)+"*"
            if comando not in self.secConfig['NoSikuCommands']:#entonces es un sikuli
                #SIKULIS
                msg=msg+"SIKU"
            else:#NO SIKULIS
                msg=msg+"0"


            # NOTA: EL MENSAJE SE ENVÍA A TODOS LOS CLIENTES, DE ESA MANERA TODOS ACTUALIZARÁN SU ESTADO
            if len(self.secConfig['clientesNecFinalSec']) is not 0:
                self.protocol.sendMsg(msg.encode('ascii'))

            # MÁS ALLÁ DEL ENVÍO DEL MENSAJE, SI ERA PARA DIOS, DEBE EJECUTARSE Y AUTO-RESPONDER
            if cliente=="DIOS":
                if comando=="Time":
                    t=time.time()
                    t=round(t, 3)#miliSeg
                    auxmsg = "{:.3f}".format(t)
                    if not self.notificaciones.IsShown():
                        self.notificaciones.Show()
                    self.notifAddNotif(auxmsg)
                    self.secConfig['secTIMES'].append(t)
                    wx.CallAfter(self.FinalSecProcess,['SEC',str(nLinea),'OK'])
                    return
                elif comando.startswith("TC"):
                    #!DIOS:TCNN:MM
                    #Tomar tiempo
                    t=time.time()#SEGUNDOS!
                    #Obtener cuál TCoord era
                    if comando.startswith("TCS"):
                        TcoordN=int(comando[3:])
                    elif comando.startswith("TC"):
                        TcoordN=int(comando[2:])
                    #Insertar tiempo en diccionario, y modificar el tiempo del eTCoord respectivo
                    tiempos=self.secConfig['secDictTCoords'][TcoordN]
                    tiempos[0]=t
                    tiempos[1]=float(tiempos[2]/1000)+tiempos[0]
                    self.secConfig['secDictTCoords'][TcoordN]=tiempos
                    wx.CallAfter(self.FinalSecProcess,['SEC',str(nLinea),'OK'])
                    return
                elif comando.startswith("eTC"):
                    #!DIOS:eTCNN:MM
                    #Obtener cuál eTCoord era
                    flagStop=False #Depende de si son TCoord o TCoordS (STOP)
                    if comando.startswith("eTCS"):
                        endTCoordN=int(comando[4:])
                        flagStop=True
                    elif comando.startswith("eTC"):
                        endTCoordN=int(comando[3:])
                    #Obtener el tiempo programado:
                    endT=self.secConfig['secDictTCoords'][endTCoordN][1]
                    #Verificar si ya es demasiado tarde:
                    ahora=time.time()
                    if endT<=ahora:
                        #Registrar tiempo vencido y tiempo en que se detectó el vencimiento
                        endTvencidos=str(ahora)+" should have been detected in: "+str(endT)
                        self.secConfig['secEndTCoordsVencidos'].append(endTvencidos)
                        if flagStop:
                            wx.CallAfter(self.FinalSecProcess,['SEC',str(nLinea),'FAILTCoordS'])
                        else:
                            wx.CallAfter(self.FinalSecProcess,['SEC',str(nLinea),'FAILTCoord'])
                    else:
                        milisegInt=int(round(1000.0*(endT-ahora)))#round redondea pero retorna float, por eso luego se usa int
                        #Programar llamada a FinalSecProcess luego de milisegInt milisegundos
                        self.secConfig['secDELAYSfromTC'].append(wx.CallLater(milisegInt, self.FinalSecProcess, ['SEC', str(nLinea), 'OK']))
                        #Iniciar el timer que muestra cuántos segundos faltan
                        self.panSec.subpanControl.staticTCoordRestante.Show(True)
                        segLabel=str(int(milisegInt/1000))
                        self.panSec.subpanControl.staticTCoordRestante.SetLabel(segLabel)
                        self.timerTCRestante = wx.Timer(self)
                        #Notar que el Bind del timer es con respecto al Frame (self)
                        self.Bind(wx.EVT_TIMER, self.updateTCoordRestante, self.timerTCRestante)
                        self.timerTCRestante.Start(1000)

                        #ANTIGUO: con while, no callLater
                        #uso while con peligro de bloquear la GUI. Podría usar wx.CallLater y programar un delay
                        #o bien usar wx.FutureCall(tiempoMiliSegDespues, self.FuncionAEjecutar)

                        # diferencia=int(endT-ahora)#indicador de segundos restantes
                        # # print diferencia
                        # # self.panSec.subpanControl.staticTCoordRestante.SetLabel(str(diferencia))
                        # while endT>ahora:
                        #     dif=int(endT-ahora)
                        #     if dif!=diferencia:
                        #         diferencia=dif
                        #         # self.panSec.subpanControl.staticTCoordRestante.SetLabel(str(diferencia))
                        #         # print diferencia
                        #     ahora=time.time()#esperar
                        # #Registrar tiempo vencido y tiempo en que se detectó el vencimiento
                        # endTOK=str(endT)+" detectado OK en: "+str(ahora)
                        # self.secConfig['secEndTCoordsVencidos'].append(endTOK)
                        # wx.CallAfter(self.FinalSecProcess,['SEC',str(nLinea),'OK'])
                    return
                elif comando=="Delay":
                    #en texto[2] está el delay
                    self.secConfig['secDELAYS'].append(wx.CallLater(int(texto[2]), self.FinalSecProcess,['SEC',str(nLinea),'OK']))
                    return
                elif comando=="Pause":
                    self.panSecCoordSecIsBtns(['Paused','Resume', True])
                    wx.CallAfter(self.FinalSecProcess,['SEC',str(nLinea),'OK'])
                    return
                elif comando=="GCODE":
                    gcodefile=texto[2]
                    gcodepath=texto[3]
                    if self.OSname == "nt":
                        gcodepath=gcodepath+":"+texto[4]
                    gcode="load "+ gcodepath+"/"+gcodefile
                    self.pronSend(wx.EVT_BUTTON,gcode)
                    return
                elif comando=="MsgBox":
                    extra = extra.split("&")  # convertir a list
                    extra[0]='-'+extra[0]
                    extra = '\n-'.join(extra)

                    dlg = wx.MessageDialog(self, extra, 'Message:',
                                           wx.OK | wx.ICON_INFORMATION)
                    dlg.ShowModal()
                    dlg.Destroy()
                    wx.CallAfter(self.FinalSecProcess,['SEC',str(nLinea),'OK'])
                    return
                elif comando == "Notif":
                    if not self.notificaciones.IsShown():
                        self.notificaciones.Show()
                    extra = extra.split("&")  # convertir a list
                    extra[0]='-'+extra[0]
                    extra = '\n-'.join(extra)
                    self.notifAddNotif(extra)
                    wx.CallAfter(self.FinalSecProcess, ['SEC', str(nLinea), 'OK'])
                    return




        elif secIs=="Paused":
            pass
        elif secIs=="Stopped":
            #Informar tiempos
            if self.secConfig['secEndTCoordsVencidos']!=[]:
                print "Tiempos Coordinados:"
                print self.secConfig['secEndTCoordsVencidos']
                self.secConfig['secEndTCoordsVencidos']=[]

            print "Tiempos en secTIMEs: "
            print self.secConfig['secTIMES']

            self.changeTreeSecFinalSec(True)

            # Detener todos los timers (por las dudas, ya que algunos ya pueden haber sido ejecutados)
            for delay in self.secConfig['secDELAYS']:
                print str(delay) +":HasRun:"+str(delay.HasRun())+":IsRunning:"+str(delay.IsRunning())
                #darle stop al timer
                delay.Stop()
            self.secConfig['secDELAYS']=[]
            #Detener los delays derivados de TC
            for delayFromTC in self.secConfig['secDELAYSfromTC']:
                print str(delayFromTC) +":HasRun:"+str(delayFromTC.HasRun())+":IsRunning:"+str(delayFromTC.IsRunning())
                #darle stop al timer
                delayFromTC.Stop()
            self.secConfig['secDELAYSfromTC']=[]
            #Detener timer de TCRestante (cuenta hacia atrás)
            try:#Puede que este timer no haya sido creado, por eso try
                self.timerTCRestante.Stop()
            except:
                pass
            self.panSec.subpanControl.staticTCoordRestante.SetLabel(" ")
            self.panSec.subpanControl.staticTCoordRestante.Show(False)

            #Esto volverá a actualizarse en panSecOnCheck
            self.secConfig['secListaGCODES'] = []  # vaciar lista de GCODEs necesarios (NO GCODEord)
            self.secConfig['secFlagExistsGCODEord'] = False  # Indicar que no hay GCODEords
            #Borrar todos los archivos gcode temporales (empiezan con TEMP_)
            self.deleteAllTempGCODE()



    def panSecOnCheck(self, event):
        self.notifClear()#Borrar notificaciones si existen
        self.notificaciones.Show()
        OK=False
        self.SecUpdateWorkingClientsAndSikulis()
        self.SecUpdateGCODESNec()
        if not self.SecCheckClientsOnline():
            return OK
        if not self.SecCheckClientsSikuSRVStatus():
            return OK
        if not self.SecCheckClientsSikus():
            return OK
        if len(self.secConfig['secListaGCODES'])!=0:#si en la Sequence hay GCODES (no GCODEords):
            #Chequear que los files pertinentes estén donde deban estar, y que culminen en M400
            if not self.SecCheckPronGCODES():
                return OK

        if self.secConfig['secFlagExistsGCODEord'] or len(self.secConfig['secListaGCODES'])!=0:#Si hay GCODEords o GCODE,
            #  chequear si la impresora hizo G28
            if not self.testG28():
                return OK


        dlg = wx.MessageDialog(self,"Clients, Sikulis and GCODES ready", 'Sequence OK', wx.OK|wx.ICON_INFORMATION)
        dlg.ShowModal()
        dlg.Destroy()
        OK=True
        return OK

    def SecUpdateGCODESNec(self):
        listaGCODES=[]#lista de gcodes
        #Obtener diccionario del tree
        log=self.processTree2Dict(self.treeSec)
        totalSentencias=len(log.keys())
        if totalSentencias==0:#Si no hay elementos para salvar, salir
            self.secConfig['secListaGCODES'] = listaGCODES  # vaciar lista de GCODES
            return

        for s in range(1,totalSentencias+1):#para cada sentencia:
            texto = log[s][1]

            if texto.startswith('!'):#es comando para un cliente o DIOS, pueden ser sikulis y otros comandos
                texto=texto[1:]# eliminar "!"
                texto=texto.split(":")#convertir a list
                cliente=texto[0]
                comando=texto[1]

                if cliente == "DIOS" and comando == "GCODEord":  # LOS GCODESord SON PARA DIOS pero requieren otro
                    # tratamiento ya que en este punto aun no han sido generados los files temporales respectivos
                    # NO AGREGAR A self.secConfig['secListaGCODES']
                    self.secConfig['secFlagExistsGCODEord']=True

                if cliente =="DIOS" and comando == "GCODE":#LOS GCODES SON PARA DIOS
                    gcodefile=texto[2]
                    # NOTA: En windows se necesita el ":" para los paths. Al hacer el split de texto con ":", un path
                    # del tipo C:\GCODESdir queda "C" y "\GCODESdir"
                    gcodepath=texto[3]
                    if self.OSname == "nt":
                        gcodepath=gcodepath+":"+texto[4]
                    gcode=gcodepath+"/"+gcodefile
                    listaGCODES.append(gcode)#agregarlo a listaGCODES

        # Actualizar en secConfig
        self.secConfig['secListaGCODES']=listaGCODES#lista de gcodes

    def SecUpdateWorkingClientsAndSikulis(self):
        listaCl=[]#lista de clientes
        dictSk={}#diccionario de sikulis
        nSentencias=self.treeSec.GetCount()#Incluye al root
        if nSentencias==1:#Si no hay elementos en la Sequence (el único elemento es el root Sequence)
            self.secConfig['secListaClientes']=listaCl#vaciar lista de clientes
            self.secConfig['secDictSikulis']=dictSk#vaciar diccionario de sikulis
            return

        log=self.processTree2Dict(self.treeSec)

        for s in range(1,nSentencias):#para cada sentencia:
            # texto=log[s][2]
            texto=log[s][1]
            if texto.startswith('!'):#es comando para un cliente o DIOS, pueden ser sikulis y otros comandos
                texto=texto[1:]# eliminar "!"
                texto=texto.split(":")#convertir a list
                cliente=texto[0]
                comando=texto[1]

                if cliente not in listaCl and cliente!="DIOS":#no existe en la listaCl?
                    listaCl.append(cliente)#agregarlo a listaCl
                    dictSk[cliente]= []#lista vacía de sikulis


                #Detectar si eran sikulis o no
                if comando not in self.secConfig['NoSikuCommands'] and cliente!="DIOS":#entonces es un sikuli
                    if comando not in dictSk[cliente]:#el sikuli aun no había sido agregado?
                        dictSk[cliente].append(comando)#agregar sikuli

        # Actualizar en secConfig
        self.secConfig['secListaClientes']=listaCl#lista de clientes
        self.secConfig['secDictSikulis']=dictSk#diccionario de sikulis

    def SecCheckClientsSikuSRVStatus(self):
        OK=True
        clientesMal=[]
        for cliente in self.secConfig['secListaClientes']:
            if cliente not in self.sikuServersAlreadyON and cliente!="DIOS":
                OK=False
                clientesMal.append(cliente)
        if not OK:
            texto='\n'.join(clientesMal)
            # print texto
            dlg = wx.MessageDialog(self,"There exist Clients with Sikuli Server OFF:\n"+texto+ "\n If they are already ON, restart Sikuli Server.",
                                   'SikuSRV of some Clients OFF', wx.OK|wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()
        return OK


    def SecCheckClientsOnline(self):
        # self.listaClientes contiene a los online actualizados. Los que estén en la Sequence tienen que
        # estar en self.listaClientes (no necesariamente al revés, puede haber clientes online que no sean
        # usados en la Sequence)
        OK=True
        clientesMal=[]
        for cliente in self.secConfig['secListaClientes']:
            if cliente not in self.listaClientes and cliente!="DIOS":
                OK=False
                clientesMal.append(cliente)
        if not OK:
            texto='\n'.join(clientesMal)
            # print texto
            dlg = wx.MessageDialog(self,"There exist needed Clients not connected:\n"+texto, 'Needed Clients OFFLINE', wx.OK|wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()
        return OK

    def SecCheckPronGCODES(self):
        OK=True
        texto=""
        filesAusentes=[]
        # Chequear que los GCODES necesarios en  self.secConfig['secListaGCODES']
        # 1- Existan donde deben estar:
        gcodesNec=self.secConfig['secListaGCODES']
        for gcode in gcodesNec:
            if not os.path.isfile(gcode):
                filesAusentes.append(gcode)
                OK=False

        if not OK:
            texto='\n'.join(filesAusentes)
            dlg = wx.MessageDialog(self,str(len(filesAusentes))+" GCODES needed not available:\n"+texto, 'GCODES needed not available', wx.OK|wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()
            return OK

        # 2- Que terminen en M400
        for gcode in gcodesNec:
            self.pronCheckEndM400(gcode,True)#si no tienen M400, lo agregará en esta instancia

        return OK

    def testG28(self):
        # test: Que la impresora haya hecho su G28 (self.pronG28_OK)
        OK=True
        if not self.pronG28_OK:
            OK=False
            dlg = wx.MessageDialog(self,"3D Printer must be connected and\n"+"it must be done its Z homing(G28)", '3D Printer not ready', wx.OK|wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()
        return OK


    def SecCheckClientsSikus(self):
        OK=True
        sikusMal=[]
        clientes=self.secConfig['secDictSikulis'].keys()#nombres de clientes necesarios
        for cliente in clientes:#para cada cliente:
            listaSiku=self.secConfig['secDictSikulis'][cliente]#lista de sikulis necesarios de ese cliente
            if listaSiku != []:#si no está vacío, hay sikulis
                clientListSikulis=self.dictSikulis[cliente]#todos los sikulis del cliente (necesarios o no)
                for siku in listaSiku:#para cada sikuli necesario del cliente:
                    aux=siku+".sikuli"#es porque en self.dictSikulis se guardan con .sikuli
                    if aux not in clientListSikulis:#falta sikuli
                        OK=False
                        texto=cliente +":"+siku
                        sikusMal.append(texto)
        if not OK:
            texto='\n'.join(sikusMal)
            dlg = wx.MessageDialog(self,"There exist needed sikulis not available:\n"+texto, 'Needed sikulis not available', wx.OK|wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()

        return OK

    def deleteAllTempGCODE(self):
        #Borrar todos los gcodes que empiecen con "TEMP_"
        filelist = [f for f in os.listdir(".") if f.endswith(".gcode") and f.startswith("TEMP_")]
        for f in filelist:
            os.remove(f)

    def changenTimesTimeCoord(self,event):#para cambiar el parámetro nTimesTimeCoord
        nTimes = self.boxNTimesTimeCoord.GetValue()
        nTimes = nTimes.strip(' ')
        # Genero una lista, aunque sólo debería haber 1 valor. Si hay más, no serán analizados y serán descartados.
        nTimeslist = nTimes.split()
        if nTimeslist[0].isdigit() and (int(nTimeslist[0]) % 2) != 0 and int(nTimeslist[0]) > 0:#que sea dígito entero impar positivo
                self.nTimesTimeCoord = int(nTimeslist[0])
                self.boxNTimesTimeCoord.SetValue(str(self.nTimesTimeCoord))
        else:
            dlg = wx.MessageDialog(self, "It must be integer, odd and positive", 'Median parameter',
                                   wx.OK | wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()
            self.boxNTimesTimeCoord.SetValue(str(self.nTimesTimeCoord))

    def coordTimes(self, evt):
        if self.IsConectado and len(self.listaClientes) != 0:
            #Solicitar que el flag de tiempos coordinados pase a False en los clientes
            self.protocol.sendMsg("CFG_NOT_COORD_TIME")

    def send(self, evt):
        self.protocol.sendMsg(str(self.ctrl.GetValue()))
        self.ctrl.SetValue("")


    def cambiarConfig(self, evt):
        self.servidor=self.boxServidor.GetValue()
        self.puerto=int(self.boxPuerto.GetValue())
        self.configFile = open("configServ", "w+")#abrir como escritura y lectura
        self.configFile.seek(0,0)
        self.configFile.writelines([self.servidor+"\n", str(self.puerto)+"\n"])
        self.configFile.close()


    def DoExit(self, event):
        reactor.stop()


    def OnExeSikuli(self,event):
        #Corroborar que haya un cliente seleccionado y un sikuli seleccionado
        if self.comboListaClientes.GetCurrentSelection() is not -1 and self.comboDictSikulis.GetCurrentSelection() is not -1 :
            cliente=(self.comboListaClientes.GetStringSelection()).encode('ascii')
            msg="EXE_SIKULI"+"*"+(self.comboDictSikulis.GetStringSelection()).encode('ascii')
            self.protocol.sendMsgCliente(msg, cliente)

    def updateListaClientes(self):
        self.listaClientes=[]
        for client in self.protocol.factory.clients:
            if client.name is not "NN":
                self.listaClientes.append(client.name)
        self.listaClientes.sort()
        auxDict={}
        for client in self.listaClientes:
            ip=self.dictClientesIPs[client][0]
            if ip in self.serverIP or ip=='127.0.0.1':
                auxDict[client]=[ip, 1] # Local
            else:
                auxDict[client]=[ip, 0] # Non-Local
        self.dictClientesIPs=auxDict


        self.updateComboListaClientes()
        self.updateComboDictSikulis(wx.EVT_COMBOBOX)

    def updateDictSikulis(self, clientName, clientListSikulis):
        self.dictSikulis[clientName]=clientListSikulis
        self.updateComboDictSikulis(wx.EVT_COMBOBOX)

    def updateComboListaClientes(self):
        self.comboListaClientes.Clear()
        self.comboListaClientes.AppendItems(self.listaClientes)
        if self.comboListaClientes.GetCount():#si hay al menos 1:
            self.comboListaClientes.SetValue(str(self.comboListaClientes.GetCount())+" Clients")
        else:
            self.comboListaClientes.SetValue("0 Clients")

    def updateComboDictSikulis(self,event):
        if self.comboListaClientes.GetCurrentSelection() is not wx.NOT_FOUND:
            lista=self.dictSikulis[self.comboListaClientes.GetStringSelection()]
            lista.sort()
            self.comboDictSikulis.Clear()
            if lista==['']:
                pass
            else:
                self.comboDictSikulis.AppendItems(lista)

            if self.comboDictSikulis.GetCount():#si hay al menos 1:
                self.comboDictSikulis.SetValue(str(self.comboDictSikulis.GetCount())+" Sikulis")
            else:
                self.comboDictSikulis.SetValue("0 Sikulis")
        else:
            self.comboDictSikulis.Clear()
            self.comboDictSikulis.SetValue("Sikulis")


    def OnConectar(self, event):
        if self.primeraConexion:
            try:
                self.serverPort=reactor.listenTCP(self.puerto, EchoServerFactory(self))
            except twisted.internet.error.CannotListenError, ex:
                print "Port %d busy: %s" % (self.puerto, ex)
                self.text.SetValue(self.text.GetValue() + "Port " + str(self.puerto) + " busy?.\n")

            else:
                self.primeraConexion=False
                self.IsConectado=True
                self.OnConectarUpdate()

        elif self.IsConectado:#Quiere desconectar:
            #NOTA: no habrá una desconexión real aun llamando a stopListeing(). Si al menos un cliente se encuentra
            #conectado, entonces aun podrá interaccionar con el server. stopListening() resulta en un deferred.
            # LEER: https://jml.io/pages/how-to-disconnect-in-twisted-really.html
            #     The server must stop listening.
            #     The client connection must disconnect.
            #     The server connection must disconnect.
            #
            # You have to make each of these things happen. That’s easy. Here’s the tricky part: you have to wait for
            # each of these things to happen.

            self.serverPort.stopListening()
            self.IsConectado=False
            self.OnConectarUpdate()

        elif not self.IsConectado:#Quiere Conectar:
            try:
                self.serverPort.startListening()
            except twisted.internet.error.CannotListenError, ex:
                print "Port %d is busy: %s" % (self.puerto, ex)
                self.text.SetValue(self.text.GetValue() + "Port " + str(self.puerto) + " seems to be busy.\n")
            else:
                self.IsConectado=True
                self.OnConectarUpdate()

    def OnConectarUpdate(self):
        if self.IsConectado:
            self.btnConectar.SetLabel("Disconnect")
            self.boxServidor.SetEditable(False)
            self.boxPuerto.SetEditable(False)
            self.serverPortStateText.SetLabel("connected")
        else:
            self.btnConectar.SetLabel("Connect")
            self.boxServidor.SetEditable(True)
            self.boxPuerto.SetEditable(True)
            self.serverPortStateText.SetLabel("disconnected")


ID_EXIT  = 101

class ServerProtocol(basic.LineReceiver):
    def __init__(self):
        self.name= "NN"
        # Atención: persistent configuration is not saved in the Protocol.
        # The persistent configuration is kept in a Factory class
        #ATENCIÓN: la idea es que cada instancia del Protocolo no conserva información
        # persistente. Quien conserva esa información es la factory que comparten entre todas las instancias

        # (Esta frase proviene de otro ejemplo )
        # Here connectionMade and connectionLost cooperate to keep a count of the active
        #  protocols IN A SHARED OBJETC, the factory.
        # The factory must be passed to Echo.__init__ when creating a new instance.
        # The factory is used to share state that exists beyond the lifetime of any given connection.
        #     def __init__(self, factory):
        #         self.factory = factory


    def connectionMade(self):
        self.activo = 0
        # The factory is used to share state that exists beyond the
        # lifetime of any given connection
        self.factory.clients.append(self)
        print "Connection from: "+ self.transport.getPeer().host

    def connectionLost(self, reason):
        self.sendMsg("- %s left." % self.name)
        print "- Connection lost: "+ self.name
        try:
            self.factory.clients.remove(self)
            self.factory.gui.updateListaClientes()
        except ValueError, ex:
            print "Error ValueError: %s" % (ex)




    def lineReceived(self, line):
        gui = self.factory.gui
        gui.protocol = self

        linea= line.split("*")
        param=linea[0]
        if len(linea)>1:
            valores=linea[1]

        if param == "SEC":
            # PROCESAR APARTE, EN LA GUI
            wx.CallAfter(gui.FinalSecProcess,linea)
            return

        elif param=="RUN":#por ej para OnRunSikulisFromServer
            aux=valores.split("#")
            if aux[0]=="OK":
                val = gui.text.GetValue()
                gui.text.SetValue(val+'\n'+'\n' + "RUN OK " + aux[1] +'\n'+'\n')
                gui.text.SetInsertionPointEnd()
            elif aux[0]=="FAIL":
                val = gui.text.GetValue()
                gui.text.SetValue(val+'\n'+'\n' + "RUN FAIL " + aux[1] +'\n'+'\n')
                gui.text.SetInsertionPointEnd()
            # return
        elif param=="NOMBRE":
            if not self.activo:
                self.username(valores)
                # return
        elif param=="INFORM_SIKULIS":
            self.factory.gui.updateDictSikulis(self.name, valores.split("#"))
            # return
        elif param=="CFG_NOT_COORD_TIME":
            self.factory.gui.dictTimeCoordServer[self.name] = []
            self.factory.gui.dictTimeCoordClients[self.name] = []
            self.sendMsgCliente("CFG_COORD_TIME", self.name)
            tDesp = time.time()
            self.factory.gui.dictTimeCoordServer[self.name].append(tDesp)
        elif param=="CFG_COORD_TIME":
            #Al tiempo devuelto por un cliente, agregarlo.
            self.factory.gui.dictTimeCoordClients[self.name].append(float(valores))
            nElems=len(self.factory.gui.dictTimeCoordClients[self.name])
            #Si aun no se colectaron suficientes tiempos para el cálculo de la mediana de las diferencias de tiempos,
            #seguir pidiendo tiempos
            if nElems < self.factory.gui.nTimesTimeCoord:
                # tAntes=time.time()
                self.sendMsgCliente("CFG_COORD_TIME",self.name)
                tDesp=time.time()
                # self.factory.gui.dictTimeCoordServer[self.name].append(tAntes)
                self.factory.gui.dictTimeCoordServer[self.name].append(tDesp)
            #Se tiene el número de tiempos requeridos para el cálculo
            elif nElems == self.factory.gui.nTimesTimeCoord:
                listServ=self.factory.gui.dictTimeCoordServer[self.name]
                listClient=self.factory.gui.dictTimeCoordClients[self.name]
                listResult=[]
                cuenta=0
                for t in listClient:
                    #Promediar mediciones de tiempo en servidor antes y después de enviar el mensaje
                    # aux=statistics.mean([listServ[cuenta],listServ[cuenta+1]])
                    #Solo tiempoDespues
                    tS = listServ[cuenta]
                    #Calcular diferencia de tiempo servidor-cliente (tCliente=tServer+offset)
                    offset_tClient_tServer=t-tS
                    #Agregar esa diferencia
                    listResult.append(offset_tClient_tServer)
                    cuenta = cuenta + 1

                #Calcular mediana
                mediana=round(statistics.median(listResult),3)
                self.factory.gui.dictTimeCoordMedian[self.name]=mediana
                print "New time median for Client " + self.name + ":" + "{:.3f}".format(mediana)

                #Avisar al cliente que terminó la coordinación de tiempos
                self.sendMsgCliente("CFG_COORD_TIME_DONE", self.name)
                #Luego de coordinar, solicitar sikulis
                self.sendMsgCliente("INFORM_SIKULIS",self.name)
            # return
        elif param=="SIKUSRV_OFF":
            self.factory.gui.changeTreeSecFinalSec(True)#Esconder la lista y mostrar el tree
            dlg = wx.MessageDialog(self.factory.gui, str(self.name) + ': sikuSRV OFF'+'\nPlease turn sikuSRV on.', 'sikuSRV OFF', wx.OK|wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()

        elif param=="TREPORT":#EXPERIMENTAL
            #NOTA: si el cliente que realiza el TREPORT es local (misma PC que DIOS) entonces no se realizan correcciones
            # sobre los tiempos informados.
            #NOTA: Se opera con 3 decimales (milisegundos)
            # (tCliente = tServer + offset)
            offset=0.0
            auxList = ["TIME REPORT FROM " + str(self.name) +" (offset: LOCAL) :" ]
            if self.factory.gui.dictClientesIPs[self.name][1]==0:#Si no es Local
                offset=self.factory.gui.dictTimeCoordMedian[self.name]
                auxList = ["TIME REPORT FROM " + str(self.name) + " (offset: " + "{:.3f}".format(offset) + "):"]

            TimeReport=valores
            TimeReport = TimeReport.split(",")
            for TR in range(1,len(TimeReport)):
                auxstr=(TimeReport[TR]).strip(" ")
                auxstr = auxstr.strip("\r")
                auxstr = auxstr.strip("]")
                auxstr=auxstr.strip("'")
                if offset is not 0.0:
                    auxfloat=float(auxstr)-offset #(tCliente = tServer + offset)
                    auxstr="{:.3f}".format(auxfloat)
                auxList.append(auxstr)
            auxstr = '\n'.join(auxList)
            self.factory.gui.notifAddNotif(auxstr)

        elif param=="SIKU_SRV_STATUS":
            status=valores
            if status=="ON":
                if not self.name in self.factory.gui.sikuServersAlreadyON:
                    self.factory.gui.sikuServersAlreadyON.append(self.name)

            elif status=="OFF":
                if self.name in self.factory.gui.sikuServersAlreadyON:
                    self.factory.gui.sikuServersAlreadyON.remove(self.name)

        elif param=="SEC_PASANDO_NFRAGS":#los clientes confirman que van a recibir fragmentos
            nombre=valores
            #Pasar al cliente los fragmentos
            for Nfragmento in gui.secConfig['secDictFragmentos'].keys():
                msg=("SEC_PASANDO_FRAGS*"+str(Nfragmento)+"*"+gui.secConfig['secDictFragmentos'][Nfragmento]).encode('ascii')
                self.sendMsgCliente(msg,self.name)

        elif param=="SEC_PASANDO":
            nombre=valores
            # Remover nombre de clientesNecFinalSec
            if nombre in gui.secConfig['clientesNecFinalSec']:
                gui.secConfig['clientesNecFinalSec'].remove(nombre)

            # Cuando todos los clientes necesarios hayan respondido y por ende sido removidos, la lista quedará vacía
            # y será la señal para comenzar la Sequence

            if len(gui.secConfig['clientesNecFinalSec'])==0:
                # Reestablecer clientesNecFinalSec
                gui.secConfig['clientesNecFinalSec']=gui.secConfig['secDictSikulis'].keys()#nombres de clientes necesarios
                # Configurar estado de la Sequence (secIs), Label de botones y presencia de Stop
                gui.panSecCoordSecIsBtns(['Running','Pause',True])#secIs, LabelBtnRUN, ShowStop
                # Reiniciar lista de TIMEs con el tiempo de inicio como elemento 0
                gui.secConfig['secTIMES']=[time.time()]
                # Reiniciar lista de DELAYs
                gui.secConfig['secDELAYS']=[]
                # Programar enviar orden para comenzar la Sequence
                wx.CallAfter(gui.FinalSecProcess,[])

        if gui:
            val = gui.text.GetValue()
            gui.text.SetValue(val + line + '\n')
            gui.text.SetInsertionPointEnd()
        # De http://stackoverflow.com/questions/27042943/how-to-use-datareceived-in-twisted
        # in order to avoid delays in communications of the server,
        # one should use return at the end of dataReceived() or lineReceived() functions
        return

    def checkSTRforChars(self,STR,Chars=[]):
        OK=True
        for carac in Chars:
            if STR.find(carac) is not -1:
                OK=False
                break
        return OK

    def username(self, line):
        datos = line.split("#")
        nombre=datos[0]
        GUIactivo=datos[1]

        #Por cuestiones de codificación, los nombres no pueden ser repetidos, ni tener
        #los caracteres "*", "#", "_"
        if not self.checkSTRforChars(nombre,["*","#","_"]):
            print "Change user name"
            self.sendLine("ERR_NOMBRE")
            self.transport.loseConnection()
            return

        #NOTA: la autenticación se da al inicio. El cliente envía su nombre al inicio.
        #Es posible que un cliente ya conectado, haya perdido la conexión por alguna razón.
        #Al reconectar, declarará un nombre de usuario que ya estará en uso. Sólo habiendo
        #estado conectado y PREVIAMENTE AUTENTICADO habrá recibido CFG_NOMBREOK y por ende
        # su GUIactivo valdra "1". En ese caso, en la lista de clientes de la
        #factory habrá 2 nombres repetidos. El primero de ellos ya no debe contar, pues en la
        #reconexión se genera una nueva instancia del protocolo. Por ende, si GUIactivo es "1"
        #se usa  self.factory.clients.remove(x). En ese caso, x será la instancia que no debe ser
        #más válida.
        for x in self.factory.clients:
            if x.name == nombre:
                if GUIactivo=="1":
                    print "Already active?"
                    ####TODO ANTES TRANSFERIR AL OTRO CLIENTE LA INFO
                    self.factory.clients.remove(x)
                    break
                elif GUIactivo=="0":
                    print "Change user name"
                    self.sendLine("ERR_NOMBRE")
                    self.transport.loseConnection()
                    return

        self.name = nombre
        self.activo = 1
        self.sendMsgCliente("CFG_NOMBREOK",self.name)
        # print '%s is now known as %s' % (self.transport.getPeer().host, self.name)
        self.factory.gui.dictClientesIPs[self.name]=[self.transport.getPeer().host, 0] #IP Client, 0/1 NoLocal/Local
        self.factory.gui.updateListaClientes()

        #COORDINACIÓN INICIAL DE TIEMPOS
        self.factory.gui.dictTimeCoordServer[self.name]=[]#lista vacía
        self.factory.gui.dictTimeCoordClients[self.name]=[]#lista vacía

        #Primer envío de coordinación
        self.sendMsgCliente("CFG_COORD_TIME",self.name)
        tDesp=time.time()
        self.factory.gui.dictTimeCoordServer[self.name].append(tDesp)


    def sendMsg(self, message):#este método envía el mensaje a todos los clientes
        for client in self.factory.clients:
            # NOTA sendLine: The line to send, not including the delimiter
            client.sendLine(message)
            # client.sendLine(message+'\n')

    def sendMsgCliente(self, message, nombre):#este método envía el mensaje AL CLIENTE ESPECIFICADO
        # del tipo # self.sendMsgCliente("lo que haya que poner" , self.name)
        for client in self.factory.clients:
            # print client.name
            if client.name==nombre:
                client.sendLine(message)
                break
                # client.sendLine(message+'\n')

    def printSikulis(self):
        for client in self.factory.clients:
            if client.name is not "NN":
                print self.factory.gui.dictSikulis[client.name]



class EchoServerFactory(protocol.ServerFactory):
    #La idea es q como esta factory sólo producirá un tipo de protocolo, en lugar de
    #definir la función buildProtocol, se define EL ATRIBUTO protocol DE LA FACTORY

    # https: // twistedmatrix.com / documents / current / core / howto / servers.html  # simpler-protocol-creation
    # For a factory which simply instantiates instances of a specific protocol class,
    # there is a simpler way to implement the factory. The default implementation of
    # the buildProtocol method calls the protocol attribute of the factory to create a
    # Protocol instance, and then sets an attribute on it called factory which points to the factory itself
    protocol = ServerProtocol

    def __init__(self, gui):
        self.gui = gui
        self.clients = []


def DIOS():
    app = wx.App(False)
    frame = ChatFrame()
    frame.Show()
    reactor.registerWxApp(app)

    def alCerrar():
        # VER SI PRONSOLE SEGUÍA CONECTADO, Y EN TAL CASO DESCONECTAR
        # TODO bug en windows: Si antes de salir no se desconecta la impresora, suceden cosas:
        # 1- el puerto usado por la impresora no estará disponible al iniciar nuevamente el servidor.
        # Hay que cerrar sesión en windows
        # 2- Es posible que al querer conectar nuevamente el servidor (no la impresora) el puerto que se estaba usando
        # se encuentre ocupado (ERROR: Couldn't listen on any:5001: [Errno 10048] Sólo se permite un uso de cada
        # dirección de socket (protocolo/dirección de red/puerto))
        # Estos errores NO OCURREN EN LINUX

        #Las siguientes líneas no han funcionado:
        # print frame.pronTranspProc
        # print frame.pronProtProc.conectado
        # if frame.pronTranspProc is not None:
        #     frame.pronTranspProc.write("disconnect" + "\n")
        #     frame.pronTranspProc.write("exit" + "\n")
        #     frame.pronTranspProc=None
        reactor.stop()

    atexit.register(alCerrar)

    reactor.run()



if __name__ == "__main__":
    DIOS()
