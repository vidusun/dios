# coding=utf-8

"""
    D.I.O.S. Client: Client for Design of Inputs-Outputs with Sikuli
    Copyright (C) 2017  Gabriel Siano
    Universidad Nacional del Litoral/CONICET
    <gabrielsiano_at_gmail_dot_com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import os
import sys
import time
import atexit
import wx
import httplib2
import socket
import errno

from wx import Menu, MenuBar
from wx import EVT_MENU, EVT_CLOSE
#NOTE: next lines are mainly needed for stand-alone execution:
#See https://github.com/kivy/kivy/issues/4182 and
# https://www.reddit.com/r/learnpython/comments/1fiw47/need_help_packaging_my_program_into_an_executable/?st=j3k8k632&sh=7c737905
#to undestand this more deeply
if 'twisted.internet.reactor' in sys.modules:
    del sys.modules['twisted.internet.reactor']

from twisted.internet import wxreactor
wxreactor.install()

# import twisted reactor *only after* installing wxreactor
from twisted.internet import reactor, protocol
from twisted.internet.protocol import ReconnectingClientFactory
from twisted.protocols import basic
# import ClienteSikuSRV

from pykeyboard import PyKeyboard


ID_EXIT  = 101

class ClientFrame(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, parent=None, title="D.I.O.S. Client", size=(600, 600))
        self.MakeModal(True)
        self.protocol = ClientProtocol #cambié por la de arriba (= None)
        self.primeraConexion=True
        self.GUIactivo="0"
        self.IsTimeCoordinated=False
        self.conector=None
        self.clienteSikuSRV=ClienteSikuSRV()
        self.protoProcRunsikulix= ProtocoloProcesoRunsikulix(self)
        self.transporteDeProceso=None
        self.localFolder=os.getcwd()
        self.configFile=None
        self.OSname=os.name
        self.keyboard = PyKeyboard()
        self.clipboard=None

        self.Bind(wx.EVT_CLOSE, self.onClose)

## Configuración inicial:
        self.configDict = {
                        'servidor':'127.0.0.1',
                        'puerto':5001,
                        'nombreCliente':'Client',
                        'pathSikulis':self.localFolder,
                        'pathRunsikulix':self.localFolder,
                        'OKpathRunsikulix':False,
                        'OKpathSikulis':False,
                        'listaSikulis':[],
                        'sikuSRVrunning':False,
                        'secIs':'Stopped',#Stopped, Running, Paused
                        'FinalSec':[],#la secuencia total que envie un servidor
                        'FinalSecNFragmentos':0,#al pasar la secuencia se hará en fragmentos
                        'FinalSecDictFragmentos':{},#diccionario auxiliar para ensamblar fragmentos ordenados
                        'FinalSecLineAct':0,
        }



        menu = Menu()
        menu.Append(ID_EXIT, "E&xit", "Terminate the program")
        menuBar = MenuBar()
        menuBar.Append(menu, "&File")
        self.SetMenuBar(menuBar)
        EVT_MENU(self, ID_EXIT,  self.onClose)

        self.text = wx.TextCtrl(self, pos=(0,0), style=wx.TE_MULTILINE | wx.TE_READONLY, size=(200, 300))
        self.ctrl = wx.TextCtrl(self, pos=(0,301), style=wx.TE_PROCESS_ENTER, size=(200, 25))

        self.boxServidor =  wx.TextCtrl(self, pos=(200,50), style=wx.TE_PROCESS_ENTER, size=(150, 25))
        self.boxPuerto = wx.TextCtrl(self, pos=(350,50), style=wx.TE_PROCESS_ENTER, size=(50, 25))
        self.boxNombreCliente=  wx.TextCtrl(self, pos=(200,0), style=wx.TE_PROCESS_ENTER, size=(150, 25))
        self.boxSikulisFolder =  wx.TextCtrl(self, pos=(70,325), size=(330, 25))
        self.boxSikulisFolder.SetEditable(False)
        self.boxRunSikulixFolder =  wx.TextCtrl(self, pos=(70,360), style=wx.TE_PROCESS_ENTER, size=(330, 25))
        self.boxRunSikulixFolder.SetEditable(False)

        wx.StaticText(self, -1, 'Server', (200, 75))
        wx.StaticText(self, -1, 'Port', (350, 75))
        wx.StaticText(self, -1, 'Name', (200, 25))
        self.conectorStateText=wx.StaticText(self, -1, 'disconnected', (302, 107))
        self.conectorStateTextAsterisc=wx.StaticText(self, -1, '', (302, 100))


        self.boxServidor.Bind(wx.EVT_TEXT_ENTER, self.cambiarConfig)
        self.boxPuerto.Bind(wx.EVT_TEXT_ENTER, self.cambiarConfig)
        self.boxNombreCliente.Bind(wx.EVT_TEXT_ENTER, self.cambiarConfig)


        self.ctrl.Bind(wx.EVT_TEXT_ENTER, self.send)


        self.btnConnect = wx.Button(self, -1, 'Connect',pos=(200,100), size=(100, 25))
        self.btnConnect.Bind(wx.EVT_BUTTON, self.OnConnect)
        self.btnSikulisFolder = wx.Button(self, -1, 'sikulis',pos=(0,325), size=(70, 25))
        self.btnSikulisFolder.Bind(wx.EVT_BUTTON, self.OnOpenSikulisFolder)
        self.btnRunSikulixFolder = wx.Button(self, -1, 'runsikulix',pos=(0,360), size=(70, 25))
        self.btnRunSikulixFolder.Bind(wx.EVT_BUTTON, self.OnOpenRunSikulixFolder)

        self.comboListaSikulis = wx.ComboBox(self, pos=(400,325), size=wx.DefaultSize,  style=wx.CB_SORT, value="0 Sikulis")
        self.comboListaSikulis.SetEditable(False)

        self.btnRunSikulis = wx.Button(self, -1, 'RUN Sikulis',pos=(400,360), size=(80, 25))
        self.btnRunSikulis.Enable(False)#desactivar al inicio, solo activado si hay 1 ó más Sikulis's. Se actualiza en updateComboListaSikulis

        self.btnRunSikulis.Bind(wx.EVT_BUTTON, self.Ocultar)

        self.btnRunSikuSRV = wx.Button(self, -1, 'RunSikuSRV ',pos=(490,360), size=(100, 25))
        self.btnRunSikuSRV.Bind(wx.EVT_BUTTON, self.OnRunStopSikuSRV)


        EVT_CLOSE(self, lambda evt: reactor.stop())



## Configuración inicial:

        if os.path.isfile("config"):
            conf = []
            with open("config", "r+") as inputfile:
                for line in inputfile:
                    conf.append(line.rstrip('\r\n'))
            inputfile.close()

            self.configDict['servidor']=conf[0]
            self.configDict['puerto']=int(conf[1])
            self.configDict['nombreCliente']=conf[2]

            #Check Sikulis Folder
            if os.path.isdir(conf[3]):
                self.configDict['OKpathSikulis']=True
                self.configDict['pathSikulis']=conf[3]
            else:
                self.configDict['OKpathSikulis']=False
                conf[3]=self.localFolder

            #Check runsikulix
            if self.checkRunSikulixExists(conf[4]):
                self.configDict['OKpathRunsikulix']=True
                self.configDict['pathRunsikulix']=conf[4]
            else:
                self.configDict['OKpathRunsikulix']=False
                conf[4]=self.localFolder

            if not self.configDict['OKpathSikulis'] or not self.configDict['OKpathRunsikulix']:
                conf=map(lambda x:x+'\n', conf)#agregar "\n" antes de escribir
                with open("config", "w") as outputfile:
                    outputfile.writelines(conf)
                outputfile.close()

            self.updateRunSikulixStatus()
        else:
            #Crearlo:
            self.configFile = open("config", "w+")#Crearlo, abrir como escritura y lectura
            self.configFile.seek(0)
            self.configFile.writelines([self.configDict['servidor']+"\n",
                                        str(self.configDict['puerto'])+"\n",
                                        self.configDict['nombreCliente']+"\n",
                                        self.configDict['pathSikulis']+"\n",
                                        self.configDict['pathRunsikulix']+"\n"])
            if self.checkRunSikulixExists(self.configDict['pathRunsikulix']):
                pass#no hay que hacer nada, es sólo para que muestre el aviso.
                # Si RunSikulix estaba en el pathRunsikulix, esa variable ya estará configurada,
                # pero si no estaba, también será el mismo valor de la variable (el directorio local)

            self.updateRunSikulixStatus()
            self.configFile.close()

        # Chequear si runsikulix -s fue ejecutado y se encuentra escuchando en el puerto 50001
        # En caso afirmativo, hacer stop
        if self.clienteSikuSRV.ServerOnLine:
            self.clienteSikuSRV.stop()


        self.boxServidor.SetValue(self.configDict['servidor'])
        self.boxPuerto.SetValue(str(self.configDict['puerto']))
        self.boxNombreCliente.SetValue(self.configDict['nombreCliente'])
        self.boxSikulisFolder.SetValue(self.configDict['pathSikulis'])
        self.updateListaSikulis()
        self.boxRunSikulixFolder.SetValue(self.configDict['pathRunsikulix'])
## FIN Configuración inicial:

    def OnHide(self):
        print "hiding.."
        self.Hide()
        return

    def onClose(self, evt):
        self.tbIcon.RemoveIcon()
        self.tbIcon.Destroy()

        try:
            if self.clienteSikuSRV.stop():
                pass
        except Exception:
            pass

        reactor.stop()

        self.Destroy()



    #----------------------------------------------------------------------
    def onMinimize(self, event):
        self.Hide()


    def sikuliEndsInExit(self,file):
        EndsInExit=False
        #En fileaux debe existir la ruta completa al .py dentro de la carpeta .sikuli
        fileaux=self.configDict['pathSikulis']+"/"+file+"/"+file.split(".")[0]+".py"
        with open(fileaux, "r+") as inputfile:
            aux=inputfile.readlines()
        inputfile.close()
        for linea in reversed(aux):#recorrer desde la última hacia la primera
            if linea=='\n' or linea=='':
                pass
            elif linea.startswith("exit("):
                #Verificar que exit(n) tenga un argumento numérico o al menos que diga exit()
                auxstr=linea.replace("exit(","")
                auxstr=auxstr.replace(")","")
                auxstr=auxstr.replace('\n',"")
                auxstr=unicode(auxstr)#la funcion isnumeric sólo funciona con unicodes
                if auxstr.isnumeric() or auxstr=="":#si hay un número o hay un vacío
                    EndsInExit=True
                    break

        return EndsInExit

    def checkSTRforChars(self,STR,Chars=[]):
        OK=True
        for carac in Chars:
            if STR.find(carac) is not -1:
                OK=False
                break
        return OK



    def updateListaSikulis(self):
        self.configDict['listaSikulis']=[]
        #los sikulis (algo.sikuli) SON CARPETAS, NO ARCHIVOS
        alarmSikulisBadChars=False
        alarmSikulisExit=False
        badSikulis=""
        for file in os.listdir(self.configDict['pathSikulis']):
            if file.endswith(".sikuli"):
                self.configDict['listaSikulis'].append(str(file))

                if not self.checkSTRforChars(str(file),["*","#","_"]):
                    alarmSikulisBadChars=True

                if not self.sikuliEndsInExit(file):
                    alarmSikulisExit=True
                    badSikulis=badSikulis+file+'\n'

        self.configDict['listaSikulis'].sort()
        linea="INFORM_SIKULIS" + "*" + '#'.join(self.configDict['listaSikulis'])
        try:
            self.protocol.sendLine(linea)
        except TypeError:
            pass#Si se intenta enviar el mensaje, pero no se está conectado, entonces habrá un TypeError. Cuando exista
                #conexión, el server solicitará la lista de sikulis mediante un INFORM_SIKULIS

        if alarmSikulisBadChars:
            msg = "Sikuli files name must not include"+ '\nthe next characters: * , # , _ .\nThere may be faults. Change the names.'
            dlg = wx.MessageDialog(self, msg, 'Non-valid characters in Sikuli files name', wx.OK|wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()


        if alarmSikulisExit:
            msg = "Sikuli scripts must end with"+ '\nexit() or exit(any integer value) .\nCheck the following Sikuli scripts:\n' +badSikulis
            dlg = wx.MessageDialog(self, msg, 'Sikuli scripts without exit()', wx.OK|wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()

        self.updateComboListaSikulis()

    def updateComboListaSikulis(self):
        self.comboListaSikulis.Clear()
        self.comboListaSikulis.AppendItems(self.configDict['listaSikulis'])

        if self.comboListaSikulis.GetCount():#si hay al menos 1:
            self.comboListaSikulis.SetValue(str(self.comboListaSikulis.GetCount())+" sikulis")
            self.btnRunSikulis.Enable(True)
        else:
            self.comboListaSikulis.SetValue("0 Sikulis")
            self.btnRunSikulis.Enable(False)

    def send(self, evt):
        self.protocol.sendLine(str(self.ctrl.GetValue()))
        self.ctrl.SetValue("")

    def cambiarConfig(self, evt):
        #Primero verificar que en el nombre propuesto no existan caracteres no validos
        nombreAux=(self.boxNombreCliente.GetValue()).encode('ascii')
        if not self.checkSTRforChars(nombreAux,["*","#","_"]):
            msg = "User names must neither be repeated" + "\nnor include the next characters: * , # , _ .\nChange the names."
            dlg = wx.MessageDialog(self, msg, 'User name not valid', wx.OK|wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()
            self.configDict['nombreCliente']="Client"
            self.boxNombreCliente.SetValue(self.configDict['nombreCliente'])
            return

        self.configDict['servidor']=(self.boxServidor.GetValue()).encode('ascii')
        self.configDict['puerto']=int(self.boxPuerto.GetValue())
        self.configDict['nombreCliente']=(self.boxNombreCliente.GetValue()).encode('ascii')
        self.configDict['pathSikulis']=(self.boxSikulisFolder.GetValue()).encode('ascii')
        self.configDict['pathRunsikulix']=(self.boxRunSikulixFolder.GetValue()).encode('ascii')
        self.configFile = open("config", "w+")#abrir como escritura y lectura
        self.configFile.seek(0)
        self.configFile.writelines([self.configDict['servidor']+"\n",
                            str(self.configDict['puerto'])+"\n",
                            self.configDict['nombreCliente']+"\n",
                            self.configDict['pathSikulis']+"\n",
                            self.configDict['pathRunsikulix']+"\n"])

        self.configFile.close()


    def UpdateConectorStateText(self):
        if self.conector is not None:
            self.conectorStateText.SetLabel(self.conector.state)

    def UpdateConectorStateTextAsterisc(self):
        if self.conector is not None:
            self.conectorStateTextAsterisc.SetLabel("*")



    def OnConnect(self, event):
        if self.primeraConexion:
            try:
                # NOTA: primeraConexion=False lo tenía en el else del try, es decir, sólo se ejecutaba si
                # no se producían excepciones. Sin embargo, pueda o no pueda conectarse el conector, se creará
                # igual, por lo cual ya no será más la PrimeraConexión. Por ende, se pone False a continuación:
                self.primeraConexion=False
                self.btnConnect.SetLabel("Disconnect")
                self.conector=reactor.connectTCP(self.configDict['servidor'], self.configDict['puerto'], ClientFactory(self))
            except Exception, error:
                print "Exception when trying to connect!"
                print str(error)
                self.text.SetValue(self.text.GetValue() + "Exception when trying to connect!" +"\n")
        elif self.btnConnect.GetLabel() == "Connect":
            # Quiere conectar:
            self.conector.connect()
            self.protocol.factory.continueTrying=True #con esto, si hay desconexión sin solicitarla, intentará reconectar
            self.btnConnect.SetLabel("Disconnect")
            self.boxServidor.SetEditable(False)
            self.boxPuerto.SetEditable(False)
            self.conectorStateText.SetLabel(self.conector.state)

        elif self.btnConnect.GetLabel() == "Disconnect":
            # Quiere desconectar:
            self.GUIactivo="0"
            self.conector.disconnect()
            try:
                self.protocol.factory.continueTrying = False
            except AttributeError:
                pass

            self.btnConnect.SetLabel("Connect")
            self.boxServidor.SetEditable(True)
            self.boxPuerto.SetEditable(True)
            self.conectorStateText.SetLabel(self.conector.state)



    def conectorStateUpdate(self,state):
        if state is "connected":
            self.conectorStateTextAsterisc.SetLabel(" ")
            self.conectorStateText.SetLabel(state)
        elif state is "connecting":
            self.conectorStateText.SetLabel(state)
            self.conectorStateTextAsterisc.SetLabel("*")
        elif state is "disconnected":
            self.conectorStateText.SetLabel(state)

    def OnOpenSikulisFolder(self, event):
        pathAux = wx.DirSelector(message='Sikuli files folder?',
                              defaultPath=self.configDict['pathSikulis'])

        if pathAux:
            self.configDict['pathSikulis']=pathAux
            self.boxSikulisFolder.SetValue(self.configDict['pathSikulis'])
            self.updateListaSikulis()
            self.cambiarConfig(wx.EVT_TEXT_ENTER)
            if self.configDict['sikuSRVrunning']:#si el servidor ya estaba funcionando:
                self.clienteSikuSRV.scripts(pathAux)#debe configurarse la nueva carpeta de scripts sikulis


    def FinalSecProcessClient(self,msgList):#msgList: ["SEC","nLinea","info", "info2"... "infoN"]
        secIs=self.configDict['secIs']
        nLinea=int(msgList[1])
        self.configDict['FinalSecLineAct']=nLinea
        info=msgList[2]

        #leer la línea
        linea=self.configDict['FinalSec'][nLinea]
        # Es orden para este cliente?
        linea= linea.split(":")
        if linea[0]==self.configDict['nombreCliente'] or linea[0]=='ALL':
            pass
        else: #Si el cliente no tenía que hacer nada, retornar
            return


        if secIs=="Running":
            msg="SEC*"+str(nLinea)+"*"+"FAIL"

            if info=="SIKU":
                # Debe ejecutar un sikuli y debe responder al servidor informando si salió bien o mal
                sikuli=linea[1]+".sikuli"
                try:
                    if self.clienteSikuSRV.run(sikuli):
                        msg="SEC*"+str(nLinea)+"*"+"OK"
                except Exception:
                    print "Exception when executing .sikuli"
                    self.protocol.sendLine(msg)
                    wx.CallAfter(self.Mostrar)
                else:
                    self.protocol.sendLine(msg)
                    wx.CallAfter(self.Mostrar)


            elif info=="0":#Simplemente ejecutar
                if linea[1]=="TypeORD":
                    self.keyboard.type_string(linea[2])
                    msg="SEC*"+str(nLinea)+"*"+"OK"
                    self.protocol.sendLine(msg)
                    wx.CallAfter(self.Mostrar)
                elif linea[1]=="TypeORDs":
                    self.keyboard.type_string(linea[2])
                    msg="SEC*"+str(nLinea)+"*"+"OK"
                    self.protocol.sendLine(msg)
                    wx.CallAfter(self.Mostrar)
                elif linea[1]=="TypeIDX":
                    self.keyboard.type_string(linea[2])
                    msg="SEC*"+str(nLinea)+"*"+"OK"
                    self.protocol.sendLine(msg)
                    wx.CallAfter(self.Mostrar)
                elif linea[1]=="TypeTime":
                    t=time.time()
                    t=round(t,3)
                    t=int(t*1000.0)
                    self.keyboard.type_string(str(t))#la conversión a entero es porque sino time da float. Por ende
                    # agrega un ".". Es normal que TypeTime se use para generar nombres de archivo únicos que deben
                    # generarse, por lo que el dejar el "." complica las cosas con la extensión de dichos archivos
                    msg="SEC*"+str(nLinea)+"*"+"OK"
                    self.protocol.sendLine(msg)
                    wx.CallAfter(self.Mostrar)
                elif linea[1]=="TypeText":
                    self.keyboard.type_string(linea[2])
                    msg="SEC*"+str(nLinea)+"*"+"OK"
                    self.protocol.sendLine(msg)
                    wx.CallAfter(self.Mostrar)
                elif linea[1]=="SendKEY":
                    if linea[2]=="Enter":
                        self.keyboard.tap_key(self.keyboard.enter_key)
                    elif linea[2]=="ESC":
                        self.keyboard.tap_key(self.keyboard.escape_key)

                    msg="SEC*"+str(nLinea)+"*"+"OK"
                    self.protocol.sendLine(msg)
                    wx.CallAfter(self.Mostrar)
            else:
                pass
        elif secIs=="Paused":
            pass
        elif secIs=="Stopped":
            pass

    def Ocultar(self,event):
        self.Hide()
        wx.CallAfter(self.OnRunSikulis)

    def OcultarFromServer(self,sikuli):
        self.MakeModal(False)
        self.Hide()
        wx.CallAfter(self.OnRunSikulisFromServer,sikuli)
        return


    def OnRunSikulis(self):
        try:
            if self.comboListaSikulis.GetCurrentSelection() is not -1 and self.configDict['sikuSRVrunning']:#es -1 cdo no hay un item seleccionado
                if self.clienteSikuSRV.run((self.comboListaSikulis.GetStringSelection()).encode('ascii')):
                    pass
                else:
                    dlg = wx.MessageDialog(self, 'The following file could not be executed: '+(self.comboListaSikulis.GetStringSelection()).encode('ascii')+ '\nCheck if the last line is exit().\nVerify that the current view corresponds\nto the .sikuli training stage.', 'Sikuli script can not be executed', wx.OK|wx.ICON_INFORMATION)
                    dlg.ShowModal()
                    dlg.Destroy()
        except Exception:
            print "Exception when executing .sikuli"
            wx.CallAfter(self.Mostrar)
        else:
            wx.CallAfter(self.Mostrar)


    def OnRunSikulisFromServer(self,sikuli):
        try:
            if self.configDict['sikuSRVrunning']:
                if self.clienteSikuSRV.run(sikuli):
                    msg="RUN*OK#"+sikuli
                    self.protocol.sendLine(msg)
                else:
                    msg="RUN*FAIL#"+sikuli
                    self.protocol.sendLine(msg)
                    dlg = wx.MessageDialog(self, 'The following file could not be executed: '+sikuli+ '\nCheck if the last line is exit().\nVerify that the current view corresponds\nto the .sikuli training stage.', 'Sikuli script can not be executed', wx.OK|wx.ICON_INFORMATION)
                    dlg.ShowModal()
                    dlg.Destroy()
            else:
                self.protocol.sendLine("SIKUSRV_OFF")
        except Exception:
            print "Exception when executing .sikuli"
            wx.CallAfter(self.Mostrar)
        else:
            wx.CallAfter(self.Mostrar)

    def Mostrar(self):
        self.MakeModal(True)
        self.Show(True)
        self.SetFocus()


    def notifySRVstatus(self, isOn=True):
        #Esta función está hecha para ser llamada desde ProtocoloProcesoRunsikulix. Allí se leen las salidas del SRV
        # y los mensajes específicos que indican si el server está o no encendido.
        #isOn: True si el SikuSRV está encendido
        if isOn:
            msg = "SIKU_SRV_STATUS*ON"
        else:
            msg = "SIKU_SRV_STATUS*OFF"
        try:
            self.protocol.sendLine(msg)
        except TypeError:
            pass#Si se intenta enviar el mensaje, pero no se está conectado, entonces habrá un TypeError.


    def notifyTReport(self,TReport):#EXPERIMENTAL
        msg = "TREPORT*" + TReport
        self.protocol.sendLine(msg)



    def OnRunStopSikuSRV(self,event):
        if self.configDict['OKpathRunsikulix']:
            self.btnRunSikuSRV.Enable(False)#desactivar el botón de inmediato (lo activa updateOnRunStopSikuSRVStatus)

            if self.configDict['sikuSRVrunning']:#ESTÁ ENCENDIDO, APAGARLO
                self.clienteSikuSRV.stop()
            else:#ESTÁ APAGADO, ENCENDERLO
                if self.OSname=="posix":
                    aux=self.configDict['pathRunsikulix'] + "/runsikulix"
                elif self.OSname=="nt":
                    aux=self.configDict['pathRunsikulix'] + "/runsikulix.cmd"

                # reactor.spawnProcess returns an instance that implements IProcessTransport.
                self.transporteDeProceso=reactor.spawnProcess(self.protoProcRunsikulix, aux, [aux, "-s"], os.environ, None)

                # NOTA SOBRE os.environ: es necesario pasarlo, pues sino se pasa un empty env y no sirve
                # Buscando un poco, en la ayuda de reactor.spawnProcess dice:
                # If you just provide os.environ for env , the child program will inherit the environment from the
                # current process, which is usually the civilized thing to do (unless you want to explicitly clean
                # the environment as a security precaution). The default is to give an empty env to the child.


    def updateOnRunStopSikuSRVStatus(self):

        if self.configDict['sikuSRVrunning']:#ESTÁ ENCENDIDO, APAGARLO
            self.configDict['sikuSRVrunning']=False
            self.btnRunSikuSRV.SetLabel("RunSikuSRV")
        else:#ESTÁ APAGADO, ENCENDERLO
            self.configDict['sikuSRVrunning']=True
            self.btnRunSikuSRV.SetLabel("StopSikuSRV")
            # TAREAS POR HACER:
            # + Llamar startp
            if not self.clienteSikuSRV.startp():
                dlg = wx.MessageDialog(self, 'Runner for Python could not be started. \n The program may not work properly. \nCheck your configuration (Jython?).', 'Error Runner for Python', wx.OK|wx.ICON_INFORMATION)
                dlg.ShowModal()
                dlg.Destroy()

            # + Establecer scripts folder
            if not self.clienteSikuSRV.scripts(self.configDict['pathSikulis']):
                dlg = wx.MessageDialog(self, 'There was a problem with Sikuli files folder. \n The program may not work properly. \nTry moving files to another folder.', 'Error Sikuli files folder', wx.OK|wx.ICON_INFORMATION)
                dlg.ShowModal()
                dlg.Destroy()
        self.btnRunSikuSRV.Enable(True)#Activar botón

    def OnOpenRunSikulixFolder(self, event):
        pathAux = wx.DirSelector(message='Folder of runsikulix?',
                              defaultPath=self.configDict['pathRunsikulix'])
        if pathAux:
            if self.checkRunSikulixExists(pathAux):
                pass

            self.configDict['pathRunsikulix']=pathAux
            self.boxRunSikulixFolder.SetValue(self.configDict['pathRunsikulix'])
            self.updateRunSikulixStatus()
            self.cambiarConfig(wx.EVT_TEXT_ENTER)


    def checkRunSikulixExists(self,pathAux):
        if os.path.isfile(pathAux+"/"+"runsikulix") or os.path.isfile(pathAux+"/"+"runsikulix.cmd"):#linux OR windows, respectivamente
            self.configDict['OKpathRunsikulix']=True
        else:
            self.configDict['OKpathRunsikulix']=False
            dlg = wx.MessageDialog(self, 'Sikuli scripts can not be used. \nFind runsikulix.', 'runsikulix not found', wx.OK|wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()

        return self.configDict['OKpathRunsikulix']


    def updateRunSikulixStatus(self):
        if self.configDict['OKpathRunsikulix']:
            self.btnRunSikulixFolder.SetBackgroundColour( ( 0, 255, 0 ) )#verde
        else:
            self.btnRunSikulixFolder.SetBackgroundColour( ( 255, 0, 0 ) )#rojo

class ClienteSikuSRV(object):
    """Sin documentación"""

    def __init__(self):
        self.cliente = httplib2.Http(".cache")  # Http([cache=None][, timeout=None]), crear ese directorio para alojar
        # datos en la carpeta local
        self.cliente.force_exception_to_status_code = False  # If True then no httplib2 exceptions will be thrown.
        # Instead, those error conditions will be turned into Response objects that will be returned normally.
        self.resp = None
        self.content = None
        self.pathSikulisFolder = os.getcwd()
        self.ServerOnLine = False
        self.testServerOnLine()
        self.stopOK=False
        self.startpOK=False
        self.pathSikulisFolderOK=False
        self.runOK=False

    def testServerOnLine(self):
        try:
            self.resp, self.content = self.cliente.request("http://127.0.0.1:50001/isOnLine", "GET")
            if self.content.startswith("FAIL 400 invalid command: GET /isOnLine"):
                print "SikuSRV online"
                self.ServerOnLine = True
                return  self.ServerOnLine
        except socket.error, e:
            errorcode = e[0]
            if errorcode == errno.ECONNREFUSED:
                print "Connection to sikuSRV Refused (offline?)"
            self.ServerOnLine = False
            return  self.ServerOnLine
        except Exception, e:
            print "Exception when trying to connect with Sikuli Server!"
            self.ServerOnLine = False
            return  self.ServerOnLine

    def stop(self):
        self.stopOK=False
        self.resp, self.content = self.cliente.request("http://127.0.0.1:50001/stop", "GET")
        if self.content.startswith("PASS 200 stopping server"):
            print "SikuSRV stopped and offline"
            self.ServerOnLine = False
            self.stopOK=True
        return self.stopOK


    def startp(self):
        self.startpOK=False
        self.resp, self.content = self.cliente.request("http://127.0.0.1:50001/startp", "GET")
        if self.content.startswith("PASS 200 startRunner for: Python"):
            print "Runner for Python OK"
            self.startpOK=True
        else:
            print "Runner for Python could not be started"
            self.startpOK=False
        return self.startpOK

    def scripts(self, pathScripts):
        self.pathSikulisFolderOK=False
        # En principio es necesario verificar que pathScripts comience con /
        if not pathScripts.startswith("/"):
            pathScripts="/"+pathScripts #agregar /

        pathScripts=pathScripts.replace("\\", "/")# reemplazar \ con / (Windows)

        self.resp, self.content = self.cliente.request("http://127.0.0.1:50001/scripts" + pathScripts, "GET")
        if self.content.startswith("PASS 200 scriptFolder now"):
            print "scriptFolder found"
            self.pathSikulisFolder = pathScripts
            self.pathSikulisFolderOK=True
        elif self.content.startswith("FAIL 404 scriptFolder not found"):
            # Linux: es posible que al contener "home", luego se interprete que home es home/user
            if pathScripts.find("/home/") is not -1:
                aux=pathScripts.split("/")
                aux=pathScripts.replace("/"+aux[2], "", 1)
                self.resp, self.content = self.cliente.request("http://127.0.0.1:50001/scripts" + aux, "GET")
                if self.content.startswith("PASS 200 scriptFolder now"):
                    print "scriptFolder found"
                    self.pathSikulisFolder = pathScripts
                    self.pathSikulisFolderOK=True
                else:
                    print "scriptFolder not found"
            else:
                print "scriptFolder not found"
        elif self.content.startswith("FAIL 400 no scriptFolder given"):
            print "no scriptFolder given"
        else:
            print "Error with scriptFolder"
        return self.pathSikulisFolderOK

    def run(self,sikuliScript):
        self.runOK=False
        self.resp, self.content = self.cliente.request("http://127.0.0.1:50001/run/" + sikuliScript, "GET")
        if self.content.startswith("PASS 200 runScript: returned:"):
            self.runOK=True
            print sikuliScript + ": executed OK"
        elif self.content.startswith("FAIL 200 runScript: script not found, not valid or not supported"):
            print sikuliScript + ": script not found, not valid or not supported"
        elif self.content.startswith("FAIL 503 startRunner Python: not possible or crashed with exception"):
            print sikuliScript + ": execution not possible or crashed with exception"
        return self.runOK

    def printRespContent(self):
        print self.resp
        print self.content

class ClientProtocol(basic.LineReceiver):

    def __init__(self):
        self.output = None
        self.name = None


    def connectionMade(self):
        # NOTA: state es connected
        # When a connection is initially made to the server, the ClientProtocol's attribute,
        #  "output" is changed to the read-only text control. Twisted will redirect all output
        # received from the server into this text control (and scrolls the control to
        # the bottom with SetInsertionPointEnd)
        self.output = self.factory.gui.text  # redirect Twisted's output
        #Note that clients should call my resetDelay method after they have connected successfully.
        self.factory.resetDelay()

        #Establecer que no hay coordinación de tiempos al inicio de una conexión
        self.factory.gui.IsTimeCoordinated=False
        # ENVIAR NOMBRE y ACTIVO AL CONECTAR
        print "Client name: " + self.factory.gui.configDict['nombreCliente']
        self.sendLine("NOMBRE"+"*"+self.factory.gui.configDict['nombreCliente'] + "#" + str(self.factory.gui.GUIactivo))

        # ACTUALIZAR GUI
        self.factory.gui.conectorStateUpdate(self.factory.gui.conector.state)


    def connectionLost(self,reason):
        print 'Protocol Lost connection.  Reason:', reason

    # NOTA: NO DEBERÍA SER dataReceived, SINO lineReceived, según el siguiente comentario:
    # https://www.google.com.ar/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&ved=0CDAQFjAC&url=http%3A%2F%2Fstackoverflow.com%2Fquestions%2F27042943%2Fhow-to-use-datareceived-in-twisted&ei=-x2PVeOcNoipNtf4qMgP&usg=AFQjCNGy_c-3TjuOhTo7ZAYmEvuYSzseBw&sig2=flhYnSOF5CWAqjjY5Zbieg&bvm=bv.96783405,d.cWw&cad=rja
    # When you use twisted.protocols.basic.LineReceiver, the dataReceived callback is not for you.
    # LineReceiver.dataReceived is an implementation detail of LineReceiver.
    # The callback for you is LineReceiver.lineReceived

    # No obstante, en la referencia de lineReceived dice que retorna line:
    # The line which was received with the delimiter removed REMOVED
    # PARA Q SE COMPORTE SIMILAR EN LA GUI, LE AGREGUÉ UN \n en gui.text.SetValue(val + line +"\n")
    def lineReceived(self, line):# antes: def dataReceived(self, data):
        gui = self.factory.gui
        gui.protocol = self

        if not gui.IsTimeCoordinated:#IsTimeCoordinated es False en connectionMade.
            timeNow=time.time()#esta toma de tiempos se realiza siempre que aun no haya terminado la coordinación de
            # tiempos con el servidor.

        linea= line.split("*")
        param=linea[0]
        if len(linea)>1:
            valores=linea[1]

        if param == "SEC":
            # PROCESAR APARTE, EN LA GUI
            wx.CallAfter(gui.FinalSecProcessClient,linea)
            return
        elif param == "ERR_NOMBRE":
            self.factory.continueTrying = False
            gui.btnConnect.SetLabel("Connect")
            msg = "User names must neither be repeated" + "\nnor include the next characters: * , # , _ .\nChange the names."

            dlg = wx.MessageDialog(self.factory.gui, msg, 'User name not valid', wx.OK|wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()
            # return
        elif param == "CFG_NOMBREOK":
            gui.GUIactivo="1"
            #En ocasiones los Clientes están listos con sus SikuSRV encendidos. Si el Server debe reiniciarse, entonces
            # al arrancar nuevamente no es informado por los clientes sobre su estado de SikuSRV. Informar.
            if gui.configDict['sikuSRVrunning']:
                gui.notifySRVstatus(True)
            else:
                gui.notifySRVstatus(False)
            # return
        elif param == "CFG_COORD_TIME_DONE":
            gui.IsTimeCoordinated = True
        elif param == "CFG_NOT_COORD_TIME":
            gui.IsTimeCoordinated = False
            msg = "CFG_NOT_COORD_TIME"
            self.sendLine(msg)
        elif param == "CFG_COORD_TIME":
            t = round(timeNow, 3)  # miliSeg
            newstring = "{:.3f}".format(t)
            msg="CFG_COORD_TIME"+"*"+newstring
            self.sendLine(msg)
            # return
        elif param == "INFORM_SIKULIS":
            #NOTA: se formatea separando INFORM_SIKULIS con *, y luego a los elementos de la lista con #
            msg="INFORM_SIKULIS" + "*" + '#'.join(self.factory.gui.configDict['listaSikulis'])
            self.sendLine(msg)
            # return
        elif param == "EXE_SIKULI":
             wx.CallAfter(gui.OcultarFromServer,valores)
             return
        elif param == "SEC_PASANDO_NFRAGS":#coordinar nro de fragmentos
            # Antes de procesar, debe verificarse si el sikuSRV está online. De no ser así, responder
            # que no está listo
            if not gui.configDict['sikuSRVrunning']:
                self.sendLine("SIKUSRV_OFF")
            else:
                gui.configDict['FinalSec']=[]#vaciar lista
                gui.configDict['FinalSecDictFragmentos']={}#vaciar dict de fragmentos
                gui.configDict['secIs']="Stopped"#indicar que aun está en stop
                gui.configDict['FinalSecNFragmentos']=int(valores)#establecer el nro de fragmentos q se recibirán
                self.sendLine("SEC_PASANDO_NFRAGS*"+self.factory.gui.configDict['nombreCliente'])
            # return
        elif param == "SEC_PASANDO_FRAGS":#recibir fragmentos
            # Antes de procesar, debe verificarse si el sikuSRV está online. De no ser así, responder
            # que no está listo
            if not gui.configDict['sikuSRVrunning']:
                self.sendLine("SIKUSRV_OFF")
            else:
                nFrag=valores#nro de fragmento q se está recibiendo
                fragmento=linea[2]
                #Agregar al diccionario
                gui.configDict['FinalSecDictFragmentos'][int(nFrag)]=fragmento
                #Verificar que el nro de fragmentos en el diccionario es el mismo que el nro de fragmentos que se iban
                # a recibir. En dicho caso, confirmar.
                if len(gui.configDict['FinalSecDictFragmentos'].keys())==gui.configDict['FinalSecNFragmentos']:
                    sec=""
                    for frag in range(1,gui.configDict['FinalSecNFragmentos']+1):
                        sec=sec+gui.configDict['FinalSecDictFragmentos'][frag]
                    # print "SEC:"
                    # print sec
                    sec=sec.split("#")
                    # print sec
                    gui.configDict['FinalSec']=sec
                    gui.configDict['secIs']="Running"
                    #Al responder con SEC_PASANDO y su nombre, indica que está listo
                    self.sendLine("SEC_PASANDO*"+self.factory.gui.configDict['nombreCliente'])
            return
        elif param == "SEC_RUNNING":
            gui.configDict['secIs']="Running"
        elif param == "SEC_STOPPED":
            gui.configDict['secIs']="Stopped"
        elif param == "SEC_PAUSED":
            gui.configDict['secIs']="Paused"

        if gui:
            val = gui.text.GetValue()
            gui.text.SetValue(val + line + "\n" )
            gui.text.SetInsertionPointEnd()

        # De http://stackoverflow.com/questions/27042943/how-to-use-datareceived-in-twisted
        # in order to avoid delays in communications of the server,
        # one should use return at the end of dataReceived() or lineReceived() functions
        return


    def NoIntentarConnect(self):
        self.factory.stopTrying()#para que no siga intentando conectar ya que es del tipo Reconnecting



class ClientFactory(protocol.ReconnectingClientFactory):
# The ClientFactory is used when connecting to Twisted's reactor.
# It stores a reference to the gui, so that it can update its text box,
# and also sets its protocol to the ClientProtocol.
# This tells twisted which Protocol handles any data received from the server.
    def __init__(self, gui):
        # NOTA: Lo que se defina en el init de la Factory, luego estará disponible
        # en cada instancia del protocolo creado. El acceso es:

        # When the protocol is created, it gets a reference to the factory as self.factory .
        # It can then access attributes of the factory in its logic

        # Notar como se le pasa gui (disponible luego para el protocolo como self.factory.gui)
        # que podría ser cualquier otra cosa necesaria (filename, una dirección, etc).

        # Esta es la forma por la cual un protocolo toma datos persistentes, desde la factory
        # y no desde si mismo, ya que no conservan info persistente.
        self.gui = gui
        self.protocol = ClientProtocol
        self.clients = []


    def startedConnecting(self, connector):
        # NOTA: ACÁ YA EXISTE EL connector (CON DOBLE N), pero no existe el gui.conector al inicio, y por eso va el if..is not None
        if self.gui.conector is not None:
            self.gui.conectorStateUpdate(connector.state)

    def clientConnectionLost(self, connector, reason):
       # NOTA: aquí el state es disconnected
        #NOTA: Called when an established connection is lost.
        print 'Lost connection.  Reason:', reason
        #NOTA: si anulo la siguiente línea, al querer conectarse YA HABIENDO ESTADO CONECTADO
        # (DICE "Called when an ESTABLISHED connection is lost.") y no encontrar al servidor
        #no intenta hacerlo una y otra vez. Si la dejo, lo intenta repetitivamente.
        ReconnectingClientFactory.clientConnectionLost(self, connector, reason)
        self.gui.conectorStateUpdate(connector.state)
        print "Factory:clientConnectionLost:connector.state: " + connector.state

    def clientConnectionFailed(self, connector, reason):
        #NOTA: Called when a connection has failed to connect.
        print 'Connection failed. Reason:', reason
        #NOTA: si anulo la siguiente línea, al querer conectarse y no encontrar al servidor
        #no intenta hacerlo una y otra vez. Si la dejo, lo intenta repetitivamente.
        ReconnectingClientFactory.clientConnectionFailed(self, connector,reason)
        self.gui.conectorStateUpdate(connector.state)
        print "Factory:clientConnectionFailed:connector.state: " + connector.state


class ProtocoloProcesoRunsikulix(protocol.ProcessProtocol):
    # Original file: https://twistedmatrix.com/documents/11.1.0/core/howto/listings/process/process.py
    def __init__(self, gui):#CON ACCESO A LA GUI
        self.data = ""
        self.ProcesoConectado=False
        self.sikuSRVisListening=False
        self.gui=gui#CON ACCESO A LA GUI
        self.analizar=True

    def debeAnalizar(self, TrueOrFalse):
        self.analizar=TrueOrFalse

    def connectionMade(self):
        print "connectionMade!"
        self.ProcesoConectado=True

    def outReceived(self, data):# ES EL EQUIVALENTE A dataReceived PERO PARA EL STDOUT
        # self.data = self.data + data #ESTO SIRVE PARA ACUMULAR
        self.data = data
        self.processSTDOUT(data)
    def errReceived(self, err):
        print err

    def processSTDOUT(self, data):
        data=data.split("\n")
        for linea in data:
            print "...." + linea
            if self.analizar:
                if linea.startswith("[info] RunServer: now waiting on port: 50001 at"):
                    if not self.sikuSRVisListening:
                        self.sikuSRVisListening=True
                        self.gui.updateOnRunStopSikuSRVStatus()
                        self.gui.notifySRVstatus(True)
                elif linea.startswith("[info] RunServer: now stopped on port: 50001"):
                    if self.sikuSRVisListening:
                        self.sikuSRVisListening=False
                        self.gui.updateOnRunStopSikuSRVStatus()
                        self.gui.notifySRVstatus(False)
                elif linea.startswith("[error] RunServer: Starting: Address already in use"):
                    print "Server was already in use"
                elif linea.startswith("['TReport"):#EXPERIMENTAL
                    self.gui.notifyTReport(linea)

    def inConnectionLost(self):
        print "INConnectionLost! stdIN is closed! (we probably did it)"
    def outConnectionLost(self):
        print "OUTConnectionLost! The child closed their stdOUT!"
        print self.data
    def errConnectionLost(self):
        print "ERRConnectionLost! The child closed their stdERR."
    def processExited(self, reason):
        self.ProcesoConectado=False
        print "processExited, status %d" % (reason.value.exitCode,)
    def processEnded(self, reason):
        self.ProcesoConectado=False
        print "processEnded, status %d" % (reason.value.exitCode,)
        print "quitting"
        # reactor.stop() #EN MI CASO EL STOP NO DEBE APLICARSE ACÁ PORQUE EL REACTOR TIENE OTROS USOS


def ARCANGEL():
    app = wx.App(False)
    frame = ClientFrame()
    frame.Show()
    reactor.registerWxApp(app)

    def alCerrar():
        if frame.clienteSikuSRV.testServerOnLine():
            frame.clienteSikuSRV.stop()
        reactor.stop()

    atexit.register(alCerrar)

    reactor.run()


if __name__ == '__main__':
    ARCANGEL()
